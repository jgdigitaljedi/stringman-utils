# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.8.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.7.2...v1.8.0?from_project_id=29192996) (2021-12-26)


### Features

* **area:** refactoring area to add many more (WIP), potentially fixed some password methods ([0729346](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/07293461f5c8872c0b1e5b2da30ed221a40a91d4))

### [1.7.2](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.7.1...v1.7.2?from_project_id=29192996) (2021-11-29)


### Chores

* **ci:** attempting to add testing to pipeline ([7265a7d](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/7265a7d106dda5b841b30935fc5ac31105d3ade9))

### [1.7.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.7.0...v1.7.1?from_project_id=29192996) (2021-11-29)


### Chores

* **ci:** changed .gitlab-ci.yml to test in pipeline ([2945e98](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/2945e9890508d7d2303c46e8f805acb22913e299))

## [1.7.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.6.0...v1.7.0?from_project_id=29192996) (2021-11-29)


### Features

* **area:** added conversions for sections, combined typedoc module gen, organization ([6ebe83f](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/6ebe83f4f97d813a671bafbc755f20f0aa4b9e4a))

## [1.6.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.5.1...v1.6.0?from_project_id=29192996) (2021-11-24)


### Features

* **area:** added square miles conversions to other area units, broke out land area into a file ([fcbf9e1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/fcbf9e102d20e7751f97a2d648f0c477b425eecd))

### [1.5.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.5.0...v1.5.1?from_project_id=29192996) (2021-11-20)


### Chores

* updated deps and readme ([252ea77](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/252ea770db04f84319f2cd4678f4056dad1e3dd2))

## [1.5.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.4.1...v1.5.0?from_project_id=29192996) (2021-11-20)


### Features

* **area:** added 10 more acre conversions, time to move on to next unit in area ([26e00f2](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/26e00f29d48e116461dd3aa4b6cd3e630d2d5738))
* **area:** created area module with 8 functions, more to come soon ([2ae9ddf](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/2ae9ddf4ff29c635b664c1b858283f102b71e1d2))

### [1.4.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.4.0...v1.4.1?from_project_id=29192996) (2021-11-06)


### Chores

* **dependencies:** updated dev dependencies to latest versions ([f894468](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/f894468de74ebfd2a9d786dad557c1019c214e5e))

## [1.4.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.3.0...v1.4.0?from_project_id=29192996) (2021-10-22)


### Features

* **volume:** added tsp and tbsp conversions and tests ([c8c8c5b](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/c8c8c5b5c9a83e7003e89700533fd5e73b53f366))


### Docs

* updated method count in readme ([dd0c29a](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/dd0c29abf731d15dcdc54eafc6ef43e72292c0f6))

## [1.3.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.2.0...v1.3.0?from_project_id=29192996) (2021-10-15)


### Features

* added brackets functions and tests ([50d59ef](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/50d59ef453341b7e023abb9935dc2d2e457644b8))

## [1.2.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.1.1...v1.2.0?from_project_id=29192996) (2021-09-21)


### Features

* **dataStorage:** added missing PB to GB and back, added to CD, BD, and DVD ([472a7fe](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/472a7fe0e6a1cad288d2978cc6c113282fda8d32))

### [1.1.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.1.0...v1.1.1?from_project_id=29192996) (2021-09-21)


### Docs

* **speed:** added forgotten usage examples to speedPercentspeedOfLight and speedToMach ([aa76393](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/aa7639333ae88be06695160570ac9260c315fb43))

## [1.1.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.0.3...v1.1.0?from_project_id=29192996) (2021-09-21)


### Features

* **speed:** added speedPercentSpeedOfLight and speedToMach with tests coverage ([2429a2c](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/2429a2c4e42e4e3b471790c2e3def15848850950))

### [1.0.3](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.0.2...v1.0.3?from_project_id=29192996) (2021-09-20)


### Features

* error handling and coverage for volume, weight, and colors ([93e4961](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/93e49615a33a0a25bb96a9d2e59a3ee01a286019))

### [1.0.2](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.0.1...v1.0.2?from_project_id=29192996) (2021-09-19)


### Improvements

* fixed fahrenheit typos everywhere, added error handling for distance and speed ([9ddd267](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/9ddd2671316c7adf7d5651166a9aceef30c52b3d))

### [1.0.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v1.0.0...v1.0.1?from_project_id=29192996) (2021-09-19)


### Chores

* **distance:** added error handling to distance with negative tests coverage ([223b878](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/223b878cce384b8f4e8380ccb626e937a196e94c))

## [1.0.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.7.1...v1.0.0?from_project_id=29192996) (2021-09-19)


### Features

* **computer:** added dataStorage, dataRate, and clockRate modules, errors for math ([7493247](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/7493247e7a8a7fce8a217ced9f879183d36cd7e6))


### Docs

* added file comments to each file for doc generation ([b3de2b9](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/b3de2b9136ba45b4d40ad6dd6be9802ad13a490d))

### [0.7.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.7.0...v0.7.1?from_project_id=29192996) (2021-09-17)


### Features

* **speed:** added speed conversions ([4ea9a41](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/4ea9a41d49ff9a934a8125e5b622a65b2535a98e))

## [0.7.0](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.6.4...v0.7.0?from_project_id=29192996) (2021-09-17)

### [0.6.4](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.6.3...v0.6.4?from_project_id=29192996) (2021-09-16)


### Features

* **distance:** added UK nautical miles conversions ([735474d](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/735474df1ce426497679d5e1a4581bbfd0a5df4b))

### [0.6.3](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.6.2...v0.6.3?from_project_id=29192996) (2021-09-15)

### [0.6.2](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.6.1...v0.6.2?from_project_id=29192996) (2021-09-15)


### Features

* **distance:** added several international nautical mile conversions ([4f883f1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/commits/4f883f1fb68177a9d935da7d63af3ef0f91388e9))

### [0.6.1](https://gitlab.com/jgdigitaljedi/stringman-utils/-/compare/v0.5.0...v0.6.1?from_project_id=29192996) (2021-09-15)
