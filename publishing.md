# Publishing guide

This is really just a note for myself to remind myself the process in which I cut releases. This way, if I don't touch this library for a while, I'll have a guide to let me know how I was going about publishing.

1. update function count in readme.md (I've been using the number of tests that show when run)
2. run `npm run build`
3. run `npm run docs`
4. run `git add -A`
5. run `git commit -m "<commit message>"`
   - look at `.versionrc.json` for commit prefixes to make sure your commit is prefixed properly
6. run `npm run release:<which>` **major, minor, patch**
   - This should increment version and generate changelog
7. run `npm publish`
   - This SHOULD run tests and lint, create the tag, publish to npm, and push tag and master to Gitlab
