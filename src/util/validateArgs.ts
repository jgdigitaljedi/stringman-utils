/** @internal */
export function isString(arg: any): boolean {
  return typeof arg === 'string';
}

export function isNumberString(arg: any): boolean {
  return typeof arg === 'number' || (typeof arg === 'string' && !isNaN(parseFloat(arg)));
}
