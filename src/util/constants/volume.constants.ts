// file volume.constants.ts
/** @module constants-volume */
export const ozInCup = 8;
export const cupsInPintsInQuarts = 2;
export const quartsInGallon = 4;
export const mlInFlOz = 29.57;
export const litersInGallon = 3.785411784;
export const ozInGallons = 128;
export const tspInTbsp = 3;
export const tspInFlOz = 6;
export const tbspInFlOz = 2;
export const mlInTsp = 4.9289215937;
export const mlInTbsp = 14.7867647811;
