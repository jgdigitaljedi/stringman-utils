// file speed.constants.ts
/** @module constants-speed */
export const kmphInMph = 1.609344;
export const kmhInMs = 3.6;
export const kmphInFtPerSec = 1.09728;
export const mPerSecInMph = 0.44704;
export const mphInKn = 1.1507794480235;
export const kmphInKn = 1.8519995164223486;
export const mphInFtPerSec = 0.6818181802686;
export const knotsInFtPerSec = 0.5924838012959;
export const mPerSecInFtPerSec = 0.3048;
export const mPerSecInKn = 0.51444444444444;
export const ftsInSpeedOfLight = 983571056.43045;
export const msInSpeedOfLight = 299792458;
export const milesPsInSpeedOfLight = 186282;
export const kmphInSpeedOfLight = 1079252848.8;
export const mphInSpeedOfLight = 670616629.3844;
export const knInSpeedOfLight = 582749918.36357;
export const ftsInSpeedOfSound = 1125;
export const kmphInSpeedOfSound = 1234.8;
export const mphInSpeedOfSound = 767.26915;
export const milesPsInSpeedOfSound = 0.21313;
export const msInSpeedOfSound = 343;
export const knotsInSpeedOfSound = 661;
