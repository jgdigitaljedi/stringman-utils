// file geo.constants.ts
/** @module constants-geo */
export const gradsToDegrees = 0.9;
export const radiansToDegrees = 57.2958;
export const radiansToGrads = 63.662;
