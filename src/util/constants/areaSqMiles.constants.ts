// file areaSqMiles.constants.ts
/** @module constants-area-sqMiles */
/** naming scheme
 * I learned, while creating this, that there are subtle differences/versions in acres.
 * For example, there is a standard acre, intl acre, Ireland acre, and survey acre, all of which
 * are slightly different.
 *
 * I am differentiating by using suffixes. "Standard" units get no suffix. Here is my legend:
 * - Gunter/survey = `Gun`
 * - Ramden/engineer = `Ram`
 * - commercial = `Com`
 * - Ireland = `Ire`
 * - survey = `Sur`
 */

export const squareYdsInSquareMiles = 3097600; // exactly
export const squareRodsInSquareMile = 102400; // exactly
export const hectaresInSqMile = 258.9988110336;
export const squareFeetInSquareMile = 27878400; // exactly
export const squareMilesInTownship = 36;
export const squareMetersInSquareMile = 2589988.110336;
export const squareLinksGunInSquareMile = 64000000.889576;
export const squareLinksRamInSquareMile = 36919031.563663;
export const squareChainsInSquareMile = 6400; // exactly
export const squareChainsGunInSquareMile = 8475.4434819618;
export const squareChainsRamInSquareMile = 3691.9031563663;
export const squareMilesInSection = 1.0000000001297;
export const squareKilometersInSquareMiles = 2.5899881103360003;
export const acresComInSqMile = 774.4; // exactly
export const acresIreInSqMile = 523.25003813883;
export const acresSurInSqMile = 639.99743733847;
