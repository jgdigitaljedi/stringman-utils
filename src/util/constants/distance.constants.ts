// file distance.constants.ts
/** @module constants-distance */
export const feetInYard = 3; // exactly
export const feetInMeter = 3.28084;
export const yardsInMile = 1760; // exactly
export const feetInMile = 5280; // exactly
export const metersInMile = 1609.344;
export const inchesInFoot = 12; // exactly
export const inchesInMeter = 39.3700787401;
export const inchesInCm = 0.393700787401;
export const metersInYards = 0.9144; // exactly
export const metersInFeet = 0.3048; // exactly
export const intlNauticalMileToMeters = 1852; // exactly
export const intlNauticalMileToMile = 1.1507794480235425;
export const uKNauticalMileToMeters = 1853.184; // exactly
export const uKNauticalMileToMiles = 1.1515151515151514;
