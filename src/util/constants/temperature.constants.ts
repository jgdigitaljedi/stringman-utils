// file temperature.constants.ts
/** @module constants-temperature */
export const celciusToKelvinDifference = 273.15;
export const celciusFahrenheitRatio = 1.8;
export const celciusFahrenheitDifference = 32;
export const fahrenheitKelvinDifference = 459.67;
