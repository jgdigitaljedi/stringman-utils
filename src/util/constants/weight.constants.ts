// file weight.constants.ts
/** @module constants-weight */
export const lbsInKg = 2.20462262185;
export const lbsInTons = 2000;
export const ozInLbs = 16;
export const ozInGrams = 0.035274;
export const lbsToStones = 14;
export const kgToStones = 6.350293179999988;
