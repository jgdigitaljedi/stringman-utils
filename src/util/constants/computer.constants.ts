// file computer.constants.ts
/** @module constants-computer */
export const dataDecimalStep = 1024;
export const bitsInByte = 8;
export const gigsOnCd = 0.634765625;
export const gigsOnDvd = 4.38;
export const gigsOnBd = 25;
