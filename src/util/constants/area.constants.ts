// file area.constants.ts
/** @module constants-area */
/** naming scheme
 * I learned, while creating this, that there are subtle differences/versions in some units.
 * For example, there is a standard acre, intl acre, Ireland acre, and survey acre, all of which
 * are slightly different. I didn't intend on making area such a big module, but this discovery
 * made it necessary.
 *
 * I am differentiating by using suffixes. Here is my legend:
 * - Ramden/engineer = `Ram`
 * - Gunter/survey = `Gun`
 * - Internationl = `Intl`
 * - survey = `Sur`
 * - Ireland = `Ire`
 * - commercial = `Com`
 * - nautical = `Naut`
 *
 * A 'standard' unit will have no suffix (eg. acres)
 */

// acres (survey)
export const acreSurInSqChainRam = 4.356;

// acres (commercial)
// acres (Ireland)

// sq mile (Nautical)
// sq mile (survey)

// section
export const squareChainsInSection = 6400.0000889576;
export const squareLinksInSection = 64000000.889576;
export const squareYdsInSection = 3097612.4259347;
export const squareMetersInSection = 2589998.5; // exactly
export const squareKilometersInSection = 2.5899985; // exactly
export const squareFeetInSection = 27878511.833413;
export const hectaresInSection = 258.99985;
export const sectionsInTownship = 35.999999737308;
export const squareRodsInSection = 102400.00074721;

// sq chain (Gunter/survey)
export const squareLinksGunInSquareChainsGun = 10000; // exactly
