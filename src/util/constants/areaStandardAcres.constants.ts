// file areaStandardAcres.constants.ts
/** @module constants-area-acres */
/** naming scheme
 * I learned, while creating this, that there are subtle differences/versions in acres.
 * For example, there is a standard acre, intl acre, Ireland acre, and survey acre, all of which
 * are slightly different.
 *
 * I am differentiating by using suffixes. Here is my legend:
 * - survey = `Sur`
 * - Ireland = `Ire`
 * - commercial = `Com`
 * - Gunter/survey = `Gun`
 * - Ramden/engineer = `Ram`
 * - internation = `Intl`
 *
 * A 'standard' unit will have no suffix (eg. acres)
 */

export const acresComInAcre = 1.21; // exactly
export const acresInAcreIre = 1.6197757755173;
export const acresInAcreSur = 1.0000040041747;
export const hectaresInAcre = 2.4710538146717; // exactly
export const acresInSqMiles = 640; // exactly
export const acresInSqMileNaut = 847.54773631576;
export const acresInSqMileSur = 640.00256734189;
export const sqChainGunInAcre = 9.9999600244396;
export const sqChainRamInAcre = 4.355982557885;
export const squareFeetInAcre = 43560; // exactly
export const squareYardInAcre = 4840; // exactly
export const sqRodsInAcres = 159.99935933462;
export const sqLinksGunInAcre = 99999.600244396;
export const sqLinksRamInAcre = 99999.600244396;
export const acresInTownship = 23040.092256185;
export const acresInSection = 640.00256734189;
export const acresInSquareKilometer = 247.10538146716533;
export const squareMetersInAcre = 4046.8564224; // exactly, intl acres
