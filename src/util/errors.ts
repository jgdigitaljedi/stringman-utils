/** @internal */
export const TWO_NUM_ERROR = 'At least 1 of the values passed does not parse into a number.';
export const ONE_NUM_ERROR = 'Value passed does not parse into a number.';
export const MUST_BE_STRING = 'Argument must be a string.';
export const NOT_VALID_HEX_COLOR = 'Argument provided is not a valid hexidecimal color value.';
export const INVALID_UNIT = 'Invalid unit value sent in arguments.';
