import {
  distanceKmToMeters,
  distanceMetersToKm,
  distanceCmToInches,
  distanceFeetToInches,
  distanceFeetToKm,
  distanceFeetToMeters,
  distanceFeetToMiles,
  distanceFeetToYards,
  distanceInchesToCm,
  distanceInchesToFeet,
  distanceInchesToMeters,
  distanceKmToFeet,
  distanceMetersToFeet,
  distanceMetersToInches,
  distanceMetersToMiles,
  distanceMilesToFeet,
  distanceMilesToMeters,
  distanceMilesToYards,
  distanceYardsToFeet,
  distanceYardsToMiles,
  distanceFeetToIntlNauticalMi,
  distanceIntlNauticalMiToFeet,
  distanceIntlNauticalMiToMeters,
  distanceIntlNauticalMiToMiles,
  distanceMetersToIntlNauticalMi,
  distanceMilesToIntlNauticalMi,
  distanceFeetToUkNauticalMi,
  distanceMetersToUkNauticalMi,
  distanceMilesToUkNauticalMi,
  distanceUkNauticalMiToFeet,
  distanceUkNauticalMiToMeters,
  distanceUkNauticalMiToMiles
} from '..';
import { TWO_NUM_ERROR } from '../util/errors';

describe('distance utils', () => {
  it("should call 'distanceMetersToKm' and return conversion of meters to kilometers", () => {
    expect(distanceMetersToKm(1000)).toEqual(1);
    expect(distanceMetersToKm(5000)).toEqual(5);
    expect(distanceMetersToKm(5500)).toEqual(5.5);
    // @ts-ignore
    expect(() => distanceMetersToKm('nope', true)).toThrow(`distanceMetersToKm: ${TWO_NUM_ERROR}`);
  });

  it("should call 'distanceKmToMeters' and return conversion of kilometers to meters", () => {
    expect(distanceKmToMeters(1)).toEqual(1000);
    expect(distanceKmToMeters(5)).toEqual(5000);
    expect(distanceKmToMeters(5.5)).toEqual(5500);
    // @ts-ignore
    expect(() => distanceKmToMeters('nope', true)).toThrow(`distanceKmToMeters: ${TWO_NUM_ERROR}`);
  });

  it("should call 'distanceFeetToYards' and return conversion of feet to yards", () => {
    expect(distanceFeetToYards(3)).toEqual(1);
    expect(distanceFeetToYards(300)).toEqual(100);
    expect(distanceFeetToYards(31.5)).toEqual(10.5);
    // @ts-ignore
    expect(() => distanceFeetToYards('nope', true)).toThrow(
      `distanceFeetToYards: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceYardsToFeet' and return conversion of yards to feet", () => {
    expect(distanceYardsToFeet(1)).toEqual(3);
    expect(distanceYardsToFeet(100)).toEqual(300);
    expect(distanceYardsToFeet(10.5)).toEqual(31.5);
    // @ts-ignore
    expect(() => distanceYardsToFeet('nope', true)).toThrow(
      `distanceYardsToFeet: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceFeetToMeters' and return conversion of feet to meters", () => {
    expect(distanceFeetToMeters(1)).toEqual(0.3047999902464003);
    expect(distanceFeetToMeters(3.28084)).toEqual(1);
    expect(distanceFeetToMeters(275)).toEqual(83.81999731776008);
    // @ts-ignore
    expect(() => distanceFeetToMeters('nope', true)).toThrow(
      `distanceFeetToMeters: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMetersToFeet' and return conversion of meters to feet", () => {
    expect(distanceMetersToFeet(0.3047999902464003)).toEqual(1);
    expect(distanceMetersToFeet(1)).toEqual(3.28084);
    expect(distanceMetersToFeet(83.81999731776008)).toEqual(275);
    // @ts-ignore
    expect(() => distanceMetersToFeet('nope', true)).toThrow(
      `distanceMetersToFeet: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMetersToMiles' and return conversion of meters to miles", () => {
    expect(distanceMetersToMiles(1609.344)).toEqual(1);
    expect(distanceMetersToMiles(1)).toEqual(0.0006213711922373339);
    expect(distanceMetersToMiles(2500)).toEqual(1.5534279805933349);
    // @ts-ignore
    expect(() => distanceMetersToMiles('nope', true)).toThrow(
      `distanceMetersToMiles: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMilesToMeters' and return conversion of miles to meters", () => {
    expect(distanceMilesToMeters(0.0006213711922373339)).toEqual(0.9999999999999999);
    expect(distanceMilesToMeters(1)).toEqual(1609.344);
    expect(distanceMilesToMeters(1.5534279805933349)).toEqual(2500);
    // @ts-ignore
    expect(() => distanceMilesToMeters('nope', true)).toThrow(
      `distanceMilesToMeters: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceFeetToKm' and return conversion of feet to km", () => {
    expect(distanceFeetToKm(3280.84)).toEqual(1);
    expect(distanceFeetToKm(1)).toEqual(0.0003047999902464003);
    expect(distanceFeetToKm(10000)).toEqual(3.047999902464003);
    // @ts-ignore
    expect(() => distanceFeetToKm('nope', true)).toThrow(`distanceFeetToKm: ${TWO_NUM_ERROR}`);
  });

  it("should call 'distanceKmToFeet' and return conversion of km to feet", () => {
    expect(distanceKmToFeet(0.0003047999902464003)).toEqual(1);
    expect(distanceKmToFeet(1)).toEqual(3280.84);
    expect(distanceKmToFeet(10000)).toEqual(32808400);
    // @ts-ignore
    expect(() => distanceKmToFeet('nope', true)).toThrow(`distanceKmToFeet: ${TWO_NUM_ERROR}`);
  });

  it("should call 'distanceInchesToFeet' and return conversion of in to feet", () => {
    expect(distanceInchesToFeet(12)).toEqual(1);
    expect(distanceInchesToFeet(1)).toEqual(0.08333333333333333);
    expect(distanceInchesToFeet(240)).toEqual(20);
    // @ts-ignore
    expect(() => distanceInchesToFeet('nope', true)).toThrow(
      `distanceInchesToFeet: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceFeetToInches' and return conversion of feet to inches", () => {
    expect(distanceFeetToInches(0.08333333333333333)).toEqual(1);
    expect(distanceFeetToInches(1)).toEqual(12);
    expect(distanceFeetToInches(10)).toEqual(120);
    // @ts-ignore
    expect(() => distanceFeetToInches('nope', true)).toThrow(
      `distanceFeetToInches: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceInchesToMeters' and return conversion of inches to meters", () => {
    expect(distanceInchesToMeters(39.3700787401)).toEqual(1);
    expect(distanceInchesToMeters(1)).toEqual(0.025400000000037084);
    expect(distanceInchesToMeters(100)).toEqual(2.5400000000037084);
    // @ts-ignore
    expect(() => distanceInchesToMeters('nope', true)).toThrow(
      `distanceInchesToMeters: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMetersToInches' and return conversion of meters to inches", () => {
    expect(distanceMetersToInches(0.025400000000037084)).toEqual(1);
    expect(distanceMetersToInches(1)).toEqual(39.3700787401);
    expect(distanceMetersToInches(100)).toEqual(3937.00787401);
    // @ts-ignore
    expect(() => distanceMetersToInches('nope', true)).toThrow(
      `distanceMetersToInches: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceCmToInches' and return conversion of cm to inches", () => {
    expect(distanceCmToInches(0.393700787401)).toEqual(1);
    expect(distanceCmToInches(1)).toEqual(2.540000000003708);
    expect(distanceCmToInches(100)).toEqual(254.00000000037085);
    // @ts-ignore
    expect(() => distanceCmToInches('nope', true)).toThrow(`distanceCmToInches: ${TWO_NUM_ERROR}`);
  });

  it("should call 'distanceInchesToCm' and return conversion of inches to cm", () => {
    expect(distanceInchesToCm(2.540000000003708)).toEqual(0.9999999999999999);
    expect(distanceInchesToCm(1)).toEqual(0.393700787401);
    expect(distanceInchesToCm(100)).toEqual(39.3700787401);
    // @ts-ignore
    expect(() => distanceInchesToCm('nope', true)).toThrow(`distanceInchesToCm: ${TWO_NUM_ERROR}`);
  });

  it("should call 'distanceYardsToMiles' and return conversion of yards to miles", () => {
    expect(distanceYardsToMiles(1760)).toEqual(1);
    expect(distanceYardsToMiles(1)).toEqual(0.0005681818181818182);
    expect(distanceYardsToMiles(3520)).toEqual(2);
    // @ts-ignore
    expect(() => distanceYardsToMiles('nope', true)).toThrow(
      `distanceYardsToMiles: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMilesToYards' and return conversion of miles to yards", () => {
    expect(distanceMilesToYards(1)).toEqual(1760);
    expect(distanceMilesToYards(0.0005681818181818182)).toEqual(1);
    expect(distanceMilesToYards(2)).toEqual(3520);
    // @ts-ignore
    expect(() => distanceMilesToYards('nope', true)).toThrow(
      `distanceMilesToYards: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceFeetToMiles' and return conversion of feet to miles", () => {
    expect(distanceFeetToMiles(1)).toEqual(0.0001893939393939394);
    expect(distanceFeetToMiles(5280)).toEqual(1);
    expect(distanceFeetToMiles(10560)).toEqual(2);
    // @ts-ignore
    expect(() => distanceFeetToMiles('nope', true)).toThrow(
      `distanceFeetToMiles: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMilesToFeet' and return conversion of feet to miles", () => {
    expect(distanceMilesToFeet(1)).toEqual(5280);
    expect(distanceMilesToFeet(0.0001893939393939394)).toEqual(1);
    expect(distanceMilesToFeet(2)).toEqual(10560);
    // @ts-ignore
    expect(() => distanceMilesToFeet('nope', true)).toThrow(
      `distanceMilesToFeet: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceFeetToIntlNauticalMi' and return conversion of feet to intl nautical miles", () => {
    expect(distanceFeetToIntlNauticalMi(1)).toEqual(0.00016457883369330452);
    expect(distanceFeetToIntlNauticalMi(6076.115485564)).toEqual(0.9999999999999498);
    expect(distanceFeetToIntlNauticalMi(12000)).toEqual(1.9749460043196543);
    // @ts-ignore
    expect(() => distanceFeetToIntlNauticalMi('nope', true)).toThrow(
      `distanceFeetToIntlNauticalMi: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceIntlNauticalMiToFeet' and return conversion of feet to intl nautical miles", () => {
    expect(distanceIntlNauticalMiToFeet(1)).toEqual(6076.115485564304);
    expect(distanceIntlNauticalMiToFeet(0.00016457883369330452)).toEqual(0.9999999999999999);
    expect(distanceIntlNauticalMiToFeet(1.9749460043196543)).toEqual(11999.999999999998);
    // @ts-ignore
    expect(() => distanceIntlNauticalMiToFeet('nope', true)).toThrow(
      `distanceIntlNauticalMiToFeet: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceIntlNauticalMiToMeters' and return conversion of feet to intl nautical miles", () => {
    expect(distanceIntlNauticalMiToMeters(1)).toEqual(1852);
    expect(distanceIntlNauticalMiToMeters(0.000539957)).toEqual(1.000000364);
    expect(distanceIntlNauticalMiToMeters(2.5)).toEqual(4630);
    // @ts-ignore
    expect(() => distanceIntlNauticalMiToMeters('nope', true)).toThrow(
      `distanceIntlNauticalMiToMeters: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceIntlNauticalMiToMiles' and return conversion of feet to intl nautical miles", () => {
    expect(distanceIntlNauticalMiToMiles(1)).toEqual(1.1507794480235425);
    expect(distanceIntlNauticalMiToMiles(0.868976242)).toEqual(1.0000000001143323);
    expect(distanceIntlNauticalMiToMiles(2.5)).toEqual(2.8769486200588563);
    // @ts-ignore
    expect(() => distanceIntlNauticalMiToMiles('nope', true)).toThrow(
      `distanceIntlNauticalMiToMiles: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMetersToIntlNauticalMi' and return conversion of feet to intl nautical miles", () => {
    expect(distanceMetersToIntlNauticalMi(1)).toEqual(0.0005399568034557236);
    expect(distanceMetersToIntlNauticalMi(1852)).toEqual(1);
    expect(distanceMetersToIntlNauticalMi(4630)).toEqual(2.5);
    // @ts-ignore
    expect(() => distanceMetersToIntlNauticalMi('nope', true)).toThrow(
      `distanceMetersToIntlNauticalMi: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMilesToIntlNauticalMi' and return conversion of feet to intl nautical miles", () => {
    expect(distanceMilesToIntlNauticalMi(1)).toEqual(0.8689762419006479);
    expect(distanceMilesToIntlNauticalMi(1.1507794480235425)).toEqual(1);
    expect(distanceMilesToIntlNauticalMi(2.8769486200588563)).toEqual(2.5);
    // @ts-ignore
    expect(() => distanceMilesToIntlNauticalMi('nope', true)).toThrow(
      `distanceMilesToIntlNauticalMi: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceFeetToUkNauticalMi' and return conversion of feet to UK nautical miles", () => {
    expect(distanceFeetToUkNauticalMi(1)).toEqual(0.00016447368421052634);
    expect(distanceFeetToUkNauticalMi(6080)).toEqual(1.0000000000000002);
    expect(distanceFeetToUkNauticalMi(12160)).toEqual(2.0000000000000004);
    // @ts-ignore
    expect(() => distanceFeetToUkNauticalMi('nope', true)).toThrow(
      `distanceFeetToUkNauticalMi: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMetersToUkNauticalMi' and return conversion of meters to UK nautical miles", () => {
    expect(distanceMetersToUkNauticalMi(1)).toEqual(0.0005396118248376848);
    expect(distanceMetersToUkNauticalMi(1853.184)).toEqual(1);
    expect(distanceMetersToUkNauticalMi(3706.368)).toEqual(2);
    // @ts-ignore
    expect(() => distanceMetersToUkNauticalMi('nope', true)).toThrow(
      `distanceMetersToUkNauticalMi: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceMilesToUkNauticalMi' and return conversion of miles to UK nautical miles", () => {
    expect(distanceMilesToUkNauticalMi(1)).toEqual(0.86842105263157903421);
    expect(distanceMilesToUkNauticalMi(1.1515151515151514)).toEqual(1);
    expect(distanceMilesToUkNauticalMi(2.3030303030303028)).toEqual(2);
    // @ts-ignore
    expect(() => distanceMilesToUkNauticalMi('nope', true)).toThrow(
      `distanceMilesToUkNauticalMi: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceUkNauticalMiToFeet' and return conversion of UK nautical miles to feet", () => {
    expect(distanceUkNauticalMiToFeet(1.0000000000000002)).toEqual(6080);
    expect(distanceUkNauticalMiToFeet(0.00016447368421052634)).toEqual(1);
    expect(distanceUkNauticalMiToFeet(2.0000000000000004)).toEqual(12160);
    // @ts-ignore
    expect(() => distanceUkNauticalMiToFeet('nope', true)).toThrow(
      `distanceUkNauticalMiToFeet: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceUkNauticalMiToMeters' and return conversion of UK nautical miles to feet", () => {
    expect(distanceUkNauticalMiToMeters(1)).toEqual(1853.184);
    expect(distanceUkNauticalMiToMeters(0.0005396118248376848)).toEqual(1);
    expect(distanceUkNauticalMiToMeters(2)).toEqual(3706.368);
    // @ts-ignore
    expect(() => distanceUkNauticalMiToMeters('nope', true)).toThrow(
      `distanceUkNauticalMiToMeters: ${TWO_NUM_ERROR}`
    );
  });

  it("should call 'distanceUkNauticalMiToMiles' and return conversion of UK nautical miles to feet", () => {
    expect(distanceUkNauticalMiToMiles(1)).toEqual(1.1515151515151514);
    expect(distanceUkNauticalMiToMiles(0.86842105263157903421)).toEqual(1);
    expect(distanceUkNauticalMiToMiles(2)).toEqual(2.3030303030303028);
    // @ts-ignore
    expect(() => distanceUkNauticalMiToMiles('nope', true)).toThrow(
      `distanceUkNauticalMiToMiles: ${TWO_NUM_ERROR}`
    );
  });
});
