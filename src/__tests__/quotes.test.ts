import {
  quotesRemoveSingle,
  quotesRetrieveSingle,
  quotesSwapSingle,
  quotesDoubleToSingle,
  quotesSingleToDouble,
  quotesRemoveDouble,
  quotesRetrieveDouble,
  quotesSwapDouble
} from '../';

describe('quotesUtils', () => {
  it("should call 'quotesRemoveSingle' and remove content inside single quotes from string and returns string", () => {
    expect(quotesRemoveSingle("this is a test 'and this should go away'")).toBe('this is a test');
    expect(quotesRemoveSingle("with only one quote ' it should do nothing")).toBe(
      "with only one quote ' it should do nothing"
    );
    expect(() => {
      // @ts-ignore
      quotesRemoveSingle(false);
    }).toThrow('quotesRemoveSingle: ARGUMENT MUST BE A STRING.');
  });

  it("should call 'quotesRemoveDouble' and remove content inside double quotes from string and returns string", () => {
    expect(quotesRemoveDouble('this is a test "and this should go away"')).toBe('this is a test');
    expect(quotesRemoveDouble('with only one quote " it should do nothing')).toBe(
      'with only one quote " it should do nothing'
    );
    expect(() => {
      // @ts-ignore
      quotesRemoveDouble(false);
    }).toThrow('quotesRemoveDouble: ARGUMENT MUST BE A STRING.');
  });

  it("should call 'quotesRetrieveSingle' and return array of what is inside single quotes", () => {
    expect(quotesRetrieveSingle("this 'is a test' of the 'inside function'")[1]).toBe(
      'inside function'
    );
    expect(
      quotesRetrieveSingle(
        "this 'is a test' of the 'inside function without a second closing single quote"
      )[1]
    ).toBe(undefined);
    expect(() => {
      // @ts-ignore
      quotesRetrieveSingle(false);
    }).toThrow('quotesRetrieveSingle: ARGUMENT MUST BE A STRING.');
  });

  it("should call 'quotesRetrieveDouble' and return array of what is inside single quotes", () => {
    expect(quotesRetrieveDouble('this "is a test" of the "inside function"')[1]).toBe(
      'inside function'
    );
    expect(
      quotesRetrieveDouble(
        'this "is a test" of the "inside function without a second closing double quote'
      )[1]
    ).toBe(undefined);
    expect(() => {
      // @ts-ignore
      quotesRetrieveDouble(false);
    }).toThrow('quotesRetrieveDouble: ARGUMENT MUST BE A STRING.');
  });

  it("should call 'quotesSwapSingle' and take 2 strings, swaps anything in single quotes INCLUDING THE SINGLE QUOTES from the first string with the second string", () => {
    expect(quotesSwapSingle("replaces things 'inside' quotes with other things", 'within')).toBe(
      'replaces things within quotes with other things'
    );
    expect(quotesSwapSingle("replaces things 'inside' quotes with other things", "'within'")).toBe(
      "replaces things 'within' quotes with other things"
    );
    // @ts-ignore
    expect(quotesSwapSingle("replaces things 'inside' quotes with other things", 5)).toBe(
      'replaces things 5 quotes with other things'
    );
    expect(() => {
      // @ts-ignore
      quotesSwapSingle(false);
    }).toThrow('quotesSwapSingle: ARGUMENT MUST BE A STRING.');
  });

  it("should call 'quotesSwapDouble' and take 2 strings, swaps anything in double quotes INCLUDING THE DOUBLE QUOTES from the first string with the second string", () => {
    expect(quotesSwapDouble('replaces things "inside" quotes with other things', 'within')).toBe(
      'replaces things within quotes with other things'
    );
    expect(quotesSwapDouble('replaces things "inside" quotes with other things', '"within"')).toBe(
      'replaces things "within" quotes with other things'
    );
    // @ts-ignore
    expect(quotesSwapDouble('replaces things "inside" quotes with other things', 5)).toBe(
      'replaces things 5 quotes with other things'
    );
    expect(() => {
      // @ts-ignore
      quotesSwapDouble(false);
    }).toThrow('quotesSwapDouble: ARGUMENT MUST BE A STRING.');
  });

  it('should call "quotesDoubleToSingle" and swap and double quotes for single quotes', () => {
    expect(quotesDoubleToSingle('this "is a" test')).toBe("this 'is a' test");
    expect(quotesDoubleToSingle('this "is a" test and "it is slightly" more complex')).toBe(
      "this 'is a' test and 'it is slightly' more complex"
    );
    expect(quotesDoubleToSingle('this "is a test')).toBe("this 'is a test");
    expect(() => {
      // @ts-ignore
      quotesDoubleToSingle(false);
    }).toThrow('quotesDoubleToSingle: ARGUMENT MUST BE A STRING.');
  });

  it('should call "quotesSingleToDouble" and swap and single quotes for double quotes', () => {
    expect(quotesSingleToDouble("this 'is a' test")).toBe('this "is a" test');
    expect(quotesSingleToDouble("this 'is a' test and 'it is slightly' more complex")).toBe(
      'this "is a" test and "it is slightly" more complex'
    );
    expect(quotesSingleToDouble("this 'is a test")).toBe('this "is a test');
    expect(() => {
      // @ts-ignore
      quotesSingleToDouble(false);
    }).toThrow('quotesSingleToDouble: ARGUMENT MUST BE A STRING.');
  });
});
