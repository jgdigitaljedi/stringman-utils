import { stringIsValid, stringRemove, stringRetrieve, stringSwap } from '../lib/stringUtils';
import { emailRegex, semverStrRegex, urlStrRegex } from '../util/constants/regex.constants';

describe('stringUtils', () => {
  it('should call isValid, take a string and RegEx, and return a boolean indicating of the string tests for the RegEx', () => {
    expect(stringIsValid('test@test.com', emailRegex)).toBe(true);
    expect(stringIsValid('test@test.', emailRegex)).toBe(false);
    expect(stringIsValid('https://www.', urlStrRegex)).toBe(false);
    expect(stringIsValid('https://www.test.com', urlStrRegex)).toBe(true);
    expect(stringIsValid('1.2.3', semverStrRegex)).toBe(true);
    expect(stringIsValid('1.2.', semverStrRegex)).toBe(false);
  });

  it('should call swap, take a string to find and a string to replace it with as well as a regex string to test for the part to be replace, and return a string with the replacement', () => {
    expect(stringSwap('my email is test@test.com', 'none@none.com', emailRegex)).toEqual(
      'my email is none@none.com'
    );
    expect(stringSwap('in version 1.0.0 I added stuff', '1.1.0', semverStrRegex)).toEqual(
      'in version 1.1.0 I added stuff'
    );
    expect(
      stringSwap('check out my site at www.whateverTF.com', 'notarealsite.com', urlStrRegex)
    ).toEqual('check out my site at notarealsite.com');
    expect(stringSwap('', 'www.test.com', urlStrRegex)).toEqual('');
  });

  it('should call remove, take a string and a RegExp to test for the part to remove, and return the string with the part removed', () => {
    expect(stringRemove('my email is test@test.com', emailRegex)).toEqual('my email is');
    expect(stringRemove('in version 1.0.0 I added stuff', semverStrRegex)).toEqual(
      'in version  I added stuff'
    );
    expect(stringRemove('check out my site at www.whateverTF.com', urlStrRegex)).toEqual(
      'check out my site at'
    );
    // @ts-ignore
    expect(stringRemove(4, urlStrRegex)).toEqual(4);
  });

  it('should call retrieve, take a string and a RegExp string to test for retrieval, and return an array of matches from the RegExp', () => {
    expect(stringRetrieve('my email is test@test.com', emailRegex)).toEqual(['test@test.com']);
    expect(
      stringRetrieve('going from 1.9.5 to 2.0.0 is a breaking change', semverStrRegex)
    ).toEqual(['1.9.5', '2.0.0']);
    expect(stringRetrieve('check out my site at www.whateverTF.com', urlStrRegex)).toEqual([
      'www.whateverTF.com'
    ]);
    // @ts-ignore
    expect(stringRetrieve(4, urlStrRegex)).toEqual([]);
  });
});
