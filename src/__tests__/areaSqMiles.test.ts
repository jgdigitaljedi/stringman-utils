import { TWO_NUM_ERROR } from '../util/errors';
import {
  areaHectaresToSquareMiles,
  areaSectionsToSquareMiles,
  areaSquareChainsToSquareMiles,
  areaSquareFeetToSquareMiles,
  areaSquareKilometersToSquareMiles,
  areaSquareLinksGunToSquareMiles,
  areaSquareMetersToSquareMiles,
  areaSquareMilesToHectares,
  areaSquareMilesToSections,
  areaSquareMilesToSquareChains,
  areaSquareMilesToSquareFeet,
  areaSquareMilesToSquareKilometers,
  areaSquareMilesToSquareLinksGun,
  areaSquareMilesToSquareMeters,
  areaSquareMilesToSquareRods,
  areaSquareMilesToSquareYards,
  areaSquareMilesToTownships,
  areaSquareRodsToSquareMiles,
  areaSquareYardsToSquareMiles,
  areaTownshipsToSquareMiles
} from '../index';

describe('area land unit utils', () => {
  it('should call areaSquareMilesToSquareYards, take a number for sq miles and optional arg for string return, and return the value converted to sq yds', () => {
    expect(areaSquareMilesToSquareYards(1)).toBe(3097600);
    expect(areaSquareMilesToSquareYards(2, true)).toBe('6195200');
    expect(areaSquareMilesToSquareYards('3.75', true)).toBe('11616000');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareYards('nope', true)).toThrow(
      `areaSquareMilesToSquareYards: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareYardsToSquareMiles, take a number for sq yds and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaSquareYardsToSquareMiles(3097600)).toBe(1);
    expect(areaSquareYardsToSquareMiles(6195200, true)).toBe('2');
    expect(areaSquareYardsToSquareMiles('11616000', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareYardsToSquareMiles('nope', true)).toThrow(
      `areaSquareYardsToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToHectares, take a number for sq miles and optional arg for string return, and return the value converted to hectares', () => {
    expect(areaSquareMilesToHectares(1)).toBe(258.9988110336);
    expect(areaSquareMilesToHectares(2, true)).toBe('517.9976220672');
    expect(areaSquareMilesToHectares('3.75', true)).toBe('971.245541376');
    // @ts-ignore
    expect(() => areaSquareMilesToHectares('nope', true)).toThrow(
      `areaSquareMilesToHectares: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaHectaresToSquareMiles, take a number for hectares and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaHectaresToSquareMiles(258.9988110336)).toBe(1);
    expect(areaHectaresToSquareMiles(517.9976220672, true)).toBe('2');
    expect(areaHectaresToSquareMiles('971.245541376', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaHectaresToSquareMiles('nope', true)).toThrow(
      `areaHectaresToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSquareFeet, take a number for sq miles and optional arg for string return, and return the value converted to sq ft', () => {
    expect(areaSquareMilesToSquareFeet(1)).toBe(27878400);
    expect(areaSquareMilesToSquareFeet(2, true)).toBe('55756800');
    expect(areaSquareMilesToSquareFeet('3.75', true)).toBe('104544000');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareFeet('nope', true)).toThrow(
      `areaSquareMilesToSquareFeet: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareFeetToSquareMiles, take a number for sq ft and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaSquareFeetToSquareMiles(27878400)).toBe(1);
    expect(areaSquareFeetToSquareMiles(55756800, true)).toBe('2');
    expect(areaSquareFeetToSquareMiles('104544000', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareFeetToSquareMiles('nope', true)).toThrow(
      `areaSquareFeetToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaTownshipsToSquareMiles, take a number for townships and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaTownshipsToSquareMiles(1)).toBe(36);
    expect(areaTownshipsToSquareMiles(2, true)).toBe('72');
    expect(areaTownshipsToSquareMiles('3.75', true)).toBe('135');
    // @ts-ignore
    expect(() => areaTownshipsToSquareMiles('nope', true)).toThrow(
      `areaTownshipsToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToTownships, take a number for sq miles and optional arg for string return, and return the value converted to townships', () => {
    expect(areaSquareMilesToTownships(36)).toBe(1);
    expect(areaSquareMilesToTownships(72, true)).toBe('2');
    expect(areaSquareMilesToTownships('135', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareMilesToTownships('nope', true)).toThrow(
      `areaSquareMilesToTownships: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSquareMeters, take a number for sq miles and optional arg for string return, and return the value converted to sq meters', () => {
    expect(areaSquareMilesToSquareMeters(1)).toBe(2589988.110336);
    expect(areaSquareMilesToSquareMeters(2, true)).toBe('5179976.220672');
    expect(areaSquareMilesToSquareMeters('3.75', true)).toBe('9712455.41376');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareMeters('nope', true)).toThrow(
      `areaSquareMilesToSquareMeters: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMetersToSquareMiles, take a number for sq meters and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaSquareMetersToSquareMiles(2589988.110336)).toBe(1);
    expect(areaSquareMetersToSquareMiles(5179976.220672, true)).toBe('2');
    expect(areaSquareMetersToSquareMiles('9712455.41376', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareMetersToSquareMiles('nope', true)).toThrow(
      `areaSquareMetersToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSquareRods, take a number for sq miles and optional arg for string return, and return the value converted to sq rods', () => {
    expect(areaSquareMilesToSquareRods(1)).toBe(102400);
    expect(areaSquareMilesToSquareRods(2, true)).toBe('204800');
    expect(areaSquareMilesToSquareRods('3.75', true)).toBe('384000');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareRods('nope', true)).toThrow(
      `areaSquareMilesToSquareRods: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareRodsToSquareMiles, take a number for sq rods and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaSquareRodsToSquareMiles(102400)).toBe(1);
    expect(areaSquareRodsToSquareMiles(204800, true)).toBe('2');
    expect(areaSquareRodsToSquareMiles('384000', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareRodsToSquareMiles('nope', true)).toThrow(
      `areaSquareRodsToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSquareLinksGun, take a number for sq miles and optional arg for string return, and return the value converted to sq links', () => {
    expect(areaSquareMilesToSquareLinksGun(1)).toBe(64000000.889576);
    expect(areaSquareMilesToSquareLinksGun(2, true)).toBe('128000001.779152');
    expect(areaSquareMilesToSquareLinksGun('3.75', true)).toBe('240000003.33591');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareLinksGun('nope', true)).toThrow(
      `areaSquareMilesToSquareLinksGun: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareLinksGunToSuqareMiles, take a number for sq links and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaSquareLinksGunToSquareMiles(64000000)).toBe(0.9999999861003752);
    expect(areaSquareLinksGunToSquareMiles(128000000, true)).toBe(
      '1.999999972200750386399144910447'
    );
    expect(areaSquareLinksGunToSquareMiles('240000000', true)).toBe(
      '3.749999947876406974498396707088'
    );
    // @ts-ignore
    expect(() => areaSquareLinksGunToSquareMiles('nope', true)).toThrow(
      `areaSquareLinksGunToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSquareChains, take a number for sq miles and optional arg for string return, and return the value converted to sq chains', () => {
    expect(areaSquareMilesToSquareChains(1)).toBe(6400);
    expect(areaSquareMilesToSquareChains(2, true)).toBe('12800');
    expect(areaSquareMilesToSquareChains('3.75', true)).toBe('24000');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareChains('nope', true)).toThrow(
      `areaSquareMilesToSquareChains: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareChainsToSquareMiles, take a number for sq miles and optional arg for string return, and return the value converted to sq chains', () => {
    expect(areaSquareChainsToSquareMiles(6400)).toBe(1);
    expect(areaSquareChainsToSquareMiles(12800, true)).toBe('2');
    expect(areaSquareChainsToSquareMiles('24000', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareChainsToSquareMiles('nope', true)).toThrow(
      `areaSquareChainsToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareMiles, take a number for sections and optional arg for string return, and return the value converted to sq miles', () => {
    expect(areaSectionsToSquareMiles(1)).toBe(1.0000000001297);
    expect(areaSectionsToSquareMiles(2, true)).toBe('2.0000000002594');
    expect(areaSectionsToSquareMiles('3.75', true)).toBe('3.750000000486375');
    // @ts-ignore
    expect(() => areaSectionsToSquareMiles('nope', true)).toThrow(
      `areaSectionsToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSections, take a number for sq miles and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareMilesToSections(1.0000000001297)).toBe(1);
    expect(areaSquareMilesToSections(2.0000000002594, true)).toBe('2');
    expect(areaSquareMilesToSections('3.750000000486375', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareMilesToSections('nope', true)).toThrow(
      `areaSquareMilesToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToSquareKilometers, take a number for sq miles and optional arg for string return, and return the value converted to sq km', () => {
    expect(areaSquareMilesToSquareKilometers(1)).toBe(2.5899881103360003);
    expect(areaSquareMilesToSquareKilometers(2, true)).toBe('5.1799762206720006');
    expect(areaSquareMilesToSquareKilometers('3.75', true)).toBe('9.712455413760001125');
    // @ts-ignore
    expect(() => areaSquareMilesToSquareKilometers('nope', true)).toThrow(
      `areaSquareMilesToSquareKilometers: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareKilometersToSquareMiles, take a number for sq miles and optional arg for string return, and return the value converted to sq km', () => {
    expect(areaSquareKilometersToSquareMiles(2.5899881103360003)).toBe(1);
    expect(areaSquareKilometersToSquareMiles(5.1799762206720006, true)).toBe(
      '2.000000000000000154440863416978'
    );
    expect(areaSquareKilometersToSquareMiles('9.712455413760001125', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareKilometersToSquareMiles('nope', true)).toThrow(
      `areaSquareKilometersToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });
});
