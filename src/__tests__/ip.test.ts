import { ipRetrieve, ipRemove, ipSwap, ipIsValid } from '../';

describe('ipUtils', () => {
  it("should call 'isValid' and determine whether string is valid IP address", () => {
    expect(ipIsValid('192.168.0.1')).toBe(true);
    expect(ipIsValid('0.0.0.0')).toBe(true);
    expect(ipIsValid('255.255.255.256')).toBe(false);
    expect(ipIsValid('192.168.0.1.1')).toBe(false);
    expect(ipIsValid('192.168.0')).toBe(false);
    // @ts-ignore
    expect(ipIsValid(true)).toBe(false);
    // @ts-ignore
    expect(ipIsValid(15.15)).toBe(false);
  });

  it("should call 'retrieve' and get ip address from string", () => {
    expect(ipRetrieve('router at 192.168.0.1')[0]).toBe('192.168.0.1');
    expect(ipRetrieve('router at 192.168.0.1.1')[0]).toBe('192.168.0.1');
  });

  it("should call 'swap' and swap ip for a string", () => {
    expect(ipSwap('router at 192.168.0.1', '***.***.***.***')).toBe('router at ***.***.***.***');
  });

  it("should call 'remove' and remove ip address from string", () => {
    expect(ipRemove('router at 192.168.0.1')).toBe('router at');
  });
});
