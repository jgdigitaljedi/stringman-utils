import {
  weightGramsToKg,
  weightGramsToOz,
  weightKgToGrams,
  weightKgToLbs,
  weightKgToStone,
  weightKgToTonnes,
  weightKgToTons,
  weightLbsToKg,
  weightLbsToOz,
  weightLbsToStone,
  weightLbsToTonnes,
  weightLbsToTons,
  weightOzToGrams,
  weightOzToLbs,
  weightStoneToKg,
  weightStoneToLbs,
  weightTonnesToKg,
  weightTonnesToLbs,
  weightTonsToKg,
  weightTonsToLbs
} from '../lib/weightUtils';
import { TWO_NUM_ERROR } from '../util/errors';

describe('weightUtils', () => {
  it('should call weightKgToLbs, take value in kg as string or number, optional arg for number return, and return value converted to lbs', () => {
    expect(weightKgToLbs(1)).toBe(2.20462262185);
    expect(weightKgToLbs('1')).toBe(2.20462262185);
    expect(weightKgToLbs(5.67891, true)).toBe('12.5198534534501835');
    // @ts-ignore
    expect(() => weightKgToLbs('nope', true)).toThrow(`weightKgToLbs: ${TWO_NUM_ERROR}`);
  });

  it('should call weightLbsToKg, take value in lbs as string or number, optional arg for number return, and return value converted to kg', () => {
    expect(weightLbsToKg(2.20462262185)).toBe(1);
    expect(weightLbsToKg('2.20462262185')).toBe(1);
    expect(weightLbsToKg('5.67891', true)).toBe('2.575910245915269636967052062457');
    // @ts-ignore
    expect(() => weightLbsToKg('nope', true)).toThrow(`weightLbsToKg: ${TWO_NUM_ERROR}`);
  });

  it('should call weightOzToGrams, take value in oz as string or number, optional arg for number return, and return value converted to grams', () => {
    expect(weightOzToGrams(0.035274)).toBe(1);
    expect(weightOzToGrams('0.035274')).toBe(1);
    expect(weightOzToGrams('5.67891', true)).toBe('160.994216703521006973975165844531');
    // @ts-ignore
    expect(() => weightOzToGrams('nope', true)).toThrow(`weightOzToGrams: ${TWO_NUM_ERROR}`);
  });

  it('should call weightGramsToOz, take value in grams as string or number, optional arg for number return, and return value converted to oz', () => {
    expect(weightGramsToOz(28.349492544)).toBe(0.999999999997056);
    expect(weightGramsToOz('28.349492544')).toBe(0.999999999997056);
    expect(weightGramsToOz('5.67891', true)).toBe('0.20031787134');
    // @ts-ignore
    expect(() => weightGramsToOz('nope', true)).toThrow(`weightGramsToOz: ${TWO_NUM_ERROR}`);
  });

  it('should call weightOzToLbs, take value in oz as string or number, optional arg for number return, and return value converted to pounds', () => {
    expect(weightOzToLbs(16)).toBe(1);
    expect(weightOzToLbs('16')).toBe(1);
    expect(weightOzToLbs('5.67891', true)).toBe('0.354931875');
    // @ts-ignore
    expect(() => weightOzToLbs('nope', true)).toThrow(`weightOzToLbs: ${TWO_NUM_ERROR}`);
  });

  it('should call weightLbsToOz, take value in pounds as string or number, optional arg for number return, and return value converted to ounces', () => {
    expect(weightLbsToOz(0.0625)).toBe(1);
    expect(weightLbsToOz('0.0625')).toBe(1);
    expect(weightLbsToOz('5.67891', true)).toBe('90.86256');
    // @ts-ignore
    expect(() => weightLbsToOz('nope', true)).toThrow(`weightLbsToOz: ${TWO_NUM_ERROR}`);
  });

  it('should call weightGramsToKg, take value in grams as string or number, optional arg for number return, and return value converted to kg', () => {
    expect(weightGramsToKg(1000)).toBe(1);
    expect(weightGramsToKg('1000')).toBe(1);
    expect(weightGramsToKg('5.67891', true)).toBe('0.00567891');
    // @ts-ignore
    expect(() => weightGramsToKg('nope', true)).toThrow(`weightGramsToKg: ${TWO_NUM_ERROR}`);
  });

  it('should call weightKgToGrams, take value in kg as string or number, optional arg for number return, and return value converted to grams', () => {
    expect(weightKgToGrams(0.001)).toBe(1);
    expect(weightKgToGrams('0.001')).toBe(1);
    expect(weightKgToGrams('5.67891', true)).toBe('5678.91');
    // @ts-ignore
    expect(() => weightKgToGrams('nope', true)).toThrow(`weightKgToGrams: ${TWO_NUM_ERROR}`);
  });

  it('should call weightLbsToTons, take value in lbs as string or number, optional arg for number return, and return value converted to tons', () => {
    expect(weightLbsToTons(2000)).toBe(1);
    expect(weightLbsToTons('2000')).toBe(1);
    expect(weightLbsToTons('5.67891', true)).toBe('0.002839455');
    // @ts-ignore
    expect(() => weightLbsToTons('nope', true)).toThrow(`weightLbsToTons: ${TWO_NUM_ERROR}`);
  });

  it('should call weightTonsToLbs, take value in tons as string or number, optional arg for number return, and return value converted to lbs', () => {
    expect(weightTonsToLbs(0.0005)).toBe(1);
    expect(weightTonsToLbs('0.0005')).toBe(1);
    expect(weightTonsToLbs('5.67891', true)).toBe('11357.82');
    // @ts-ignore
    expect(() => weightTonsToLbs('nope', true)).toThrow(`weightTonsToLbs: ${TWO_NUM_ERROR}`);
  });

  it('should call weightKgToTonnes, take value in kg as string or number, optional arg for number return, and return value converted to metric tonnes', () => {
    expect(weightKgToTonnes(1000)).toBe(1);
    expect(weightKgToTonnes('1000')).toBe(1);
    expect(weightKgToTonnes('5.67891', true)).toBe('0.00567891');
    // @ts-ignore
    expect(() => weightKgToTonnes('nope', true)).toThrow(`weightKgToTonnes: ${TWO_NUM_ERROR}`);
  });

  it('should call weightTonnesToKg, take value in metric tonnes as string or number, optional arg for number return, and return value converted to kg', () => {
    expect(weightTonnesToKg(0.001)).toBe(1);
    expect(weightTonnesToKg('0.001')).toBe(1);
    expect(weightTonnesToKg('5.67891', true)).toBe('5678.91');
    // @ts-ignore
    expect(() => weightTonnesToKg('nope', true)).toThrow(`weightTonnesToKg: ${TWO_NUM_ERROR}`);
  });

  it('should call weightKgToTons, take value in kg as string or number, optional arg for number return, and return value converted to tons', () => {
    expect(weightKgToTons(907.184739999)).toBe(0.999999999999453);
    expect(weightKgToTons('907.184739999')).toBe(0.999999999999453);
    expect(weightKgToTons('5.67891', true)).toBe('0.00625992672672509175');
    // @ts-ignore
    expect(() => weightKgToTons('nope', true)).toThrow(`weightKgToTons: ${TWO_NUM_ERROR}`);
  });

  it('should call weightTonsToKg, take value in tons as string or number, optional arg for number return, and return value converted to kg', () => {
    expect(weightTonsToKg(0.999999999999453)).toBe(907.184739999);
    expect(weightTonsToKg('0.999999999999453')).toBe(907.184739999);
    expect(weightTonsToKg('5.67891', true)).toBe('5151.820491830539273934104124914543');
    // @ts-ignore
    expect(() => weightTonsToKg('nope', true)).toThrow(`weightTonsToKg: ${TWO_NUM_ERROR}`);
  });

  it('should call weightLbsToTonnes, take value in lbs as string or number, optional arg for number return, and return value converted to metric tonnes', () => {
    expect(weightLbsToTonnes(2204.62262185)).toBe(1);
    expect(weightLbsToTonnes('2204.62262185')).toBe(1);
    expect(weightLbsToTonnes('5.67891', true)).toBe('0.002575910245915269636967052062');
    // @ts-ignore
    expect(() => weightLbsToTonnes('nope', true)).toThrow(`weightLbsToTonnes: ${TWO_NUM_ERROR}`);
  });

  it('should call weightTonnesToLbs, take value in metric tonnes as string or number, optional arg for number return, and return value converted to metric lbs', () => {
    expect(weightTonnesToLbs(1)).toBe(2204.62262185);
    expect(weightTonnesToLbs('1')).toBe(2204.62262185);
    expect(weightTonnesToLbs('5.67891', true)).toBe('12519.8534534501835');
    // @ts-ignore
    expect(() => weightTonnesToLbs('nope', true)).toThrow(`weightTonnesToLbs: ${TWO_NUM_ERROR}`);
  });

  it('should call weightLbsToStone, take value in pounds as string or number, optional arg for number return, and return value converted to stones', () => {
    expect(weightLbsToStone(14)).toBe(1);
    expect(weightLbsToStone('14')).toBe(1);
    expect(weightLbsToStone('5', true)).toBe('0.357142857142857142857142857143');
    // @ts-ignore
    expect(() => weightLbsToStone('nope', true)).toThrow(`weightLbsToStone: ${TWO_NUM_ERROR}`);
  });

  it('should call weightStoneToLbs, take value in stones as string or number, optional arg for number return, and return value converted to pounds', () => {
    expect(weightStoneToLbs(1)).toBe(14);
    expect(weightStoneToLbs('1')).toBe(14);
    expect(weightStoneToLbs('5', true)).toBe('70');
    // @ts-ignore
    expect(() => weightStoneToLbs('nope', true)).toThrow(`weightStoneToLbs: ${TWO_NUM_ERROR}`);
  });

  it('should call weightKgToStone, take value in kilograms as string or number, optional arg for number return, and return value converted to stones', () => {
    expect(weightKgToStone(6.350293179999988)).toBe(1);
    expect(weightKgToStone('6.350293179999988')).toBe(1);
    expect(weightKgToStone('5', true)).toBe('0.787365222088849990447632382573');
    // @ts-ignore
    expect(() => weightKgToStone('nope', true)).toThrow(`weightKgToStone: ${TWO_NUM_ERROR}`);
  });

  it('should call weightStoneToKg, take value in stones as string or number, optional arg for number return, and return value converted to kilograms', () => {
    expect(weightStoneToKg(1)).toBe(6.350293179999988);
    expect(weightStoneToKg('1')).toBe(6.350293179999988);
    expect(weightStoneToKg('5', true)).toBe('31.75146589999994');
    // @ts-ignore
    expect(() => weightStoneToKg('nope', true)).toThrow(`weightStoneToKg: ${TWO_NUM_ERROR}`);
  });
});
