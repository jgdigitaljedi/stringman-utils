import {
  precisionMatchGt,
  precisionMathSubtract,
  precisionMathAbsolute,
  precisionMathAdd,
  precisionMathDivide,
  precisionMathEquals,
  precisionMathExponential,
  precisionMathGte,
  precisionMathLt,
  precisionMathLte,
  precisionMathModulo,
  precisionMathMultiply,
  precisionMathPower,
  precisionMathSqrt
} from '../lib/precisionMathUtils';

describe('precisionMath', () => {
  it('should add numbers sent as string or number and return expected result', () => {
    expect(precisionMathAdd(150, 175)).toEqual(325);
    expect(precisionMathAdd('150', 175)).toEqual(325);
    expect(precisionMathAdd('150', '175', true)).toEqual('325');
  });

  it('should subtract numbers sent as string or number and return expected result', () => {
    expect(precisionMathSubtract(175, 150)).toEqual(25);
    expect(precisionMathSubtract('175', 150)).toEqual(25);
    expect(precisionMathSubtract('175', '150', true)).toEqual('25');
  });

  it('should multiply numbers sent as string or number and return expected result', () => {
    expect(precisionMathMultiply(5, 100)).toEqual(500);
    expect(precisionMathMultiply(5, '100', true)).toEqual('500');
    expect(precisionMathMultiply('5.5555555555555555555555555555555555555555', 100, true)).toEqual(
      '555.55555555555555555555555555555555555555'
    );
  });

  it('should divide numbers sent as string or number and return expected result', () => {
    expect(precisionMathDivide(150, 5)).toEqual(30);
    expect(precisionMathDivide('150', 5, true)).toEqual('30');
    expect(precisionMathDivide('1', '3', true)).toEqual('0.333333333333333333333333333333');
  });

  it('should divide numbers and return remainder (modulo) numbers sent as string or number and return expected result', () => {
    expect(1 % 0.9).toEqual(0.09999999999999998); // js sucks at math
    expect(precisionMathModulo(1, 0.9)).toEqual(0.1);
    expect(precisionMathModulo('100', 7, true)).toEqual('2');
  });

  it('should compare 2 numbers sent as string or number and return expected result of boolean for it first is greater than the second', () => {
    expect(0.1 > 0.3 - 0.2).toBe(true); // js sucks at math
    expect(precisionMatchGt(0.1, precisionMathSubtract(0.3, 0.2))).toBe(false);
    expect(precisionMatchGt('100', 7)).toBe(true);
    expect(precisionMatchGt('7', 100)).toBe(false);
  });

  it('should compare 2 numbers sent as string or number and return expected result of boolean for it first is greater or equal to the second', () => {
    expect(0.1 > 0.3 - 0.2).toBe(true); // js sucks at math
    expect(precisionMathGte(0.1, precisionMathSubtract(0.3, 0.2))).toBe(true);
    expect(precisionMathGte('100', 100)).toBe(true);
    expect(precisionMathGte('0.99999999999999', 1)).toBe(false);
  });

  it('should compare 2 numbers sent as string or number and return expected result of boolean for it first is less than the second', () => {
    expect(0.3 - 0.2 < 0.1).toBe(true); // js sucks at math
    expect(precisionMathLt(0.1, precisionMathSubtract(0.3, 0.2))).toBe(false);
    expect(precisionMathLt('100', 7)).toBe(false);
    expect(precisionMathLt('7', 100)).toBe(true);
  });

  it('should compare 2 numbers sent as string or number and return expected result of boolean for it first is less or equal to the second', () => {
    expect(0.3 - 0.2 < 0.1).toBe(true); // js sucks at math
    expect(precisionMathLte(0.1, precisionMathSubtract(0.3, 0.2))).toBe(true);
    expect(precisionMathLte('100', 7)).toBe(false);
    expect(precisionMathLte('1', 0.99999999999999999999)).toBe(true);
  });

  it('should compare 2 numbers sent as string or number and return expected result of boolean for it first is equal to the second', () => {
    expect(0 === 1e-324).toBe(true); // js sucks at math
    expect(precisionMathEquals(0, '1e-324')).toBe(false);
    expect(precisionMathEquals('-0', 0)).toBe(true);
  });

  it('should take number sent as string or number and return expected result of square root of that number', () => {
    expect(precisionMathSqrt(16)).toEqual(4);
    expect(precisionMathSqrt('3', true)).toEqual('1.732050807568877293527446341506');
  });

  it('should take 2 numbers sent as string or number and return expected result of the first number raised to the power of the second number', () => {
    expect(precisionMathPower(5, 3)).toEqual(125);
    expect(precisionMathPower('5', 3)).toEqual(125);
    expect(precisionMathPower(3, -2, true)).toEqual('0.111111111111111111111111111111');
  });

  it("should take a number sent as string or number and return expected result of the number's absolute value", () => {
    expect(precisionMathAbsolute(5)).toEqual(5);
    expect(precisionMathAbsolute('-5')).toEqual(5);
  });

  it('should take 2 numbers sent as string or number and return expected result of the first number in exponentional notation limited to the second numbers precision', () => {
    expect(precisionMathExponential(45.6)).toEqual('4.56e+1');
    expect(precisionMathExponential(45.6, 0)).toEqual('5e+1');
    expect(precisionMathExponential(45.6, 1)).toEqual('4.6e+1');
    expect(precisionMathExponential(45.6, 3)).toEqual('4.560e+1');
  });
});
