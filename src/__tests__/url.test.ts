import { urlGetDomain, urlIsValid, urlRemove, urlRetrieve, urlSwap } from '../lib/urlUtils';

describe('test various methods of "url"', () => {
  it("should call 'retrieve' and return url from string", () => {
    expect(urlRetrieve('my portfolio: https://joeyg.me')[0]).toBe('https://joeyg.me');
  });

  it("should call 'isValid' and verfy string is url", () => {
    expect(urlIsValid('https://joeyg.me')).toBe(true);
    expect(urlIsValid('not a url://')).toBe(false);
  });

  it("should call 'remove' and remove url from string", () => {
    expect(urlRemove('will it remove https://joeyg.me')).toBe('will it remove');
  });

  it("should call 'getDomain' and get the domain from a url string", () => {
    expect(urlGetDomain('https://www.google.com/test')[0]).toBe('google.com');
  });

  it("should call 'swap' and swap any URLS with second string", () => {
    expect(urlSwap('this is a test at https://test.com', 'https://www.fakesite.com')).toBe(
      'this is a test at https://www.fakesite.com'
    );
  });
});
