import { caseKebabCase, caseSnakeCase, caseSpaceCase, caseCamelCase, casePascalCase } from '..';
import { MUST_BE_STRING } from '../util/errors';

describe('case utils', () => {
  it('should call "caseKebabCase" and return string sent as kebab case', () => {
    expect(caseKebabCase('ThisIsATest')).toBe('this-is-a-test');
    expect(caseKebabCase('This Is A Test')).toBe('this-is-a-test');
    expect(caseKebabCase('This_Is_A_Test')).toBe('this-is-a-test');
    expect(caseKebabCase('this_Is_a_Test')).toBe('this-is-a-test');
    expect(() => {
      // @ts-ignore
      caseKebabCase(false);
    }).toThrow(`caseKebabCase: ${MUST_BE_STRING}`);
  });

  it('should call "caseSnakeCase" and return string sent as snake case', () => {
    expect(caseSnakeCase('ThisIsATest')).toBe('this_is_a_test');
    expect(caseSnakeCase('This Is A Test')).toBe('this_is_a_test');
    expect(caseSnakeCase('This-Is-A-Test')).toBe('this_is_a_test');
    expect(caseSnakeCase('this-Is-a-Test')).toBe('this_is_a_test');
    expect(() => {
      // @ts-ignore
      caseSnakeCase(false);
    }).toThrow(`caseSnakeCase: ${MUST_BE_STRING}`);
  });

  it('should call "caseSpaceCase" and return string sent as delimited by spaces', () => {
    expect(caseSpaceCase('ThisIsATest')).toBe('this is a test');
    expect(caseSpaceCase('This_Is_A_Test')).toBe('this is a test');
    expect(caseSpaceCase('This-Is-A-Test')).toBe('this is a test');
    expect(caseSpaceCase('this-Is-a-Test')).toBe('this is a test');
    expect(caseSpaceCase('thisIsATest')).toBe('this is a test');
    expect(() => {
      // @ts-ignore
      caseSpaceCase(false);
    }).toThrow(`caseSpaceCase: ${MUST_BE_STRING}`);
  });

  it('should call "caseCamelCase" and return string sent as camel case', () => {
    expect(caseCamelCase('ThisIsATest')).toBe('thisIsATest');
    expect(caseCamelCase('This_Is_A_Test')).toBe('thisIsATest');
    expect(caseCamelCase('This-Is-A-Test')).toBe('thisIsATest');
    expect(caseCamelCase('this-Is-a-Test')).toBe('thisIsATest');
    expect(caseCamelCase('thisIsATest')).toBe('thisIsATest');
    expect(() => {
      // @ts-ignore
      caseCamelCase(false);
    }).toThrow(`caseCamelCase: ${MUST_BE_STRING}`);
  });

  it('should call "casePascalCase" and return string sent as Pascal case', () => {
    expect(casePascalCase('ThisIsATest')).toBe('ThisIsATest');
    expect(casePascalCase('This_Is_A_Test')).toBe('ThisIsATest');
    expect(casePascalCase('This-Is-A-Test')).toBe('ThisIsATest');
    expect(casePascalCase('this-Is-a-Test')).toBe('ThisIsATest');
    expect(casePascalCase('thisIsATest')).toBe('ThisIsATest');
    expect(() => {
      // @ts-ignore
      casePascalCase(false);
    }).toThrow(`casePascalCase: ${MUST_BE_STRING}`);
  });
});
