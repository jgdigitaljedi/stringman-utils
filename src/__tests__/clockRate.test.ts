import {
  clockRateGhzToHz,
  clockRateGhzToKhz,
  clockRateGhzToMhz,
  clockRateHzToGhz,
  clockRateHzToKhz,
  clockRateHzToMhz,
  clockRateKhzToGhz,
  clockRateKhzToHz,
  clockRateKhzToMhz,
  clockRateMhzToGhz,
  clockRateMhzToHz,
  clockRateMhzToKhz
} from '..';
import { TWO_NUM_ERROR } from '../util/errors';

describe('clock rate utils', () => {
  it('should call "clockRateGhzToHz", take a number of GHz, and return that value in Hz', () => {
    expect(clockRateGhzToHz(1)).toEqual(1000000000);
    expect(clockRateGhzToHz(2, true)).toEqual('2000000000');
    expect(clockRateGhzToHz('2.345', true)).toEqual('2345000000');
    expect(() => {
      // @ts-ignore
      clockRateGhzToHz(false);
    }).toThrow(`clockRateGhzToHz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateHzToGhz" take a number of Hz, and return that value in GHz', () => {
    expect(clockRateHzToGhz(1000000000)).toEqual(1);
    expect(clockRateHzToGhz(2000000000, true)).toEqual('2');
    expect(clockRateHzToGhz('2345000000', true)).toEqual('2.345');
    expect(() => {
      // @ts-ignore
      clockRateHzToGhz(false);
    }).toThrow(`clockRateHzToGhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateGhzToKhz" take a number of GHz, and return that value in KHz', () => {
    expect(clockRateGhzToKhz(1)).toEqual(1000000);
    expect(clockRateGhzToKhz(2, true)).toEqual('2000000');
    expect(clockRateGhzToKhz('2.567', true)).toEqual('2567000');
    expect(() => {
      // @ts-ignore
      clockRateGhzToKhz(false);
    }).toThrow(`clockRateGhzToKhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateKhzToGhz" take a number of KHz, and return that value in GHz', () => {
    expect(clockRateKhzToGhz(1000000)).toEqual(1);
    expect(clockRateKhzToGhz(2000000, true)).toEqual('2');
    expect(clockRateKhzToGhz('2567000', true)).toEqual('2.567');
    expect(() => {
      // @ts-ignore
      clockRateKhzToGhz(false);
    }).toThrow(`clockRateKhzToGhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateGhzToMhz" take a number of GHz, and return that value in MHz', () => {
    expect(clockRateGhzToMhz(1)).toEqual(1000);
    expect(clockRateGhzToMhz(2, true)).toEqual('2000');
    expect(clockRateGhzToMhz('2.567', true)).toEqual('2567');
    expect(() => {
      // @ts-ignore
      clockRateGhzToMhz(false);
    }).toThrow(`clockRateGhzToMhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateMhzToGhz" take a number of MHz, and return that value in GHz', () => {
    expect(clockRateMhzToGhz(1000)).toEqual(1);
    expect(clockRateMhzToGhz(2000, true)).toEqual('2');
    expect(clockRateMhzToGhz('2567', true)).toEqual('2.567');
    expect(() => {
      // @ts-ignore
      clockRateMhzToGhz(false);
    }).toThrow(`clockRateMhzToGhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateHzToKhz" take a number of Hz, and return that value in KHz', () => {
    expect(clockRateHzToKhz(1000)).toEqual(1);
    expect(clockRateHzToKhz(2000, true)).toEqual('2');
    expect(clockRateHzToKhz('2567', true)).toEqual('2.567');
    expect(() => {
      // @ts-ignore
      clockRateHzToKhz(false);
    }).toThrow(`clockRateHzToKhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateKhzToHz" take a number of KHz, and return that value in Hz', () => {
    expect(clockRateKhzToHz(1)).toEqual(1000);
    expect(clockRateKhzToHz(2, true)).toEqual('2000');
    expect(clockRateKhzToHz('2.567', true)).toEqual('2567');
    expect(() => {
      // @ts-ignore
      clockRateKhzToHz(false);
    }).toThrow(`clockRateKhzToHz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateHzToMhz" take a number of Hz, and return that value in MHz', () => {
    expect(clockRateHzToMhz(1000000)).toEqual(1);
    expect(clockRateHzToMhz(2, true)).toEqual('0.000002');
    expect(clockRateHzToMhz('2567', true)).toEqual('0.002567');
    expect(() => {
      // @ts-ignore
      clockRateHzToMhz(false);
    }).toThrow(`clockRateHzToMhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateMhzToHz" take a number of MHz, and return that value in Hz', () => {
    expect(clockRateMhzToHz(1)).toEqual(1000000);
    expect(clockRateMhzToHz(0.002, true)).toEqual('2000');
    expect(clockRateMhzToHz('2.567', true)).toEqual('2567000');
    expect(() => {
      // @ts-ignore
      clockRateMhzToHz(false);
    }).toThrow(`clockRateMhzToHz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateKhzToMhz" take a number of KHz, and return that value in MHz', () => {
    expect(clockRateKhzToMhz(1)).toEqual(0.001);
    expect(clockRateKhzToMhz(0.002, true)).toEqual('0.000002');
    expect(clockRateKhzToMhz('2567', true)).toEqual('2.567');
    expect(() => {
      // @ts-ignore
      clockRateKhzToMhz(false);
    }).toThrow(`clockRateKhzToMhz: ${TWO_NUM_ERROR}`);
  });

  it('should call "clockRateMhzToKhz" take a number of MHz, and return that value in KHz', () => {
    expect(clockRateMhzToKhz(1)).toEqual(1000);
    expect(clockRateMhzToKhz(0.002, true)).toEqual('2');
    expect(clockRateMhzToKhz('2.567', true)).toEqual('2567');
    expect(() => {
      // @ts-ignore
      clockRateMhzToKhz(false);
    }).toThrow(`clockRateMhzToKhz: ${TWO_NUM_ERROR}`);
  });
});
