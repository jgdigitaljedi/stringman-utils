import { colorLuminance } from '..';
import {
  colorHexToHsl,
  colorHexToRgb,
  colorIsHexColor,
  colorRgbToHex,
  colorRgbToHsl
} from '../lib/colorsUtils';
import { NOT_VALID_HEX_COLOR, TWO_NUM_ERROR } from '../util/errors';

describe('colorUtils', () => {
  it("should call 'colorIsHexColor' and verify that string passed is hexidecimal color", () => {
    expect(colorIsHexColor('#ff00ff')).toBe(true);
    expect(colorIsHexColor('#fff')).toBe(true);
    expect(colorIsHexColor('nope')).toBe(false);
    // @ts-ignore
    expect(colorIsHexColor(5)).toBe(false);
  });

  it("should call 'colorRgbToHex' and convert rgb color to hex", () => {
    expect(colorRgbToHex(22, 33, 44)).toBe('#16212C');
    expect(colorRgbToHex('22', '33', '44')).toBe('#16212C');
    // @ts-ignore
    expect(() => colorRgbToHex('nope', 33, 44)).toThrow(`colorRgbToHex: ${TWO_NUM_ERROR}`);
  });

  it("should call 'colorHexToRgb' and convert hex color to rgb", () => {
    expect(colorHexToRgb('#00ff00')).toEqual([0, 255, 0]);
    expect(colorHexToRgb('00ff00')).toEqual([0, 255, 0]);
    expect(colorHexToRgb('#fff')).toEqual([255, 255, 255]);
    expect(colorHexToRgb('00')).toEqual([0, 0, 0]);
    expect(() => colorHexToRgb('nope')).toThrow(`colorHexToRgb: ${NOT_VALID_HEX_COLOR}`);
    expect(() => colorHexToRgb('0')).toThrow(`colorHexToRgb: ${NOT_VALID_HEX_COLOR}`);
    expect(() => colorHexToRgb('0000000')).toThrow(`colorHexToRgb: ${NOT_VALID_HEX_COLOR}`);
  });

  it("should call 'colorLuminance' and lighten or darken a color", () => {
    expect(colorLuminance('#63C6FF', 40)).toBe('#8affff');
    expect(colorLuminance('#63C6FF', -40)).toBe('#3b7699');
    expect(() => colorLuminance('nope', -40)).toThrow(`colorLuminance: ${NOT_VALID_HEX_COLOR}`);
  });

  it("should call 'colorHexToHsl' and convert hex color to hsl", () => {
    expect(colorHexToHsl('#63C6FF')!['h']).toBe(201.9); // {h: 201.9, s: 100, l: 69.4}
    expect(colorHexToHsl('#63C6FF')!['s']).toBe(100); // {h: 201.9, s: 100, l: 69.4}
    expect(colorHexToHsl('63C6FF')!['l']).toBe(69.4);
    expect(() => colorHexToHsl('nope')).toThrow(`colorHexToHsl: ${NOT_VALID_HEX_COLOR}`);
  });

  it("should call 'colorRgbToHsl' and convert rgb to hsl", () => {
    const hsl = colorRgbToHsl(99, 198, 255);
    expect(colorRgbToHsl(99, 198, 255)).toEqual({ h: 201.9, s: 100, l: 69.4 });
    expect(colorRgbToHsl(0, 0, 0)).toEqual({ h: 0, l: 0, s: 0 });
    expect(colorRgbToHsl(30, 20, 10)).toEqual({ h: 30, l: 7.8, s: 50 });
    expect(colorRgbToHsl(10, 30, 20)).toEqual({ h: 150, l: 7.8, s: 50 });
    // @ts-ignore
    expect(() => colorRgbToHsl('a', 'b', 'c')).toThrow(`colorRgbToHsl: ${TWO_NUM_ERROR}`);
    expect(() => colorRgbToHsl(101.5, 7, 55)).toThrow(
      'colorRgbToHsl: Arguments for RGB values must be whole numbers.'
    );
  });
});
