import {
  semverIsValid,
  semverRemove,
  semverRetrieve,
  semverSwap,
  semverIncrement,
  semverDecrement
} from '../lib/semverUtils';

describe('semverUtils', () => {
  it("should call 'retrieve' and return semver from string", () => {
    expect(semverRetrieve('this is a test v 10.7.8')[0]).toBe('10.7.8');
  });

  it("should call 'isValid' and verify string is semver", () => {
    expect(semverIsValid('10.7.8')).toBe(true);
    expect(semverIsValid('not semver 10')).toBe(false);
  });

  it("should call 'remove' and remove semver from string", () => {
    expect(semverRemove('remove this stuff 10.11.12')).toBe('remove this stuff');
  });

  it("should call 'swap' and take 2 strings and returns string with any semantic versions in the first string replaced by the second string", () => {
    expect(semverSwap('this project is version 1.0.1', '2.2.5')).toBe(
      'this project is version 2.2.5'
    );
  });

  it('should call "semverIncrement", take the args, and return the expected result', () => {
    expect(semverIncrement('1.0.0', 'major')).toEqual('2.0.0');
    expect(semverIncrement('9.9.9', 'major')).toEqual('10.9.9');
    expect(semverIncrement('1.0.0', 'minor')).toEqual('1.1.0');
    expect(semverIncrement('1.0.0', 'patch')).toEqual('1.0.1');

    // test error throw
    expect(() => {
      semverIncrement('1,0.0', 'patch');
    }).toThrow('THE SEMANTIC VERSION 1,0.0 CANNOT BE INCREMENTED, CHECK FORMAT ("x.x.x")');
  });

  it('should call "semverDecrement", take the args, and return the expected result', () => {
    expect(semverDecrement('1.1.1', 'major')).toEqual('0.1.1');
    expect(semverDecrement('1.1.1', 'minor')).toEqual('1.0.1');
    expect(semverDecrement('1.1.1', 'patch')).toEqual('1.1.0');

    // test error throw
    expect(() => {
      semverDecrement('1,0.0', 'patch');
    }).toThrow('THE SEMANTIC VERSION 1,0.0 CANNOT BE DECREMENTED, CHECK FORMAT ("x.x.x")');

    expect(() => {
      semverDecrement('0.1.1', 'major');
    }).toThrow('CANNOT DECREMENT MAJOR VERSION BELOW ZERO: 0.1.1');

    expect(() => {
      semverDecrement('1.0.1', 'minor');
    }).toThrow('CANNOT DECREMENT MINOR VERSION BELOW ZERO: 1.0.1');

    expect(() => {
      semverDecrement('1.1.0', 'patch');
    }).toThrow('CANNOT DECREMENT PATCH VERSION BELOW ZERO: 1.1.0');
  });
});
