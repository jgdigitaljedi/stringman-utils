import { TWO_NUM_ERROR } from '../util/errors';

import {
  areaSectionsToSquareChains,
  areaSquareChainsToSections,
  areaSectionsToSquareLinks,
  areaSquareLinksToSections,
  areaSectionsToSquareYards,
  areaSquareYardsToSections,
  areaSectionsToSquareMeters,
  areaSquareMetersToSections,
  areaSectionsToSquareKilometers,
  areaSquareKilometersToSections,
  areaSectionsToSquareFeet,
  areaSquareFeetToSections,
  areaSectionsToHectares,
  areaHectaresToSections,
  areaTownshipsToSections,
  areaSectionsToTownships,
  areaSectionsToSquareRods,
  areaSquareRodsToSections
} from '../index';

describe('area land unit utils (2nd file)', () => {
  it('should call areaSectionsToSquareChains, take a number for sections and optional arg for string return, and return the value converted to sq chains', () => {
    expect(areaSectionsToSquareChains(1)).toBe(6400.0000889576);
    expect(areaSectionsToSquareChains(2, true)).toBe('12800.0001779152');
    expect(areaSectionsToSquareChains('3.75', true)).toBe('24000.000333591');
    // @ts-ignore
    expect(() => areaSectionsToSquareChains('nope', true)).toThrow(
      `areaSectionsToSquareChains: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareChainsToSections, take a number for sq chains and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareChainsToSections(6400.0000889576)).toBe(1);
    expect(areaSquareChainsToSections(12800.0001779152, true)).toBe('2');
    expect(areaSquareChainsToSections('24000.000333591', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareChainsToSections('nope', true)).toThrow(
      `areaSquareChainsToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareLinks, take a number for sections and optional arg for string return, and return the value converted to sq links', () => {
    expect(areaSectionsToSquareLinks(1)).toBe(64000000.889576);
    expect(areaSectionsToSquareLinks(2, true)).toBe('128000001.779152');
    expect(areaSectionsToSquareLinks('3.75', true)).toBe('240000003.33591');
    // @ts-ignore
    expect(() => areaSectionsToSquareLinks('nope', true)).toThrow(
      `areaSectionsToSquareLinks: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareLinksToSections, take a number for sq links and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareLinksToSections(64000000.889576)).toBe(1);
    expect(areaSquareLinksToSections(128000001.779152, true)).toBe('2');
    expect(areaSquareLinksToSections('240000003.33591', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareLinksToSections('nope', true)).toThrow(
      `areaSquareLinksToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareYards, take a number for sections and optional arg for string return, and return the value converted to sq yards', () => {
    expect(areaSectionsToSquareYards(1)).toBe(3097612.4259347);
    expect(areaSectionsToSquareYards(2, true)).toBe('6195224.8518694');
    expect(areaSectionsToSquareYards('3.75', true)).toBe('11616046.597255125');
    // @ts-ignore
    expect(() => areaSectionsToSquareYards('nope', true)).toThrow(
      `areaSectionsToSquareYards: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareYardsToSections, take a number for sq yards and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareYardsToSections(3097612.4259347)).toBe(1);
    expect(areaSquareYardsToSections(6195224.8518694, true)).toBe('2');
    expect(areaSquareYardsToSections('11616046.597255125', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareYardsToSections('nope', true)).toThrow(
      `areaSquareYardsToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareMeters, take a number for sections and optional arg for string return, and return the value converted to sq meters', () => {
    expect(areaSectionsToSquareMeters(1)).toBe(2589998.5);
    expect(areaSectionsToSquareMeters(2, true)).toBe('5179997');
    expect(areaSectionsToSquareMeters('3.75', true)).toBe('9712494.375');
    // @ts-ignore
    expect(() => areaSectionsToSquareMeters('nope', true)).toThrow(
      `areaSectionsToSquareMeters: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMetersToSections, take a number for sq meters and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareMetersToSections(2589998.5)).toBe(1);
    expect(areaSquareMetersToSections(5179997, true)).toBe('2');
    expect(areaSquareMetersToSections('9712494.375', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareMetersToSections('nope', true)).toThrow(
      `areaSquareMetersToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareKilometers, take a number for sections and optional arg for string return, and return the value converted to sq km', () => {
    expect(areaSectionsToSquareKilometers(1)).toBe(2.5899985);
    expect(areaSectionsToSquareKilometers(2, true)).toBe('5.179997');
    expect(areaSectionsToSquareKilometers('3.75', true)).toBe('9.712494375');
    // @ts-ignore
    expect(() => areaSectionsToSquareKilometers('nope', true)).toThrow(
      `areaSectionsToSquareKilometers: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareKilometersToSections, take a number for sq km and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareKilometersToSections(2.5899985)).toBe(1);
    expect(areaSquareKilometersToSections(5.179997, true)).toBe('2');
    expect(areaSquareKilometersToSections('9.712494375', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareKilometersToSections('nope', true)).toThrow(
      `areaSquareKilometersToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareFeet, take a number for sections and optional arg for string return, and return the value converted to sq ft', () => {
    expect(areaSectionsToSquareFeet(1)).toBe(27878511.833413);
    expect(areaSectionsToSquareFeet(2, true)).toBe('55757023.666826');
    expect(areaSectionsToSquareFeet('3.75', true)).toBe('104544419.37529875');
    // @ts-ignore
    expect(() => areaSectionsToSquareFeet('nope', true)).toThrow(
      `areaSectionsToSquareFeet: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareFeetToSections, take a number for sq ft and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareFeetToSections(27878511.833413)).toBe(1);
    expect(areaSquareFeetToSections(55757023.666826, true)).toBe('2');
    expect(areaSquareFeetToSections('104544419.37529875', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareFeetToSections('nope', true)).toThrow(
      `areaSquareFeetToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToHectares, take a number for sections and optional arg for string return, and return the value converted to hectares', () => {
    expect(areaSectionsToHectares(1)).toBe(258.99985);
    expect(areaSectionsToHectares(2, true)).toBe('517.9997');
    expect(areaSectionsToHectares('3.75', true)).toBe('971.2494375');
    // @ts-ignore
    expect(() => areaSectionsToHectares('nope', true)).toThrow(
      `areaSectionsToHectares: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaHectaresToSections, take a number for hectares and optional arg for string return, and return the value converted to sections', () => {
    expect(areaHectaresToSections(258.99985)).toBe(1);
    expect(areaHectaresToSections(517.9997, true)).toBe('2');
    expect(areaHectaresToSections('971.2494375', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaHectaresToSections('nope', true)).toThrow(
      `areaHectaresToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaTownshipsToSections, take a number for townships and optional arg for string return, and return the value converted to sections', () => {
    expect(areaTownshipsToSections(1)).toBe(35.999999737308);
    expect(areaTownshipsToSections(2, true)).toBe('71.999999474616');
    expect(areaTownshipsToSections('3.75', true)).toBe('134.999999014905');
    // @ts-ignore
    expect(() => areaTownshipsToSections('nope', true)).toThrow(
      `areaTownshipsToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToTownships, take a number for sections and optional arg for string return, and return the value converted to townships', () => {
    expect(areaSectionsToTownships(35.999999737308)).toBe(1);
    expect(areaSectionsToTownships(71.999999474616, true)).toBe('2');
    expect(areaSectionsToTownships('134.999999014905', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSectionsToTownships('nope', true)).toThrow(
      `areaSectionsToTownships: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToSquareRods, take a number for sections and optional arg for string return, and return the value converted to sq rods', () => {
    expect(areaSectionsToSquareRods(1)).toBe(102400.00074721);
    expect(areaSectionsToSquareRods(2, true)).toBe('204800.00149442');
    expect(areaSectionsToSquareRods('3.75', true)).toBe('384000.0028020375');
    // @ts-ignore
    expect(() => areaSectionsToSquareRods('nope', true)).toThrow(
      `areaSectionsToSquareRods: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareRodsToSections, take a number for sq rods and optional arg for string return, and return the value converted to sections', () => {
    expect(areaSquareRodsToSections(102400.00074721)).toBe(1);
    expect(areaSquareRodsToSections(204800.00149442, true)).toBe('2');
    expect(areaSquareRodsToSections('384000.0028020375', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareRodsToSections('nope', true)).toThrow(
      `areaSquareRodsToSections: ${TWO_NUM_ERROR}`
    );
  });
});
