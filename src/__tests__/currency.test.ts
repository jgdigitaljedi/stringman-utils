import { currencySupportsIntl, CurrencyUtils } from '..';

const currencyUtilsUS = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
const currencyUtilsVN = new CurrencyUtils({ language: 'vi', country: 'VN' }, 'VND');

describe('currencyUtils.ts', () => {
  it('should call getLocale() and should return locale from window', () => {
    expect(currencyUtilsUS.getLocaleStr({ language: 'en', country: 'US' })).toEqual('en-US');
  });

  it('should call getCurrencySymbol() and should return expected result', () => {
    expect(currencyUtilsUS.getCurrencySymbol()).toEqual('$');

    expect(currencyUtilsVN.getCurrencySymbol()).toEqual('₫');
  });

  it('should call formatCurrencyDisplay() and should return the expected result', () => {
    expect(currencyUtilsUS.formatCurrencyDisplay(1000)).toEqual('$1,000.00');
    expect(currencyUtilsVN.formatCurrencyDisplay(1000)).toBe(`1.000\xa0₫`); // non-breaking space
  });

  it('should switch major to minor units', () => {
    expect(currencyUtilsUS.majorToMinorUnits(1000.55)).toEqual(100055);
    expect(currencyUtilsVN.majorToMinorUnits(1000.55)).toEqual(1000.55); // no minor units returns amount
  });

  it('should switch minor to major units', () => {
    expect(currencyUtilsUS.minorToMajorUnits(100055)).toEqual(1000.55);
    expect(currencyUtilsVN.minorToMajorUnits(100055)).toEqual(100055); // no minor units returns amount
    expect(currencyUtilsUS.minorToMajorUnits(100000, true)).toEqual('1000.00');
    expect(currencyUtilsVN.minorToMajorUnits(100000, true)).toEqual('100000'); // no minor units returns amount
  });

  it('should detect that Intl is present because full-icu is installed', () => {
    expect(currencySupportsIntl()).toBe(true);
  });
});
