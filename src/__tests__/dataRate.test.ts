import {
  dataRateBitsToBytes,
  dataRateBpsToGbps,
  dataRateBpsToKbps,
  dataRateBpsToMbps,
  dataRateBytesToBits,
  dataRateGbpsToBps,
  dataRateGbpsToKbps,
  dataRateGbpsToMbps,
  dataRateKbpsToBps,
  dataRateKbpsToGbps,
  dataRateKbpsToMbps,
  dataRateMbpsToBps,
  dataRateMbpsToGbps,
  dataRateMbpsToKbps
} from '..';
import { TWO_NUM_ERROR } from '../util/errors';

describe('data rate utils', () => {
  it('should call "dataRateBitsToBytes" and return the expected value', () => {
    expect(dataRateBitsToBytes(8)).toEqual(1);
    expect(dataRateBitsToBytes(12, true)).toEqual('1.5');
    expect(dataRateBitsToBytes('1', true)).toEqual('0.125');
    expect(() => {
      // @ts-ignore
      dataRateBitsToBytes(false);
    }).toThrow(`dataRateBitsToBytes: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateBytesToBits" and return the expected value', () => {
    expect(dataRateBytesToBits(1)).toEqual(8);
    expect(dataRateBytesToBits(1.5, true)).toEqual('12');
    expect(dataRateBytesToBits('0.125', true)).toEqual('1');
    expect(() => {
      // @ts-ignore
      dataRateBytesToBits(false);
    }).toThrow(`dataRateBytesToBits: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateBpsToGbps" and return the expected value', () => {
    expect(dataRateBpsToGbps(1000000000)).toEqual(1);
    expect(dataRateBpsToGbps(2567567567, true)).toEqual('2.567567567');
    expect(dataRateBpsToGbps('3', true)).toEqual('0.000000003');
    expect(() => {
      // @ts-ignore
      dataRateBpsToGbps(false);
    }).toThrow(`dataRateBpsToGbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateGbpsToBps" and return the expected value', () => {
    expect(dataRateGbpsToBps(1)).toEqual(1000000000);
    expect(dataRateGbpsToBps(2.567567567, true)).toEqual('2567567567');
    expect(dataRateGbpsToBps('0.000000003', true)).toEqual('3');
    expect(() => {
      // @ts-ignore
      dataRateGbpsToBps(false);
    }).toThrow(`dataRateGbpsToBps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateBpsToKbps" and return the expected value', () => {
    expect(dataRateBpsToKbps(1000)).toEqual(1);
    expect(dataRateBpsToKbps(2567567567, true)).toEqual('2567567.567');
    expect(dataRateBpsToKbps('3', true)).toEqual('0.003');
    expect(() => {
      // @ts-ignore
      dataRateBpsToKbps(false);
    }).toThrow(`dataRateBpsToKbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateKbpsToBps" and return the expected value', () => {
    expect(dataRateKbpsToBps(1)).toEqual(1000);
    expect(dataRateKbpsToBps(2567567.567, true)).toEqual('2567567567');
    expect(dataRateKbpsToBps('0.003', true)).toEqual('3');
    expect(() => {
      // @ts-ignore
      dataRateKbpsToBps(false);
    }).toThrow(`dataRateKbpsToBps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateBpsToMbps" and return the expected value', () => {
    expect(dataRateBpsToMbps(1000000)).toEqual(1);
    expect(dataRateBpsToMbps(2567567567, true)).toEqual('2567.567567');
    expect(dataRateBpsToMbps('3', true)).toEqual('0.000003');
    expect(() => {
      // @ts-ignore
      dataRateBpsToMbps(false);
    }).toThrow(`dataRateBpsToMbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateMbpsToBps" and return the expected value', () => {
    expect(dataRateMbpsToBps(1)).toEqual(1000000);
    expect(dataRateMbpsToBps(2567.567567, true)).toEqual('2567567567');
    expect(dataRateMbpsToBps('0.000003', true)).toEqual('3');
    expect(() => {
      // @ts-ignore
      dataRateMbpsToBps(false);
    }).toThrow(`dataRateMbpsToBps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateGbpsToKbps" and return the expected value', () => {
    expect(dataRateGbpsToKbps(1)).toEqual(1000000);
    expect(dataRateGbpsToKbps(2567.567567, true)).toEqual('2567567567');
    expect(dataRateGbpsToKbps('0.000003', true)).toEqual('3');
    expect(() => {
      // @ts-ignore
      dataRateGbpsToKbps(false);
    }).toThrow(`dataRateGbpsToKbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateKbpsToGbps" and return the expected value', () => {
    expect(dataRateKbpsToGbps(1000000)).toEqual(1);
    expect(dataRateKbpsToGbps(2567567567, true)).toEqual('2567.567567');
    expect(dataRateKbpsToGbps('3', true)).toEqual('0.000003');
    expect(() => {
      // @ts-ignore
      dataRateKbpsToGbps(false);
    }).toThrow(`dataRateKbpsToGbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateGbpsToMbps" and return the expected value', () => {
    expect(dataRateGbpsToMbps(1)).toEqual(1000);
    expect(dataRateGbpsToMbps(2567567.567, true)).toEqual('2567567567');
    expect(dataRateGbpsToMbps('0.003', true)).toEqual('3');
    expect(() => {
      // @ts-ignore
      dataRateGbpsToMbps(false);
    }).toThrow(`dataRateGbpsToMbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateMbpsToGbps" and return the expected value', () => {
    expect(dataRateMbpsToGbps(1000)).toEqual(1);
    expect(dataRateMbpsToGbps(2567567567, true)).toEqual('2567567.567');
    expect(dataRateMbpsToGbps('3', true)).toEqual('0.003');
    expect(() => {
      // @ts-ignore
      dataRateMbpsToGbps(false);
    }).toThrow(`dataRateMbpsToGbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateKbpsToMbps" and return the expected value', () => {
    expect(dataRateKbpsToMbps(1000)).toEqual(1);
    expect(dataRateKbpsToMbps(2567567567, true)).toEqual('2567567.567');
    expect(dataRateKbpsToMbps('3', true)).toEqual('0.003');
    expect(() => {
      // @ts-ignore
      dataRateKbpsToMbps(false);
    }).toThrow(`dataRateKbpsToMbps: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataRateMbpsToKbps" and return the expected value', () => {
    expect(dataRateMbpsToKbps(1)).toEqual(1000);
    expect(dataRateMbpsToKbps(2567567.567, true)).toEqual('2567567567');
    expect(dataRateMbpsToKbps('0.003', true)).toEqual('3');
    expect(() => {
      // @ts-ignore
      dataRateMbpsToKbps(false);
    }).toThrow(`dataRateMbpsToKbps: ${TWO_NUM_ERROR}`);
  });
});
