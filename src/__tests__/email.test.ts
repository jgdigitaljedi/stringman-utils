import { emailRetrieve, emailRemove, emailSwap, emailIsValid } from '../';

describe('emailUtils', () => {
  it("should call 'retrieve' and return an email address from a string", () => {
    expect(emailRetrieve('my email address is joey@fakedomain.com')[0]).toBe('joey@fakedomain.com');
    expect(
      emailRetrieve('this one is a little trickier because @ is in it. joey@fakedomain.com')[0]
    ).toBe('joey@fakedomain.com');
  });

  it("should call 'isValid' and return boolean for if string is valid email address", () => {
    expect(emailIsValid('joey@fakedomain.com')).toBe(true);
    expect(emailIsValid('joey-fakedomain.com')).toBe(false);
    expect(emailIsValid('joey@fakedomain')).toBe(false);
  });

  it('should call remove and remove email address, if any, from string', () => {
    expect(emailRemove('my email address is joey@fakedomain.com')).toBe('my email address is');
    expect(
      emailRemove('my email address is joey@fakedomain.com but make it difficult @ with stuff.com')
    ).toBe('my email address is  but make it difficult @ with stuff.com');
  });

  it("should call 'swap' and swap email addresses, if any, in string", () => {
    expect(emailSwap('my email address is joey@fakedomain.com', 'test@test.com')).toBe(
      'my email address is test@test.com'
    );
  });
});
