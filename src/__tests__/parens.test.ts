import { parensRemove, parensRetrieve, parensSwap } from '../';

describe('parenUtils', () => {
  it("should call 'remove' and remove parenthesis from string and returns string", () => {
    expect(parensRemove('this is a test (and this should go away)')).toBe('this is a test');
    expect(parensRemove('with only one paren ( it should do nothing')).toBe(
      'with only one paren ( it should do nothing'
    );
  });

  it("should call 'inside' and return array of what is inside parenthesis", () => {
    expect(parensRetrieve('this (is a test) of the (inside function)')[1]).toBe('inside function');
    expect(
      parensRetrieve('this (is a test) of the (inside function without a second closing paren')[1]
    ).toBe(undefined);
  });

  it("should call 'swap' and take 2 strings, swaps anything in parenthesis INCLUDING THE PARENTHESIS from the first string with the second string", () => {
    expect(parensSwap('replaces things (inside) parens with other things', 'within')).toBe(
      'replaces things within parens with other things'
    );
    // @ts-ignore
    expect(parensSwap('replaces things (inside) parens with other things', 5)).toBe(
      'replaces things 5 parens with other things'
    );
  });
});
