import {
  volumeCupsToFlOz,
  volumeCupsToPints,
  volumeFlOzToCups,
  volumeFlOzToGallons,
  volumeFlOzToMl,
  volumeGallonsToFlOz,
  volumeGallonsToLiters,
  volumeGallonsToQuarts,
  volumeLitersToGallons,
  volumeLitersToMl,
  volumeMlToFlOz,
  volumeMlToLiters,
  volumePintsToCups,
  volumePintsToQuarts,
  volumeQuartsToGallons,
  volumeQuartsToPints
} from '../lib/volumeUtils';
import {
  volumeFlOzToTbsp,
  volumeFlOzToTsp,
  volumeMlToTbsp,
  volumeMlToTsp,
  volumeTbspToFlOz,
  volumeTbspToMl,
  volumeTbspToTsp,
  volumeTspToFlOz,
  volumeTspToMl,
  volumeTspToTbsp
} from '../lib/volumeUtils/volume.utils';
import { TWO_NUM_ERROR } from '../util/errors';

describe('volume', () => {
  it('should call volumeFlOzToCups, take a number for fl oz and optional arg for string return, and return the value converted to cups', () => {
    expect(volumeFlOzToCups(8)).toBe(1);
    expect(volumeFlOzToCups(1, true)).toBe('0.125');
    expect(volumeFlOzToCups('240.8', true)).toBe('30.1');
    // @ts-ignore
    expect(() => volumeFlOzToCups('nope', true)).toThrow(`volumeFlOzToCups: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeCupsToFlOz, take a number for cups and optional arg for string return, and return the value converted to fl oz', () => {
    expect(volumeCupsToFlOz(0.125)).toBe(1);
    expect(volumeCupsToFlOz(1, true)).toBe('8');
    expect(volumeCupsToFlOz('10.3', true)).toBe('82.4');
    // @ts-ignore
    expect(() => volumeCupsToFlOz('nope', true)).toThrow(`volumeCupsToFlOz: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeCupsToPints, take a number for cups and optional arg for string return, and return the value converted to pints', () => {
    expect(volumeCupsToPints(2)).toBe(1);
    expect(volumeCupsToPints(1, true)).toBe('0.5');
    expect(volumeCupsToPints('10.33333333333333333333', true)).toBe('5.166666666666666666665');
    // @ts-ignore
    expect(() => volumeCupsToPints('nope', true)).toThrow(`volumeCupsToPints: ${TWO_NUM_ERROR}`);
  });

  it('should call volumePintsToCups, take a number for pints and optional arg for string return, and return the value converted to cups', () => {
    expect(volumePintsToCups(0.5)).toBe(1);
    expect(volumePintsToCups(1, true)).toBe('2');
    expect(volumePintsToCups('5.33', true)).toBe('10.66');
    // @ts-ignore
    expect(() => volumePintsToCups('nope', true)).toThrow(`volumePintsToCups: ${TWO_NUM_ERROR}`);
  });

  it('should call volumePintsToQuarts, take a number for cpints and optional arg for string return, and return the value converted to quarts', () => {
    expect(volumePintsToQuarts(2)).toBe(1);
    expect(volumePintsToQuarts(1, true)).toBe('0.5');
    expect(volumePintsToQuarts('10.33333333333333333333', true)).toBe('5.166666666666666666665');
    // @ts-ignore
    expect(() => volumePintsToQuarts('nope', true)).toThrow(
      `volumePintsToQuarts: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeQuartsToPints, take a number for quarts and optional arg for string return, and return the value converted to pints', () => {
    expect(volumeQuartsToPints(0.5)).toBe(1);
    expect(volumeQuartsToPints(1, true)).toBe('2');
    expect(volumeQuartsToPints('5.33', true)).toBe('10.66');
    // @ts-ignore
    expect(() => volumeQuartsToPints('nope', true)).toThrow(
      `volumeQuartsToPints: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeQuartsToGallons, take a number for quarts and optional arg for string return, and return the value converted to gallons', () => {
    expect(volumeQuartsToGallons(4)).toBe(1);
    expect(volumeQuartsToGallons(1, true)).toBe('0.25');
    expect(volumeQuartsToGallons('10', true)).toBe('2.5');
    // @ts-ignore
    expect(() => volumeQuartsToGallons('nope', true)).toThrow(
      `volumeQuartsToGallons: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeGallonsToQuarts, take a number for gallons and optional arg for string return, and return the value converted to quarts', () => {
    expect(volumeGallonsToQuarts(0.25)).toBe(1);
    expect(volumeGallonsToQuarts(1, true)).toBe('4');
    expect(volumeGallonsToQuarts('2.5', true)).toBe('10');
    // @ts-ignore
    expect(() => volumeGallonsToQuarts('nope', true)).toThrow(
      `volumeGallonsToQuarts: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeMlToLiters, take a number for ml and optional arg for string return, and return the value converted to liters', () => {
    expect(volumeMlToLiters(1000)).toBe(1);
    expect(volumeMlToLiters(1, true)).toBe('0.001');
    expect(volumeMlToLiters('2555', true)).toBe('2.555');
    // @ts-ignore
    expect(() => volumeMlToLiters('nope', true)).toThrow(`volumeMlToLiters: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeLitersToMl, take a number for liters and optional arg for string return, and return the value converted to ml', () => {
    expect(volumeLitersToMl(0.001)).toBe(1);
    expect(volumeLitersToMl(1, true)).toBe('1000');
    expect(volumeLitersToMl('2.555', true)).toBe('2555');
    // @ts-ignore
    expect(() => volumeLitersToMl('nope', true)).toThrow(`volumeLitersToMl: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeMlToFlOz, take a number for ml and optional arg for string return, and return the value converted to fl oz', () => {
    expect(volumeMlToFlOz(29.57)).toBe(1);
    expect(volumeMlToFlOz(1, true)).toBe('0.033818058843422387554954345621');
    expect(volumeMlToFlOz('10.5', true)).toBe('0.355089617855935069327020629016');
    // @ts-ignore
    expect(() => volumeMlToFlOz('nope', true)).toThrow(`volumeMlToFlOz: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeFlOzToMl, take a number for fl oz and optional arg for string return, and return the value converted to ml', () => {
    expect(volumeFlOzToMl(0.03381805884342238755)).toBe(1);
    expect(volumeFlOzToMl(1, true)).toBe('29.57');
    expect(volumeFlOzToMl('0.35508961785593506933', true)).toBe('10.5000000000000000000881');
    // @ts-ignore
    expect(() => volumeFlOzToMl('nope', true)).toThrow(`volumeFlOzToMl: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeLitersToGallons, take a number for liters and optional arg for string return, and return the value converted to gallons', () => {
    expect(volumeLitersToGallons(3.785411784)).toBe(1);
    expect(volumeLitersToGallons(1, true)).toBe('0.264172052358148415379899921609');
    expect(volumeLitersToGallons('15.75', true)).toBe('4.160709824640837542233423765344');
    // @ts-ignore
    expect(() => volumeLitersToGallons('nope', true)).toThrow(
      `volumeLitersToGallons: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeGallonsToLiters, take a number for gallons and optional arg for string return, and return the value converted to liters', () => {
    expect(volumeGallonsToLiters(0.26417205235814841538)).toBe(0.9999999999999999);
    expect(volumeGallonsToLiters(1, true)).toBe('3.785411784');
    expect(volumeGallonsToLiters('10.5', true)).toBe('39.746823732');
    // @ts-ignore
    expect(() => volumeGallonsToLiters('nope', true)).toThrow(
      `volumeGallonsToLiters: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeFlOzToGallons, take a number for fl oz and optional arg for string return, and return the value converted to gallons', () => {
    expect(volumeFlOzToGallons(128)).toBe(1);
    expect(volumeFlOzToGallons(1, true)).toBe('0.0078125');
    expect(volumeFlOzToGallons('15.75', true)).toBe('0.123046875');
    // @ts-ignore
    expect(() => volumeFlOzToGallons('nope', true)).toThrow(
      `volumeFlOzToGallons: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeGallonsToFlOz, take a number for gallons and optional arg for string return, and return the value converted to fl oz', () => {
    expect(volumeGallonsToFlOz(0.0078125)).toBe(1);
    expect(volumeGallonsToFlOz(1, true)).toBe('128');
    expect(volumeGallonsToFlOz('0.123046875', true)).toBe('15.75');
    // @ts-ignore
    expect(() => volumeGallonsToFlOz('nope', true)).toThrow(
      `volumeGallonsToFlOz: ${TWO_NUM_ERROR}`
    );
  });

  it('should call volumeTspToTbsp, take a number for teaspoons and optional arg for string return, and return the value converted to tablespoons', () => {
    expect(volumeTspToTbsp(3)).toBe(1);
    expect(volumeTspToTbsp(6, true)).toBe('2');
    expect(volumeTspToTbsp('9', true)).toBe('3');
    // @ts-ignore
    expect(() => volumeTspToTbsp('nope', true)).toThrow(`volumeTspToTbsp: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeTbspToTsp, take a number for tablespoons and optional arg for string return, and return the value converted to teaspoons', () => {
    expect(volumeTbspToTsp(1)).toBe(3);
    expect(volumeTbspToTsp(2, true)).toBe('6');
    expect(volumeTbspToTsp('3', true)).toBe('9');
    // @ts-ignore
    expect(() => volumeTbspToTsp('nope', true)).toThrow(`volumeTbspToTsp: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeTbspToFlOz, take a number for tablespoons and optional arg for string return, and return the value converted to fl oz', () => {
    expect(volumeTbspToFlOz(2)).toBe(1);
    expect(volumeTbspToFlOz(4, true)).toBe('2');
    expect(volumeTbspToFlOz('6', true)).toBe('3');
    // @ts-ignore
    expect(() => volumeTbspToFlOz('nope', true)).toThrow(`volumeTbspToFlOz: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeFlOzToTbsp, take a number for fl oz and optional arg for string return, and return the value converted to tablespoons', () => {
    expect(volumeFlOzToTbsp(1)).toBe(2);
    expect(volumeFlOzToTbsp(2, true)).toBe('4');
    expect(volumeFlOzToTbsp('3', true)).toBe('6');
    // @ts-ignore
    expect(() => volumeFlOzToTbsp('nope', true)).toThrow(`volumeFlOzToTbsp: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeFlOzToTsp, take a number for fl oz and optional arg for string return, and return the value converted to teaspoons', () => {
    expect(volumeFlOzToTsp(1)).toBe(6);
    expect(volumeFlOzToTsp(2, true)).toBe('12');
    expect(volumeFlOzToTsp('3', true)).toBe('18');
    // @ts-ignore
    expect(() => volumeFlOzToTsp('nope', true)).toThrow(`volumeFlOzToTsp: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeTspToFlOz, take a number for teaspoons and optional arg for string return, and return the value converted to fl oz', () => {
    expect(volumeTspToFlOz(6)).toBe(1);
    expect(volumeTspToFlOz(12, true)).toBe('2');
    expect(volumeTspToFlOz('18', true)).toBe('3');
    // @ts-ignore
    expect(() => volumeTspToFlOz('nope', true)).toThrow(`volumeTspToFlOz: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeTspToMl, take a number for teaspoons and optional arg for string return, and return the value converted to ml', () => {
    expect(volumeTspToMl(1)).toBe(4.9289215937);
    expect(volumeTspToMl(2, true)).toBe('9.8578431874');
    expect(volumeTspToMl('3', true)).toBe('14.7867647811');
    // @ts-ignore
    expect(() => volumeTspToMl('nope', true)).toThrow(`volumeTspToMl: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeMlToTsp, take a number for ml and optional arg for string return, and return the value converted to teaspoons', () => {
    expect(volumeMlToTsp(4.9289215937)).toBe(1);
    expect(volumeMlToTsp(9.8578431874, true)).toBe('2');
    expect(volumeMlToTsp('14.7867647811', true)).toBe('3');
    // @ts-ignore
    expect(() => volumeMlToTsp('nope', true)).toThrow(`volumeMlToTsp: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeTbspToMl, take a number for tablespoons and optional arg for string return, and return the value converted to ml', () => {
    expect(volumeTbspToMl(1)).toBe(14.7867647811);
    expect(volumeTbspToMl(2, true)).toBe('29.5735295622');
    expect(volumeTbspToMl('3', true)).toBe('44.3602943433');
    // @ts-ignore
    expect(() => volumeTbspToMl('nope', true)).toThrow(`volumeTbspToMl: ${TWO_NUM_ERROR}`);
  });

  it('should call volumeMlToTbsp, take a number for ml and optional arg for string return, and return the value converted to tablespoons', () => {
    expect(volumeMlToTbsp(14.7867647811)).toBe(1);
    expect(volumeMlToTbsp(29.5735295622, true)).toBe('2');
    expect(volumeMlToTbsp('44.3602943433', true)).toBe('3');
    // @ts-ignore
    expect(() => volumeMlToTbsp('nope', true)).toThrow(`volumeMlToTbsp: ${TWO_NUM_ERROR}`);
  });
});
