import { bracketsRemove, bracketsRetrieve, bracketsSwap } from '../';

describe('parenUtils', () => {
  it("should call 'remove' and remove square brackets from string and returns string", () => {
    expect(bracketsRemove('this is a test [and this should go away]')).toBe('this is a test');
    expect(bracketsRemove('with only one bracket [ it should do nothing')).toBe(
      'with only one bracket [ it should do nothing'
    );
  });

  it("should call 'inside' and return array of what is inside square brackets", () => {
    expect(bracketsRetrieve('this [is a test] of the [inside function]')[1]).toBe(
      'inside function'
    );
    expect(
      bracketsRetrieve(
        'this [is a test] of the [inside function without a second closing bracket'
      )[1]
    ).toBe(undefined);
  });

  it("should call 'swap' and take 2 strings, swaps anything in square brackets INCLUDING THE BRACKETS from the first string with the second string", () => {
    expect(bracketsSwap('replaces things [inside] brackets with other things', 'within')).toBe(
      'replaces things within brackets with other things'
    );
    // @ts-ignore
    expect(bracketsSwap('replaces things [inside] brackets with other things', 5)).toBe(
      'replaces things 5 brackets with other things'
    );
  });
});
