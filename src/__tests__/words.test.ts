import {
  wordAllWordCount,
  wordCapitalize,
  wordCount,
  wordReadingTime,
  wordReplaceWord,
  wordSpecificWordCount
} from '../lib/wordUtils';

describe('"words" module"', () => {
  it("shoulld call 'wordCount' and return a word count from a string", () => {
    expect(wordCount('This sentence has 4 words.')).toBe(4);
    expect(wordCount('This sentence has five words.')).toBe(5);
    expect(
      wordCount(
        'Try something with, you know, complexity. This will be more difficult! Will it work?'
      )
    ).toBe(14);
  });

  it("should call 'specificWordCount', take a string and a word, and return the number of times that word is in the string", () => {
    expect(wordSpecificWordCount('This word has word word.', 'word')).toBe(3);
    expect(
      wordSpecificWordCount('Maybe-this will, trip it up! Will it? We WILL see.', 'will')
    ).toBe(3);
    expect(
      wordSpecificWordCount('Maybe-this will, trip it up! Will it? We WILL see.', 'will', true)
    ).toBe(1);
  });

  it("should call 'allWordCount', take a string, counts the number of occurences of each word in the string, and returns as an object", () => {
    const test = `
    This is my stupid test. It needs to have a few sentences.
    It also needs to have some variation in words, but should repeat some words as well for the sake of getting some good data.
    We'll see how this one works out, but I think it will be fine. Some WORDS are capitalized, some Words are not.
    `;
    const counts = wordAllWordCount(test);
    const strict = wordAllWordCount(test, true);
    expect(counts.words).toBe(4);
    expect(strict.words).toBe(2);
    expect(counts.some).toBe(5);
    expect(strict.some).toBe(4);
    expect(strict.WORDS).toBe(1);
    expect(wordAllWordCount('')).toEqual({});
    // @ts-ignore
    expect(wordAllWordCount(null)).toEqual({});
  });

  it("should call 'capitalize', take a string, and return the string with the first character capitalized", () => {
    expect(wordCapitalize('test')).toBe('Test');
    expect(wordCapitalize('teSt')).toBe('TeSt');
    expect(wordCapitalize('TeSt')).toBe('TeSt');
    // @ts-ignore
    expect(wordCapitalize(true)).toBe(null);
    // @ts-ignore
    expect(wordCapitalize(5)).toBe(null);
  });

  it("should call 'replaceWord', take 3 to 4 arguments (original string, string to be replaced, string to replace with, and optional boolean for case sensitivity) and return original string with replacements made", () => {
    const test =
      'We hope this works because we put some serious work into this and we are invested.';
    expect(wordReplaceWord(test, 'we', 'they', true)).toBe(
      'They hope this works because they put some serious work into this and they are invested.'
    );
    expect(wordReplaceWord(test, 'we', 'they')).toBe(
      'they hope this works because they put some serious work into this and they are invested.'
    );
    // @ts-ignore
    expect(wordReplaceWord(test, 5, 'they')).toBe(null);
  });

  it("should call 'readingTime', take wordCount, imageCount, valueOnly, and wordsPerMinute and returns expected reading time", () => {
    const wordCount = 1200;
    const imageCount = 5;
    expect(wordReadingTime(wordCount, imageCount)).toEqual('5 min read');
    expect(wordReadingTime(wordCount, imageCount, true)).toEqual(5);
    expect(wordReadingTime(wordCount, imageCount, false, 400)).toEqual('4 min read');
    expect(wordReadingTime(wordCount, imageCount, true, 400)).toEqual(4);
  });
});
