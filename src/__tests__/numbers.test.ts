import {
  numberContainsDecimal,
  numberContainsFraction,
  numberContainsNum,
  numberConvertToHex,
  numberIsDecimal,
  numberIsFraction,
  numberIsPhoneNumber,
  numberIsWhole,
  numberRemoveLeadingZeros,
  numberRemoveTrailingZeros,
  numberTruncate
} from '../';

describe('numberUtils', () => {
  it("should call 'containsNum' and determine whether or not a string or number contains some digits", () => {
    expect(numberContainsNum('this has a number 33')).toBe(true);
    expect(numberContainsNum('this has no numbers')).toBe(false);
    expect(numberContainsNum(123)).toBe(true);
    // @ts-ignore
    expect(numberContainsNum(false)).toBe(false);
  });

  it("should call 'whole' and determine if passed value is a whole number", () => {
    expect(numberIsWhole(33)).toBe(true);
    expect(numberIsWhole('-33')).toBe(true);
    expect(numberIsWhole('33.326')).toBe(false);
    expect(numberIsWhole(-33.326)).toBe(false);
    expect(numberIsWhole('nope')).toBe(false);
    // @ts-ignore
    expect(numberIsWhole(false)).toBe(false);
  });

  it("should call 'decimal' and determine if passed value is a decimal", () => {
    expect(numberIsDecimal(33)).toBe(false);
    expect(numberIsDecimal('-33')).toBe(false);
    expect(numberIsDecimal('33.326')).toBe(true);
    expect(numberIsDecimal(-33.326)).toBe(true);
    expect(numberIsDecimal('nope')).toBe(false);
    // @ts-ignore
    expect(numberIsDecimal(false)).toBe(false);
  });

  it("should call 'fraction' and determine if passed value is a fraction", () => {
    // expect(numberUtils.fraction(33)).toBe(false);
    expect(numberIsFraction('-33')).toBe(false);
    expect(numberIsFraction('1/3')).toBe(true);
    expect(numberIsFraction('-22/33')).toBe(true);
    expect(numberIsFraction('nope')).toBe(false);
    // @ts-ignore
    expect(numberIsFraction(false)).toBe(false);
  });

  it("should call 'containsFraction' and determine if passed value contains a fraction", () => {
    // expect(numberUtils.containsFraction(33)).toBe(false);
    expect(numberContainsFraction('-33')).toBe(false);
    expect(numberContainsFraction('has the fraction 1/3')).toBe(true);
    expect(numberContainsFraction('has a fraction -22/33')).toBe(true);
    expect(numberContainsFraction('nope')).toBe(false);
    // @ts-ignore
    expect(numberContainsFraction(false)).toBe(false);
  });

  it("should call 'containsDecimal' and determine if passed value contains a decimal", () => {
    expect(numberContainsDecimal(33)).toBe(false);
    expect(numberContainsDecimal('-33')).toBe(false);
    expect(numberContainsDecimal('has the decimal 2.4')).toBe(true);
    expect(numberContainsDecimal('has a decimal -22.44')).toBe(true);
    expect(numberContainsDecimal(3.4)).toBe(true);
    expect(numberContainsDecimal('nope')).toBe(false);
    // @ts-ignore
    expect(numberContainsDecimal(false)).toBe(false);
  });

  it("should call 'convertToHex' and take a string or number from 0 to 255 and returns the hexidecimal equivalent", () => {
    expect(numberConvertToHex(255)).toBe('FF');
    expect(numberConvertToHex('0')).toBe('00');
    expect(numberConvertToHex('nope')).toBe('NAN');
    // @ts-ignore
    expect(numberConvertToHex(false)).toBe(null);
  });

  it("should call 'isPhoneNumber' and take a number or string and validates whether it is a valid phone number", () => {
    expect(numberIsPhoneNumber('(888) 555-1234')).toBe(true);
    expect(numberIsPhoneNumber('(123)456-7890')).toBe(true);
    expect(numberIsPhoneNumber('123-456-7890')).toBe(true);
    expect(numberIsPhoneNumber('123.456.7890')).toBe(true);
    expect(numberIsPhoneNumber(1234567890)).toBe(true);
    expect(numberIsPhoneNumber('+31636363634')).toBe(true);
    expect(numberIsPhoneNumber('075-63546725')).toBe(true);
    expect(numberIsPhoneNumber('333-44-5555')).toBe(false);
    expect(numberIsPhoneNumber('nope')).toBe(false);
    expect(numberIsPhoneNumber(100)).toBe(false);
    // @ts-ignore
    expect(numberIsPhoneNumber(false)).toBe(false);
  });

  it("should call 'removeLeadingZeros' and return the value with leading zeros removed", () => {
    expect(numberRemoveLeadingZeros('0450', true)).toBe(450);
    expect(numberRemoveLeadingZeros('00450.450')).toBe('450.450');
    expect(numberRemoveLeadingZeros('004500.4500')).toBe('4500.4500');
    expect(numberRemoveLeadingZeros('004500.4500', true)).toBe(4500.45);
    // @ts-ignore
    expect(numberRemoveLeadingZeros(false)).toBe(null);
  });

  it("should call 'removeTrailingZeros' and return the value with trailing zeros removed", () => {
    expect(numberRemoveTrailingZeros('45.450')).toBe('45.45');
    expect(numberRemoveTrailingZeros('45.450', true)).toBe(45.45);
    expect(numberRemoveTrailingZeros('450.450', false, true)).toBe('45');
    expect(numberRemoveTrailingZeros('450.450', true, true)).toBe(45);
    expect(numberRemoveTrailingZeros('450000', true, true)).toBe(45);
    expect(numberRemoveTrailingZeros('45000', false, true)).toBe('45');
    expect(numberRemoveTrailingZeros('0045000', false, true)).toBe('45');
    expect(numberRemoveTrailingZeros('0045000.4500')).toBe('45000.45');
    // @ts-ignore
    expect(numberRemoveTrailingZeros(false)).toBe(null);
  });

  it("should call 'truncate' and return the value truncated to the given amount of decimal places without rounding", () => {
    expect(numberTruncate('456.789', 2)).toEqual(456.78);
    expect(numberTruncate(456.789, 2)).toEqual(456.78);
    expect(numberTruncate('456.789', 2, true)).toEqual('456.78');
    expect(numberTruncate(456.789, 2, true)).toEqual('456.78');
    expect(numberTruncate('456.789', 2, true)).toEqual('456.78');
    expect(numberTruncate('456.789', 4, true)).toEqual('456.7890');
    expect(numberTruncate('456.789', 4)).toEqual(456.789);
    // @ts-ignore
    expect(numberTruncate(false, 2)).toBe('');
  });
});
