import {
  speedFtsToKn,
  speedFtsToKmh,
  speedFtsToMph,
  speedFtsToMs,
  speedKnToFts,
  speedKnToKmh,
  speedKnToMph,
  speedKnToMs,
  speedKmhToFts,
  speedKmhToKn,
  speedKmhToMph,
  speedKmhToMs,
  speedMphToFts,
  speedMphToKn,
  speedMphToKmh,
  speedMphToMs,
  speedMsToFts,
  speedMsToKn,
  speedMsToKmh,
  speedMsToMph,
  speedToMach,
  speedPercentSpeedOfLight
} from '..';
import { INVALID_UNIT, TWO_NUM_ERROR } from '../util/errors';

describe('speed utils', () => {
  it('should call "speedFtsToKn", take a number for ft/s, and return the conversion to knots', () => {
    expect(speedFtsToKn(1)).toBe(1.68780985710118522428);
    expect(speedFtsToKn(0.5924838012959)).toBe(1);
    expect(speedFtsToKn(2)).toBe(3.37561971420237044855);
    // @ts-ignore
    expect(() => speedFtsToKn('nope', true)).toThrow(`speedFtsToKn: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKnToFts", take a number for knots, and return the conversion to ft/s', () => {
    expect(speedKnToFts(1)).toBe(0.5924838012959);
    expect(speedKnToFts(1.68780985710118522428)).toBe(1);
    expect(speedKnToFts(3.37561971420237044855)).toBe(2);
    // @ts-ignore
    expect(() => speedKnToFts('nope', true)).toThrow(`speedKnToFts: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedFtsToKmh", take a number for ft/s, and return the conversion to km/h', () => {
    expect(speedFtsToKmh(1)).toBe(1.09728);
    expect(speedFtsToKmh(0.91134441528142315544)).toBe(1);
    expect(speedFtsToKmh(1.82268883056284631088)).toBe(2);
    // @ts-ignore
    expect(() => speedFtsToKmh('nope', true)).toThrow(`speedFtsToKmh: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKmhToFts", take a number for km/h, and return the conversion to ft/s', () => {
    expect(speedKmhToFts(1)).toBe(0.91134441528142315544);
    expect(speedKmhToFts(1.09728)).toBe(1);
    expect(speedKmhToFts(2.19456)).toBe(2);
    // @ts-ignore
    expect(() => speedKmhToFts('nope', true)).toThrow(`speedKmhToFts: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedFtsToMph", take a number for ft/s, and return the conversion to mph', () => {
    expect(speedFtsToMph(1)).toBe(0.6818181802686);
    expect(speedFtsToMph(1.46666666999998934091)).toBe(1);
    expect(speedFtsToMph(2.93333333999997868182)).toBe(2);
    // @ts-ignore
    expect(() => speedFtsToMph('nope', true)).toThrow(`speedFtsToMph: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMphToFts", take a number for mph, and return the conversion to ft/s', () => {
    expect(speedMphToFts(1)).toBe(1.4666666699999893);
    expect(speedMphToFts(0.6818181802686)).toBe(1);
    expect(speedMphToFts(1.3636363605372)).toBe(2);
    // @ts-ignore
    expect(() => speedMphToFts('nope', true)).toThrow(`speedMphToFts: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedFtsToMs", take a number for ft/s, and return the conversion to meters/s', () => {
    expect(speedFtsToMs(1)).toBe(0.3048);
    expect(speedFtsToMs(3.28083989501312335958)).toBe(1);
    expect(speedFtsToMs(6.56167979002624671916)).toBe(2);
    // @ts-ignore
    expect(() => speedFtsToMs('nope', true)).toThrow(`speedFtsToMs: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMsToFts", take a number for meters/s, and return the conversion to ft/s', () => {
    expect(speedMsToFts(1)).toBe(3.28083989501312335958);
    expect(speedMsToFts(0.3048)).toBe(1);
    expect(speedMsToFts(0.6096)).toBe(2);
    // @ts-ignore
    expect(() => speedMsToFts('nope', true)).toThrow(`speedMsToFts: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKnToKmh", take a number for knots, and return the conversion to km/h', () => {
    expect(speedKnToKmh(1)).toBe(1.8519995164223486);
    expect(speedKnToKmh(0.53995694444444441)).toBe(1);
    expect(speedKnToKmh(1.07991388888888882458)).toBe(2);
    // @ts-ignore
    expect(() => speedKnToKmh('nope', true)).toThrow(`speedKnToKmh: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKmhToKn", take a number for km/h, and return the conversion to knots', () => {
    expect(speedKmhToKn(1)).toBe(0.53995694444444441);
    expect(speedKmhToKn(1.8519995164223486)).toBe(1);
    expect(speedKmhToKn(3.7039990328446972)).toBe(2);
    // @ts-ignore
    expect(() => speedKmhToKn('nope', true)).toThrow(`speedKmhToKn: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKnToMph", take a number for knots, and return the conversion to mph', () => {
    expect(speedKnToMph(1)).toBe(1.1507794480235);
    expect(speedKnToMph(0.86897624190068)).toBe(0.9999999999999999);
    expect(speedKnToMph(1.73795248380136)).toBe(1.9999999999999998);
    // @ts-ignore
    expect(() => speedKnToMph('nope', true)).toThrow(`speedKnToMph: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMphToKn", take a number for mph, and return the conversion to knots', () => {
    expect(speedMphToKn(1)).toBe(0.86897624190068);
    expect(speedMphToKn(1.1507794480235)).toBe(1);
    expect(speedMphToKn(2.301558896047)).toBe(2);
    // @ts-ignore
    expect(() => speedMphToKn('nope', true)).toThrow(`speedMphToKn: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKnToMs", take a number for knots, and return the conversion to meters/s', () => {
    expect(speedKnToMs(1)).toBe(0.51444444444444);
    expect(speedKnToMs(1.94384449244062155)).toBe(1);
    expect(speedKnToMs(3.88768898488124309)).toBe(2);
    // @ts-ignore
    expect(() => speedKnToMs('nope', true)).toThrow(`speedKnToMs: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMsToKn", take a number for meters/s, and return the conversion to knots', () => {
    expect(speedMsToKn(1)).toBe(1.9438444924406215);
    expect(speedMsToKn(0.51444444444444)).toBe(1);
    expect(speedMsToKn(1.02888888888888)).toBe(2);
    // @ts-ignore
    expect(() => speedMsToKn('nope', true)).toThrow(`speedMsToKn: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKmhToMph", take a number for km/h, and return the conversion to mph', () => {
    expect(speedKmhToMph(1)).toBe(0.62137119223733397);
    expect(speedKmhToMph(1.609344)).toBe(1);
    expect(speedKmhToMph(3.218688)).toBe(2);
    // @ts-ignore
    expect(() => speedKmhToMph('nope', true)).toThrow(`speedKmhToMph: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMphToKmh", take a number for mph, and return the conversion to km/h', () => {
    expect(speedMphToKmh(1)).toBe(1.609344);
    expect(speedMphToKmh(0.62137119223733397)).toBe(1);
    expect(speedMphToKmh(1.24274238447466794)).toBe(2);
    // @ts-ignore
    expect(() => speedMphToKmh('nope', true)).toThrow(`speedMphToKmh: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedKmhToMs", take a number for km/h, and return the conversion to meters/s', () => {
    expect(speedKmhToMs(1)).toBe(0.27777777777777778);
    expect(speedKmhToMs(3.6)).toBe(1);
    expect(speedKmhToMs(7.2)).toBe(2);
    // @ts-ignore
    expect(() => speedKmhToMs('nope', true)).toThrow(`speedKmhToMs: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMsToKmh", take a number for meters/s, and return the conversion to km/h', () => {
    expect(speedMsToKmh(1)).toBe(3.6);
    expect(speedMsToKmh(0.27777777777777778)).toBe(1);
    expect(speedMsToKmh(0.55555555555555556)).toBe(2);
    // @ts-ignore
    expect(() => speedMsToKmh('nope', true)).toThrow(`speedMsToKmh: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMphToMs", take a number for mph, and return the conversion to meters/s', () => {
    expect(speedMphToMs(1)).toBe(0.44704);
    expect(speedMphToMs(2.23693629205440229)).toBe(1);
    expect(speedMphToMs(4.47387258410880458)).toBe(2);
    // @ts-ignore
    expect(() => speedMphToMs('nope', true)).toThrow(`speedMphToMs: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedMsToMph", take a number for meters/s, and return the conversion to mph', () => {
    expect(speedMsToMph(1)).toBe(2.23693629205440229);
    expect(speedMsToMph(0.44704)).toBe(1);
    expect(speedMsToMph(0.89408)).toBe(2);
    // @ts-ignore
    expect(() => speedMsToMph('nope', true)).toThrow(`speedMsToMph: ${TWO_NUM_ERROR}`);
  });

  it('should call "speedPercentSpeedOfLight", take a number for meters/s, and return the percent of speed of light', () => {
    expect(speedPercentSpeedOfLight(983571056.43045, 'ft/s')).toBe(100);
    expect(speedPercentSpeedOfLight(100, 'ft/s', true)).toBe('0.0000101670336216396351313181');
    expect(speedPercentSpeedOfLight(299792458, 'm/s')).toBe(100);
    expect(speedPercentSpeedOfLight(10000, 'm/s', true)).toBe('0.0033356409519815204957557671');
    expect(speedPercentSpeedOfLight(186282, 'miles/s')).toBe(100);
    expect(speedPercentSpeedOfLight(10000, 'miles/s', true)).toBe('5.368205194275345980824771046');
    expect(speedPercentSpeedOfLight(1079252848.8, 'km/h')).toBe(100);
    expect(speedPercentSpeedOfLight(1000000, 'km/h', true)).toBe('0.0926566931105977915487713096');
    expect(speedPercentSpeedOfLight(670616629.3844, 'mph')).toBe(100);
    expect(speedPercentSpeedOfLight(1000000, 'mph', true)).toBe('0.1491164931173808099018654318');
    expect(speedPercentSpeedOfLight(582749918.36357, 'kn')).toBe(100);
    expect(speedPercentSpeedOfLight(1000000, 'kn', true)).toBe('0.1716001956393433878980038303');
    // @ts-ignore
    expect(() => speedPercentSpeedOfLight('nope', true)).toThrow(
      `speedPercentSpeedOfLight: ${INVALID_UNIT}`
    );
    // @ts-ignore
    expect(() => speedPercentSpeedOfLight('nope', 'm/s', true)).toThrow(
      `speedPercentSpeedOfLight: ${TWO_NUM_ERROR}`
    );
  });

  it('should call "speedToMach", take a number for meters/s, and return the conversion to mach number', () => {
    expect(speedToMach(1125, 'ft/s')).toBe(1);
    expect(speedToMach(100, 'ft/s', true)).toBe('0.088888888888888888888888888889');
    expect(speedToMach(343, 'm/s')).toBe(1);
    expect(speedToMach(100, 'm/s', true)).toBe('0.291545189504373177842565597668');
    expect(speedToMach(0.21313, 'miles/s')).toBe(1);
    expect(speedToMach(1, 'miles/s', true)).toBe('4.691972035846666353868530943556');
    expect(speedToMach(1234.8, 'km/h')).toBe(1);
    expect(speedToMach(1000, 'km/h', true)).toBe('0.809847748623258827340459993521');
    expect(speedToMach(767.26915, 'mph')).toBe(1);
    expect(speedToMach(100, 'mph', true)).toBe('0.130332361205973158180541991034');
    expect(speedToMach(661, 'kn')).toBe(1);
    expect(speedToMach(100, 'kn', true)).toBe('0.151285930408472012102874432678');
    // @ts-ignore
    expect(() => speedToMach('nope', true)).toThrow(`speedToMach: ${INVALID_UNIT}`);
    // @ts-ignore
    expect(() => speedToMach('nope', 'm/s', true)).toThrow(`speedToMach: ${TWO_NUM_ERROR}`);
  });
});
