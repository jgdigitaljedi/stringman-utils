import { screen } from '@testing-library/dom';
import { styleConvertRemToPixels, styleGetElementFontSize } from '../lib/styleUtils';

describe('styleUtils', () => {
  it('should get font size of element passed to it', () => {
    document.body.innerHTML = `<div style="font-size: 17px">Test</div>`;
    const dummyDiv = screen.getByText('Test');
    expect(styleGetElementFontSize(dummyDiv)).toEqual(17);
  });

  it('should convert rem to pixels', () => {
    document.body.innerHTML = `<div style="font-size: 14px">Test</div>`;
    const dummyDiv = screen.getByText('Test');
    expect(styleConvertRemToPixels(2, dummyDiv)).toEqual(28);
  });
});
