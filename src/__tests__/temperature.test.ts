import {
  tempCelciusToFahrenheit,
  tempCelciusToKelvin,
  tempFahrenheitToCelcius,
  tempFahrenheitToKelvin,
  tempKelvinToCelcius,
  tempKelvinToFahrenheit
} from '../lib/temperatureUtils';
import { TWO_NUM_ERROR } from '../util/errors';

describe('temperatureUtils', () => {
  it('should call "fahrenheitToCelcius", take a fahrenheit temp, and return the celcius equivalent', () => {
    expect(tempFahrenheitToCelcius(212)).toEqual(100);
    expect(tempFahrenheitToCelcius(32)).toEqual(0);
    expect(tempFahrenheitToCelcius(-40)).toEqual(-40);
    expect(tempFahrenheitToCelcius('98.6', true)).toEqual('37');
    // @ts-ignore
    expect(() => tempFahrenheitToCelcius('nope', true)).toThrow(
      `tempFahrenheitToCelcius: ${TWO_NUM_ERROR}`
    );
  });

  it('should call "celciusToFahrenheit", take a celcius temp, and return the fahrenheit equivalent', () => {
    expect(tempCelciusToFahrenheit(100)).toEqual(212);
    expect(tempCelciusToFahrenheit(0)).toEqual(32);
    expect(tempCelciusToFahrenheit(-40)).toEqual(-40);
    expect(tempCelciusToFahrenheit('37', true)).toEqual('98.6');
    // @ts-ignore
    expect(() => tempCelciusToFahrenheit('nope', true)).toThrow(
      `tempCelciusToFahrenheit: ${TWO_NUM_ERROR}`
    );
  });

  it('should call "kelvinToCelcius", take a kelvin temp, and return the celcius equivalent', () => {
    expect(tempKelvinToCelcius(0)).toEqual(-273.15);
    expect(tempKelvinToCelcius(294.15)).toEqual(21);
    expect(tempKelvinToCelcius('1000', true)).toEqual('726.85');
    // @ts-ignore
    expect(() => tempKelvinToCelcius('nope', true)).toThrow(
      `tempKelvinToCelcius: ${TWO_NUM_ERROR}`
    );
  });

  it('should call "celciusToKelvin", take a celcius temp, and return the kelvin equivalent', () => {
    expect(tempCelciusToKelvin(-273.15)).toEqual(0);
    expect(tempCelciusToKelvin(21)).toEqual(294.15);
    expect(tempCelciusToKelvin('726.85', true)).toEqual('1000');
    // @ts-ignore
    expect(() => tempCelciusToKelvin('nope', true)).toThrow(
      `tempCelciusToKelvin: ${TWO_NUM_ERROR}`
    );
  });

  it('should call "fahrenheitToKelvin", take a fahrenheit temp, and return the kelvin equivalent', () => {
    expect(tempFahrenheitToKelvin(-459.67)).toEqual(0);
    expect(tempFahrenheitToKelvin('100')).toEqual(310.9277777777778);
    expect(tempFahrenheitToKelvin(0, true)).toEqual('255.37222222222222222222222222242652');
    // @ts-ignore
    expect(() => tempFahrenheitToKelvin('nope', true)).toThrow(
      `tempFahrenheitToKelvin: ${TWO_NUM_ERROR}`
    );
  });

  it('should call "tempKelvinToFahrenheit", take a kelvin temp, and return the fahrenheit equivalent', () => {
    expect(tempKelvinToFahrenheit(0)).toEqual(-459.67);
    expect(tempKelvinToFahrenheit('310.9277777777778')).toEqual(100.00000000000004);
    expect(tempKelvinToFahrenheit('255.37222222222222', true)).toEqual('-0.000000000000004');
    // @ts-ignore
    expect(() => tempKelvinToFahrenheit('nope', true)).toThrow(
      `tempKelvinToFahrenheit: ${TWO_NUM_ERROR}`
    );
  });
});
