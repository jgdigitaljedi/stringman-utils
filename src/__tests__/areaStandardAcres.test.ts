import { TWO_NUM_ERROR } from '../util/errors';
import {
  areaAcresToHectares,
  areaAcresToSquareChainsGun,
  areaAcresToSquareMiles,
  areaHectaresToAcres,
  areaSquareChainsGunToAcres,
  areaSquareMilesToAcres,
  areaAcresToSquareFeet,
  areaSquareFeetToAcres,
  areaAcresToSquareRods,
  areaSquareRodsToAcres,
  areaAcresToSquareLinksGun,
  areaSquareLinksGunToAcres,
  areaTownshipsToAcres,
  areaAcresToTownships,
  areaSectionsToAcres,
  areaAcresToSections,
  areaAcresToSquareYards,
  areaSquareYardsToAcres,
  areaAcresToSquareKilometers,
  areaAcresToSquareMeters,
  areaSquareKilometersToAcres,
  areaSquareMetersToAcres
} from '../index';

describe('area utils - standard acres', () => {
  it('should call areaAcresToHectares, take a number for acres and optional arg for string return, and return the value converted to hectares', () => {
    expect(areaAcresToHectares(2.4710538146717)).toBe(1);
    expect(areaAcresToHectares(4.9421076293434, true)).toBe('2');
    expect(areaAcresToHectares('9.266451805018875', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaAcresToHectares('nope', true)).toThrow(
      `areaAcresToHectares: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaHectaresToAcres, take a number for hectares and optional arg for string return, and return the value converted to acres', () => {
    expect(areaHectaresToAcres(1)).toBe(2.4710538146717);
    expect(areaHectaresToAcres(2, true)).toBe('4.9421076293434');
    expect(areaHectaresToAcres('3.75', true)).toBe('9.266451805018875');
    // @ts-ignore
    expect(() => areaHectaresToAcres('nope', true)).toThrow(
      `areaHectaresToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMilesToAcres, take a number for sq mi and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareMilesToAcres(1)).toBe(640);
    expect(areaSquareMilesToAcres(0.5, true)).toBe('320');
    expect(areaSquareMilesToAcres('10.85', true)).toBe('6944');
    // @ts-ignore
    expect(() => areaSquareMilesToAcres('nope', true)).toThrow(
      `areaSquareMilesToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareMiles, take a number for acres and optional arg for string return, and return the value converted to sq mi', () => {
    expect(areaAcresToSquareMiles(640)).toBe(1);
    expect(areaAcresToSquareMiles(320, true)).toBe('0.5');
    expect(areaAcresToSquareMiles('6944', true)).toBe('10.85');
    // @ts-ignore
    expect(() => areaAcresToSquareMiles('nope', true)).toThrow(
      `areaAcresToSquareMiles: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareChainsGunToAcres, take a number for sq chains (Gunter/survey) and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareChainsGunToAcres(1)).toBe(0.100000399757202051817595743823);
    expect(areaSquareChainsGunToAcres(2, true)).toBe('0.200000799514404103635191487646');
    expect(areaSquareChainsGunToAcres('3.75', true)).toBe('0.375001499089507694315984039336');
    // @ts-ignore
    expect(() => areaSquareChainsGunToAcres('nope', true)).toThrow(
      `areaSquareChainsGunToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareChainsGun, take a number for acres and optional arg for string return, and return the value converted to sq chains (Gunter/survey)', () => {
    expect(areaAcresToSquareChainsGun(0.100000399757202051817595743823)).toBe(1);
    expect(areaAcresToSquareChainsGun(0.200000799514404103635191487646, true)).toBe(
      '1.99999999999999996364823044236'
    );
    expect(areaAcresToSquareChainsGun('0.375001499089507694315984039336', true)).toBe(
      '3.7500000000000000000000000000033500877561056'
    );
    // @ts-ignore
    expect(() => areaAcresToSquareChainsGun('nope', true)).toThrow(
      `areaAcresToSquareChainsGun: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareFeet, take a number for acres and optional arg for string return, and return the value converted to sq feet', () => {
    expect(areaAcresToSquareFeet(1)).toBe(43560);
    expect(areaAcresToSquareFeet(2, true)).toBe('87120');
    expect(areaAcresToSquareFeet('0.55', true)).toBe('23958');
    // @ts-ignore
    expect(() => areaAcresToSquareFeet('nope', true)).toThrow(
      `areaAcresToSquareFeet: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareFeetToAcres, take a number for sq ft and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareFeetToAcres(43560)).toBe(1);
    expect(areaSquareFeetToAcres(87120, true)).toBe('2');
    expect(areaSquareFeetToAcres('23958', true)).toBe('0.55');
    // @ts-ignore
    expect(() => areaSquareFeetToAcres('nope', true)).toThrow(
      `areaSquareFeetToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareRods, take a number for acres and optional arg for string return, and return the value converted to sq rods', () => {
    expect(areaAcresToSquareRods(1)).toBe(159.99935933462);
    expect(areaAcresToSquareRods(2, true)).toBe('319.99871866924');
    expect(areaAcresToSquareRods('3.75', true)).toBe('599.997597504825');
    // @ts-ignore
    expect(() => areaAcresToSquareRods('nope', true)).toThrow(
      `areaAcresToSquareRods: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareRodsToAcres, take a number for sq rods and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareRodsToAcres(159.99935933462)).toBe(1);
    expect(areaSquareRodsToAcres(319.99871866924, true)).toBe('2');
    expect(areaSquareRodsToAcres('599.997597504825', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareRodsToAcres('nope', true)).toThrow(
      `areaSquareRodsToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareLinksGun, take a number for acres and optional arg for string return, and return the value converted to sq links', () => {
    expect(areaAcresToSquareLinksGun(1)).toBe(99999.600244396);
    expect(areaAcresToSquareLinksGun(2, true)).toBe('199999.200488792');
    expect(areaAcresToSquareLinksGun('3.75', true)).toBe('374998.500916485');
    // @ts-ignore
    expect(() => areaAcresToSquareLinksGun('nope', true)).toThrow(
      `areaAcresToSquareLinksGun: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareLinksGunToAcres, take a number for sq links and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareLinksGunToAcres(99999.600244396)).toBe(1);
    expect(areaSquareLinksGunToAcres(199999.200488792, true)).toBe('2');
    expect(areaSquareLinksGunToAcres('374998.500916485', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareLinksGunToAcres('nope', true)).toThrow(
      `areaSquareLinksGunToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaTownshipsToAcres, take a number for townships and optional arg for string return, and return the value converted to acres', () => {
    expect(areaTownshipsToAcres(1)).toBe(23040.092256185);
    expect(areaTownshipsToAcres(2, true)).toBe('46080.18451237');
    expect(areaTownshipsToAcres('3.75', true)).toBe('86400.34596069375');
    // @ts-ignore
    expect(() => areaTownshipsToAcres('nope', true)).toThrow(
      `areaTownshipsToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToTownships, take a number for acres and optional arg for string return, and return the value converted to townships', () => {
    expect(areaAcresToTownships(23040.092256185)).toBe(1);
    expect(areaAcresToTownships(46080.18451237, true)).toBe('2');
    expect(areaAcresToTownships('86400.34596069375', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaAcresToTownships('nope', true)).toThrow(
      `areaAcresToTownships: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSectionsToAcres, take a number for sections and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSectionsToAcres(1)).toBe(640.00256734189);
    expect(areaSectionsToAcres(2, true)).toBe('1280.00513468378');
    expect(areaSectionsToAcres('3.75', true)).toBe('2400.0096275320875');
    // @ts-ignore
    expect(() => areaSectionsToAcres('nope', true)).toThrow(
      `areaSectionsToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSections, take a number for acres and optional arg for string return, and return the value converted to sections', () => {
    expect(areaAcresToSections(640.00256734189)).toBe(1);
    expect(areaAcresToSections(1280.00513468378, true)).toBe('2');
    expect(areaAcresToSections('2400.0096275320875', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaAcresToSections('nope', true)).toThrow(
      `areaAcresToSections: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareYards, take a number for acres and optional arg for string return, and return the value converted to sq yds', () => {
    expect(areaAcresToSquareYards(1)).toBe(4840);
    expect(areaAcresToSquareYards(2, true)).toBe('9680');
    expect(areaAcresToSquareYards('3.75', true)).toBe('18150');
    // @ts-ignore
    expect(() => areaAcresToSquareYards('nope', true)).toThrow(
      `areaAcresToSquareYards: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareYardsToAcres, take a number for sq yds and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareYardsToAcres(4840)).toBe(1);
    expect(areaSquareYardsToAcres(9680, true)).toBe('2');
    expect(areaSquareYardsToAcres('18150', true)).toBe('3.75');
    // @ts-ignore
    expect(() => areaSquareYardsToAcres('nope', true)).toThrow(
      `areaSquareYardsToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareMeters, take a number for acres and optional arg for string return, and return the value converted to sq meters', () => {
    expect(areaAcresToSquareMeters(1)).toBe(4046.8564224);
    expect(areaAcresToSquareMeters(2, true)).toBe('8093.7128448');
    expect(areaAcresToSquareMeters('5.6', true)).toBe('22662.39596544');
    // @ts-ignore
    expect(() => areaAcresToSquareMeters('nope', true)).toThrow(
      `areaAcresToSquareMeters: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareMetersToAcres, take a number for sq meters and optional arg for string return, and return the value converted to acres', () => {
    expect(areaSquareMetersToAcres(4046.8564224)).toBe(1);
    expect(areaSquareMetersToAcres(8093.7128448, true)).toBe('2');
    expect(areaSquareMetersToAcres('22662.39596544', true)).toBe('5.6');
    // @ts-ignore
    expect(() => areaSquareMetersToAcres('nope', true)).toThrow(
      `areaSquareMetersToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaSquareKilometersToAcres, take a number for square km and optional arg for string return, and return the value converted to acres (intl)', () => {
    expect(areaSquareKilometersToAcres(1)).toBe(247.10538146716533);
    expect(areaSquareKilometersToAcres(2, true)).toBe('494.21076293433066');
    expect(areaSquareKilometersToAcres('5.6', true)).toBe('1383.790136216125848');
    // @ts-ignore
    expect(() => areaSquareKilometersToAcres('nope', true)).toThrow(
      `areaSquareKilometersToAcres: ${TWO_NUM_ERROR}`
    );
  });

  it('should call areaAcresToSquareKilometers, take a number for acres (intl) and optional arg for string return, and return the value converted to sq km', () => {
    expect(areaAcresToSquareKilometers(247.10538146716533)).toBe(1);
    expect(areaAcresToSquareKilometers(494.21076293433066, true)).toBe('2');
    expect(areaAcresToSquareKilometers('1383.790136216125848', true)).toBe('5.6');
    // @ts-ignore
    expect(() => areaAcresToSquareKilometers('nope', true)).toThrow(
      `areaAcresToSquareKilometers: ${TWO_NUM_ERROR}`
    );
  });
});
