import {
  dataStorageBToGb,
  dataStorageBToKb,
  dataStorageBToMb,
  dataStorageBToPb,
  dataStorageBToTb,
  dataStorageGbToB,
  dataStorageGbToKb,
  dataStorageGbToMb,
  dataStorageGbToTb,
  dataStorageKbToB,
  dataStorageKbToGb,
  dataStorageKbToMb,
  dataStorageKbToPb,
  dataStorageKbToTb,
  dataStorageMbToB,
  dataStorageMbToGb,
  dataStorageMbToKb,
  dataStorageMbToPb,
  dataStorageMbToTb,
  dataStoragePbToB,
  dataStoragePbToKb,
  dataStoragePbToMb,
  dataStoragePbToTb,
  dataStorageTbToB,
  dataStorageTbToGb,
  dataStorageTbToKb,
  dataStorageTbToMb,
  dataStorageTbToPb,
  dataStorageGbToPb,
  dataStoragePbToGb,
  dataStorageToBd,
  dataStorageToCd,
  dataStorageToDvd
} from '..';
import { INVALID_UNIT, ONE_NUM_ERROR, TWO_NUM_ERROR } from '../util/errors';

describe('data storage utils', () => {
  it('should call "dataStorageBToGb" and return the calculated conversion', () => {
    expect(dataStorageBToGb(1073741824)).toEqual(1);
    expect(dataStorageBToGb(2147483648, true)).toEqual('2');
    expect(dataStorageBToGb('1073741.824', true)).toEqual('0.001');
    // @ts-ignore
    expect(() => dataStorageBToGb(false, true)).toThrow(`dataStorageBToGb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageGbToB" and return the calculated conversion', () => {
    expect(dataStorageGbToB(1)).toEqual(1073741824);
    expect(dataStorageGbToB(2, true)).toEqual('2147483648');
    expect(dataStorageGbToB('0.001', true)).toEqual('1073741.824');
    // @ts-ignore
    expect(() => dataStorageGbToB('nope', true)).toThrow(`dataStorageGbToB: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageBToKb" and return the calculated conversion', () => {
    expect(dataStorageBToKb(1024)).toEqual(1);
    expect(dataStorageBToKb(2048, true)).toEqual('2');
    expect(dataStorageBToKb('40.96', true)).toEqual('0.04');
    // @ts-ignore
    expect(() => dataStorageBToKb('nope', true)).toThrow(`dataStorageBToKb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageKbToB" and return the calculated conversion', () => {
    expect(dataStorageKbToB(1)).toEqual(1024);
    expect(dataStorageKbToB(2, true)).toEqual('2048');
    expect(dataStorageKbToB('0.4', true)).toEqual('409.6');
    // @ts-ignore
    expect(() => dataStorageKbToB('nope', true)).toThrow(`dataStorageKbToB: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageBToMb" and return the calculated conversion', () => {
    expect(dataStorageBToMb(1048576)).toEqual(1);
    expect(dataStorageBToMb(2097152, true)).toEqual('2');
    expect(dataStorageBToMb('5000', true)).toEqual('0.00476837158203125');
    // @ts-ignore
    expect(() => dataStorageBToMb('nope', true)).toThrow(`dataStorageBToMb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageMbToB" and return the calculated conversion', () => {
    expect(dataStorageMbToB(1)).toEqual(1048576);
    expect(dataStorageMbToB(2, true)).toEqual('2097152');
    expect(dataStorageMbToB('0.00476837158203125', true)).toEqual('5000');
    // @ts-ignore
    expect(() => dataStorageMbToB('nope', true)).toThrow(`dataStorageMbToB: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageBToPb" and return the calculated conversion', () => {
    expect(dataStorageBToPb(1125899906842624)).toEqual(1);
    expect(dataStorageBToPb(2251799813685248, true)).toEqual('2');
    expect(dataStorageBToPb('5000', true)).toEqual('0.000000000004440892098500626162');
    // @ts-ignore
    expect(() => dataStorageBToPb('nope', true)).toThrow(`dataStorageBToPb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStoragePbToB" and return the calculated conversion', () => {
    expect(dataStoragePbToB(1)).toEqual(1125899906842624);
    expect(dataStoragePbToB(2, true)).toEqual('2251799813685248');
    expect(dataStoragePbToB('0.000000000004440892098500626162', true)).toEqual(
      '5000.000000000000000343932391129088'
    );
    // @ts-ignore
    expect(() => dataStoragePbToB('nope', true)).toThrow(`dataStoragePbToB: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageBToTb" and return the calculated conversion', () => {
    expect(dataStorageBToTb(1099511627776)).toEqual(1);
    expect(dataStorageBToTb(2199023255552, true)).toEqual('2');
    expect(dataStorageBToTb('5000', true)).toEqual('0.000000004547473508864641189575');
    // @ts-ignore
    expect(() => dataStorageBToTb('nope', true)).toThrow(`dataStorageBToTb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageTbToB" and return the calculated conversion', () => {
    expect(dataStorageTbToB(1)).toEqual(1099511627776);
    expect(dataStorageTbToB(2, true)).toEqual('2199023255552');
    expect(dataStorageTbToB('0.000000004547473508864641189575', true)).toEqual(
      '4999.9999999999999999997852516352'
    );
    // @ts-ignore
    expect(() => dataStorageTbToB('nope', true)).toThrow(`dataStorageTbToB: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageGbToKb" and return the calculated conversion', () => {
    expect(dataStorageGbToKb(1)).toEqual(1048576);
    expect(dataStorageGbToKb(2, true)).toEqual('2097152');
    expect(dataStorageGbToKb('0.00476837158203125', true)).toEqual('5000');
    // @ts-ignore
    expect(() => dataStorageGbToKb('nope', true)).toThrow(`dataStorageGbToKb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageKbToGb" and return the calculated conversion', () => {
    expect(dataStorageKbToGb(1048576)).toEqual(1);
    expect(dataStorageKbToGb(2097152, true)).toEqual('2');
    expect(dataStorageKbToGb('5000', true)).toEqual('0.00476837158203125');
    // @ts-ignore
    expect(() => dataStorageKbToGb('nope', true)).toThrow(`dataStorageKbToGb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageGbToMb" and return the calculated conversion', () => {
    expect(dataStorageGbToMb(1)).toEqual(1024);
    expect(dataStorageGbToMb(2, true)).toEqual('2048');
    expect(dataStorageGbToMb('.55', true)).toEqual('563.2');
    // @ts-ignore
    expect(() => dataStorageGbToMb('nope', true)).toThrow(`dataStorageGbToMb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageMbToGb" and return the calculated conversion', () => {
    expect(dataStorageMbToGb(1024)).toEqual(1);
    expect(dataStorageMbToGb(2048, true)).toEqual('2');
    expect(dataStorageMbToGb('563.2', true)).toEqual('0.55');
    // @ts-ignore
    expect(() => dataStorageMbToGb('nope', true)).toThrow(`dataStorageMbToGb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageGbToTb" and return the calculated conversion', () => {
    expect(dataStorageGbToTb(1024)).toEqual(1);
    expect(dataStorageGbToTb(2048, true)).toEqual('2');
    expect(dataStorageGbToTb('563.2', true)).toEqual('0.55');
    // @ts-ignore
    expect(() => dataStorageGbToTb('nope', true)).toThrow(`dataStorageGbToTb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageTbToGb" and return the calculated conversion', () => {
    expect(dataStorageTbToGb(1)).toEqual(1024);
    expect(dataStorageTbToGb(2, true)).toEqual('2048');
    expect(dataStorageTbToGb('.55', true)).toEqual('563.2');
    // @ts-ignore
    expect(() => dataStorageTbToGb('nope', true)).toThrow(`dataStorageTbToGb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageKbToMb" and return the calculated conversion', () => {
    expect(dataStorageKbToMb(1024)).toEqual(1);
    expect(dataStorageKbToMb(2048, true)).toEqual('2');
    expect(dataStorageKbToMb('563.2', true)).toEqual('0.55');
    // @ts-ignore
    expect(() => dataStorageKbToMb('nope', true)).toThrow(`dataStorageKbToMb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageMbToKb" and return the calculated conversion', () => {
    expect(dataStorageMbToKb(1)).toEqual(1024);
    expect(dataStorageMbToKb(2, true)).toEqual('2048');
    expect(dataStorageMbToKb('.55', true)).toEqual('563.2');
    // @ts-ignore
    expect(() => dataStorageMbToKb('nope', true)).toThrow(`dataStorageMbToKb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageKbToPb" and return the calculated conversion', () => {
    expect(dataStorageKbToPb(1099511627776)).toEqual(1);
    expect(dataStorageKbToPb(2199023255552, true)).toEqual('2');
    expect(dataStorageKbToPb('5000', true)).toEqual('0.000000004547473508864641189575');
    // @ts-ignore
    expect(() => dataStorageKbToPb('nope', true)).toThrow(`dataStorageKbToPb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStoragePbToKb" and return the calculated conversion', () => {
    expect(dataStoragePbToKb(1)).toEqual(1099511627776);
    expect(dataStoragePbToKb(2, true)).toEqual('2199023255552');
    expect(dataStoragePbToKb('0.000000004547473508864641189575', true)).toEqual(
      '4999.9999999999999999997852516352'
    );
    // @ts-ignore
    expect(() => dataStoragePbToKb('nope', true)).toThrow(`dataStoragePbToKb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageKbToTb" and return the calculated conversion', () => {
    expect(dataStorageKbToTb(1073741824)).toEqual(1);
    expect(dataStorageKbToTb(2147483648, true)).toEqual('2');
    expect(dataStorageKbToTb('1073741.824', true)).toEqual('0.001');
    // @ts-ignore
    expect(() => dataStorageKbToTb('nope', true)).toThrow(`dataStorageKbToTb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageTbToKb" and return the calculated conversion', () => {
    expect(dataStorageTbToKb(1)).toEqual(1073741824);
    expect(dataStorageTbToKb(2, true)).toEqual('2147483648');
    expect(dataStorageTbToKb('0.001', true)).toEqual('1073741.824');
    // @ts-ignore
    expect(() => dataStorageTbToKb('nope', true)).toThrow(`dataStorageTbToKb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageMbToPb" and return the calculated conversion', () => {
    expect(dataStorageMbToPb(1073741824)).toEqual(1);
    expect(dataStorageMbToPb(2147483648, true)).toEqual('2');
    expect(dataStorageMbToPb('1073741.824', true)).toEqual('0.001');
    // @ts-ignore
    expect(() => dataStorageMbToPb('nope', true)).toThrow(`dataStorageMbToPb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStoragePbToMb" and return the calculated conversion', () => {
    expect(dataStoragePbToMb(1)).toEqual(1073741824);
    expect(dataStoragePbToMb(2, true)).toEqual('2147483648');
    expect(dataStoragePbToMb('0.001', true)).toEqual('1073741.824');
    // @ts-ignore
    expect(() => dataStoragePbToMb('nope', true)).toThrow(`dataStoragePbToMb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageMbToTb" and return the calculated conversion', () => {
    expect(dataStorageMbToTb(1048576)).toEqual(1);
    expect(dataStorageMbToTb(2097152, true)).toEqual('2');
    expect(dataStorageMbToTb('5000', true)).toEqual('0.00476837158203125');
    // @ts-ignore
    expect(() => dataStorageMbToTb('nope', true)).toThrow(`dataStorageMbToTb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageTbToMb" and return the calculated conversion', () => {
    expect(dataStorageTbToMb(1)).toEqual(1048576);
    expect(dataStorageTbToMb(2, true)).toEqual('2097152');
    expect(dataStorageTbToMb('0.00476837158203125', true)).toEqual('5000');
    // @ts-ignore
    expect(() => dataStorageTbToMb('nope', true)).toThrow(`dataStorageTbToMb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStoragePbToTb" and return the calculated conversion', () => {
    expect(dataStoragePbToTb(1)).toEqual(1024);
    expect(dataStoragePbToTb(2, true)).toEqual('2048');
    expect(dataStoragePbToTb('.55', true)).toEqual('563.2');
    // @ts-ignore
    expect(() => dataStoragePbToTb('nope', true)).toThrow(`dataStoragePbToTb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageTbToPb" and return the calculated conversion', () => {
    expect(dataStorageTbToPb(1024)).toEqual(1);
    expect(dataStorageTbToPb(2048, true)).toEqual('2');
    expect(dataStorageTbToPb('563.2', true)).toEqual('0.55');
    // @ts-ignore
    expect(() => dataStorageTbToPb('nope', true)).toThrow(`dataStorageTbToPb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageGbToPb" and return the calculated conversion', () => {
    expect(dataStorageGbToPb(1048576)).toEqual(1);
    expect(dataStorageGbToPb(2097152, true)).toEqual('2');
    expect(dataStorageGbToPb('5000', true)).toEqual('0.00476837158203125');
    // @ts-ignore
    expect(() => dataStorageGbToPb('nope', true)).toThrow(`dataStorageGbToPb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStoragePbToGb" and return the calculated conversion', () => {
    expect(dataStoragePbToGb(1)).toEqual(1048576);
    expect(dataStoragePbToGb(2, true)).toEqual('2097152');
    expect(dataStoragePbToGb('0.00476837158203125', true)).toEqual('5000');
    // @ts-ignore
    expect(() => dataStoragePbToGb('nope', true)).toThrow(`dataStoragePbToGb: ${TWO_NUM_ERROR}`);
  });

  it('should call "dataStorageToCd" and return the calculated conversion', () => {
    expect(dataStorageToCd(650, 'mb')).toEqual(1);
    expect(dataStorageToCd('1300', 'mb', true)).toEqual('2');
    expect(dataStorageToCd(1, 'gb')).toEqual(1.5753846153846154);
    expect(dataStorageToCd('1', 'tb')).toEqual(1613.1938461538462);
    expect(dataStorageToCd(1024 * 650, 'kb')).toEqual(1);
    expect(dataStorageToCd(1024 * 1024 * 650, 'b')).toEqual(1);
    expect(dataStorageToCd(1, 'pb', true)).toEqual('1651910.498461538461538461538461538462');
    // @ts-ignore
    expect(() => dataStorageToCd('nope', true)).toThrow(`dataStorageToCd: ${INVALID_UNIT}`);
    // @ts-ignore
    expect(() => dataStorageToCd('nope', 'mb', true)).toThrow(`dataStorageToCd: ${ONE_NUM_ERROR}`);
  });

  it('should call "dataStorageToDvd" and return the calculated conversion', () => {
    expect(dataStorageToDvd(4485.12, 'mb')).toEqual(1);
    expect(dataStorageToDvd('8970.24', 'mb', true)).toEqual('2');
    expect(dataStorageToDvd(4.38, 'gb')).toEqual(1);
    expect(dataStorageToDvd('1', 'tb')).toEqual(233.78995433789953);
    expect(dataStorageToDvd(1024 * 1024 * 4.38, 'kb')).toEqual(1);
    expect(dataStorageToDvd(1024 * 1024 * 1024 * 4.38, 'b')).toEqual(1);
    expect(dataStorageToDvd(1, 'pb', true)).toEqual('239400.913242009132420091324200913242');
    // @ts-ignore
    expect(() => dataStorageToDvd('nope', true)).toThrow(`dataStorageToDvd: ${INVALID_UNIT}`);
    // @ts-ignore
    expect(() => dataStorageToDvd('nope', 'mb', true)).toThrow(
      `dataStorageToDvd: ${ONE_NUM_ERROR}`
    );
  });

  it('should call "dataStorageToBd" and return the calculated conversion', () => {
    expect(dataStorageToBd(1024 * 25, 'mb')).toEqual(1);
    expect(dataStorageToBd(1024 * 25 * 2, 'mb', true)).toEqual('2');
    expect(dataStorageToBd(25, 'gb')).toEqual(1);
    expect(dataStorageToBd('1', 'tb')).toEqual(40.96);
    expect(dataStorageToBd(1024 * 1024 * 25, 'kb')).toEqual(1);
    expect(dataStorageToBd(1024 * 1024 * 1024 * 25, 'b')).toEqual(1);
    expect(dataStorageToBd(1, 'pb', true)).toEqual('41943.04');
    // @ts-ignore
    expect(() => dataStorageToBd('nope', true)).toThrow(`dataStorageToBd: ${INVALID_UNIT}`);
    // @ts-ignore
    expect(() => dataStorageToBd('nope', 'mb', true)).toThrow(`dataStorageToBd: ${ONE_NUM_ERROR}`);
  });
});
