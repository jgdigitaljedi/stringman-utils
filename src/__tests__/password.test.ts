import { passwordBuildRegex, passwordIsValid, passwordIsValidAdvanced } from '../';

describe('passwordUtils', () => {
  it("should call 'isValid' and do a basic validity check to see if password meets given params", () => {
    expect(
      passwordIsValid('Test_-123', {
        cap: true,
        lc: true,
        num: true,
        min: 8,
        max: 12,
        special: '-_'
      })
    ).toBe(true);

    // does not meet min requirement
    expect(
      passwordIsValid('Test_-123', {
        cap: true,
        lc: true,
        num: true,
        min: 12,
        max: 15,
        special: '-_'
      })
    ).toBe(false);

    // does not meet max requirement
    expect(
      passwordIsValid('Test_-123', {
        cap: true,
        lc: true,
        num: true,
        min: 1,
        max: 6,
        special: '-_'
      })
    ).toBe(false);

    // does not meet special requirement
    expect(passwordIsValid('Test_-123', { cap: true, lc: true, num: true, min: 8, max: 12 })).toBe(
      false
    );

    // does not meet capital letter requirement
    expect(
      passwordIsValid('Test_-123', {
        cap: false,
        lc: true,
        num: true,
        min: 8,
        max: 12,
        special: '-_'
      })
    ).toBe(false);
  });

  it("should call 'isValidAdvanced' and take password and more complicated validation arguments and checks if password meets requirements", () => {
    // password has minimum requirements
    expect(
      passwordIsValidAdvanced(
        'Test-1234_A',
        {
          cap: true,
          lc: true,
          num: true,
          min: 10,
          max: 15,
          special: '-_'
        },
        {
          capital: 2,
          num: 2,
          special: 1
        }
      )
    ).toBe(true);

    // password does not have minimum requirements
    expect(
      passwordIsValidAdvanced(
        'Test-1234_A',
        {
          cap: true,
          lc: true,
          num: true,
          min: 10,
          max: 15,
          special: '-_'
        },
        {
          capital: 4,
          num: 2,
          special: 1
        }
      )
    ).toBe(false);

    // meets 3rd argument requirements but not options requirements
    expect(
      passwordIsValidAdvanced(
        'Test-1234_A',
        {
          cap: true,
          lc: true,
          num: true,
          min: 22,
          max: 55,
          special: '-_'
        },
        {
          capital: 4,
          num: 2,
          special: 1
        }
      )
    ).toBe(false);

    // testing that returns regular isValid check for anyone not calling via TS or ignoring linters
    expect(
      // @ts-ignore
      passwordIsValidAdvanced('Test-1234_A', {
        cap: true,
        lc: true,
        num: true,
        min: 10,
        max: 15,
        special: '-_'
      })
    ).toBe(true);
  });

  it('should call "buildRegex", take the object of params, and return the expected RegExp', () => {
    expect(
      passwordBuildRegex({
        cap: true,
        lc: true,
        num: true,
        min: 22,
        max: 55,
        special: '-_'
      })
    ).toEqual(/^[a-zA-Z0-9-_]{22,55}$/);
  });
});
