import {
  whitespaceRemoveAll,
  whitespaceRemoveBreaks,
  whitespaceRemoveTabs,
  whitespaceReplaceWith,
  whitespaceSingleSpace
} from '../lib/whitespaceUtils';

describe('whitespaceUtils', () => {
  it("should call 'removeBreaks', take a string and return that string with all carriage returns and line breaks removed", () => {
    expect(whitespaceRemoveBreaks('this line\n has a\n dumb amount\r of breaks\n')).toBe(
      'this line has a dumb amount of breaks'
    );
  });

  it("should call 'removeTabs', take a string and return that string with all tabs removed", () => {
    expect(whitespaceRemoveTabs('this line\t has a\t dumb amount \tof tabs\t')).toBe(
      'this line has a dumb amount of tabs'
    );
  });

  it("should call 'removeAll', take a string and return that string with all whitespace removed", () => {
    expect(
      whitespaceRemoveAll(
        'this line\n has a\n dumb amount\r of breaks\n and\t    tabs    and spaces'
      )
    ).toBe('thislinehasadumbamountofbreaksandtabsandspaces');
  });

  it("should call 'singleSpace', takes a string, replace all instances or 2 or more consecutive spaces with a single space, and return a string", () => {
    expect(
      whitespaceSingleSpace('this   line     will  end   up   being   spaced      normally!')
    ).toBe('this line will end up being spaced normally!');
  });

  it("should call 'replaceWith', take a string, an enumerable object with boolean values to detrermine what will be replaced, and another string to replace things with and return the result of replacing the values designated in the 2nd argument with the contents of the 3 argument", () => {
    expect(
      whitespaceReplaceWith('gonna just\n remove breaks\n from this\n', { breaks: true }, ' ', true)
    ).toBe('gonna just remove breaks from this');

    expect(
      whitespaceReplaceWith(
        'gonna   make\t a   \nridiculous example',
        { tabs: true, breaks: true, multiSpace: true },
        '$'
      )
    ).toBe('gonna$make$ a$$ridiculous example');
  });
});
