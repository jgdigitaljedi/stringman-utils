// volume.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform volume
 * conversions from one unit to another.
 * example: gallons to liters
 *
 *
 * General naming convention:
 * - camel cased
 * - `volume` prefix
 * - `volume<current unit>To<desired unit>`
 * - `volumeGallonsToLiters`
 *
 * @module volume utils
 **/

import { metricStep } from '../../util/constants/common.constants';
import {
  ozInCup,
  ozInGallons,
  cupsInPintsInQuarts,
  quartsInGallon,
  mlInFlOz,
  litersInGallon,
  tspInTbsp,
  tbspInFlOz,
  tspInFlOz,
  mlInTsp,
  mlInTbsp
} from '../../util/constants/volume.constants';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

/**
 * Takes a number (can be string) of fluid ounces, and optional arg to return as a string, and returns number converted to cups
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeFlOzToCups } from 'stringman-utils';
 * // or
 * const volumeFlOzToCups = require('stringman-utils').volumeFlOzToCups;
 *
 * const flCups = volumeFlOzToCups(8);
 * console.log(flCups); // 1
 * ```
 *
 * @export
 * @param {(number | string)} oz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeFlOzToCups(oz: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(oz, ozInCup, bigString, 'volumeFlOzToCups');
}

/**
 * Takes a number (can be string) of cups, and optional arg to return as a string, and returns number converted to fluid ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeCupsToFlOz } from 'stringman-utils';
 * // or
 * const volumeCupsToFlOz = require('stringman-utils').volumeCupsToFlOz;
 *
 * const cupsFl = volumeCupsToFlOz(1);
 * console.log(cupsFl); // 8
 * ```
 *
 * @export
 * @param {(number | string)} cups
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeCupsToFlOz(cups: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(cups, ozInCup, bigString, 'volumeCupsToFlOz');
}

/**
 * Takes a number (can be string) of cups, and optional arg to return as a string, and returns number converted to pints
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeCupsToPints } from 'stringman-utils';
 * // or
 * const volumeCupsToPints = require('stringman-utils').volumeCupsToPints;
 *
 * const cupsPints = volumeCupsToPints(2);
 * console.log(cupsPints); // 1
 * ```
 *
 * @export
 * @param {(number | string)} cups
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeCupsToPints(cups: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(cups, cupsInPintsInQuarts, bigString, 'volumeCupsToPints');
}

/**
 * Takes a number (can be string) of pints, and optional arg to return as a string, and returns number converted to cups
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumePintsToCups } from 'stringman-utils';
 * // or
 * const volumePintsToCups = require('stringman-utils').volumePintsToCups;
 *
 * const pintsCups = volumePintsToCups(1);
 * console.log(pintsCups); // 2
 * ```
 *
 * @export
 * @param {(number | string)} pints
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumePintsToCups(pints: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(pints, cupsInPintsInQuarts, bigString, 'volumePintsToCups');
}

/**
 * Takes a number (can be string) of pints, and optional arg to return as a string, and returns number converted to quarts
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumePintsToQuarts } from 'stringman-utils';
 * // or
 * const volumePintsToQuarts = require('stringman-utils').volumePintsToQuarts;
 *
 * const pintsQuarts = volumePintsToQuarts(2);
 * console.log(pintsQuarts); // 1
 * ```
 *
 * @export
 * @param {(number | string)} pints
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumePintsToQuarts(pints: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(pints, cupsInPintsInQuarts, bigString, 'volumePintsToQuarts');
}

/**
 * Takes a number (can be string) of pints, and optional arg to return as a string, and returns number converted to quarts
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeQuartsToPints } from 'stringman-utils';
 * // or
 * const volumeQuartsToPints = require('stringman-utils').volumeQuartsToPints;
 *
 * const quartsPints = volumeQuartsToPints(1);
 * console.log(quartsPints); // 2
 * ```
 *
 * @export
 * @param {(number | string)} quarts
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeQuartsToPints(quarts: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(quarts, cupsInPintsInQuarts, bigString, 'volumeQuartsToPints');
}

/**
 * Takes a number (can be string) of quarts, and optional arg to return as a string, and returns number converted to gallons
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeQuartsToGallons } from 'stringman-utils';
 * // or
 * const volumeQuartsToGallons = require('stringman-utils').volumeQuartsToGallons;
 *
 * const quartsGallons = volumeQuartsToGallons(4);
 * console.log(quartsGallons); // 1
 * ```
 *
 * @export
 * @param {(number | string)} quarts
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeQuartsToGallons(
  quarts: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(quarts, quartsInGallon, bigString, 'volumeQuartsToGallons');
}

/**
 * Takes a number (can be string) of gallons, and optional arg to return as a string, and returns number converted to quarts
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeGallonsToQuarts } from 'stringman-utils';
 * // or
 * const volumeGallonsToQuarts = require('stringman-utils').volumeGallonsToQuarts;
 *
 * const gallonsQuarts = volumeGallonsToQuarts(1);
 * console.log(gallonsQuarts); // 4
 * ```
 *
 * @export
 * @param {(number | string)} gallons
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeGallonsToQuarts(
  gallons: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(gallons, quartsInGallon, bigString, 'volumeGallonsToQuarts');
}

/**
 * Takes a number (can be string) of ml, and optional arg to return as a string, and returns number converted to liters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeMlToLiters } from 'stringman-utils';
 * // or
 * const volumeMlToLiters = require('stringman-utils').volumeMlToLiters;
 *
 * const mlLiters = volumeMlToLiters(1000);
 * console.log(mlLiters); // 1
 * ```
 *
 * @export
 * @param {(number | string)} ml
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeMlToLiters(ml: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(ml, metricStep, bigString, 'volumeMlToLiters');
}

/**
 * Takes a number (can be string) of liters, and optional arg to return as a string, and returns number converted to ml
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeLitersToMl } from 'stringman-utils';
 * // or
 * const volumeLitersToMl = require('stringman-utils').volumeLitersToMl;
 *
 * const litersMl = volumeLitersToMl(1);
 * console.log(litersMl); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} liters
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeLitersToMl(liters: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(liters, metricStep, bigString, 'volumeLitersToMl');
}

/**
 * Takes a number (can be string) of ml, and optional arg to return as a string, and returns number converted to fluid ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeMlToFlOz } from 'stringman-utils';
 * // or
 * const volumeMlToFlOz = require('stringman-utils').volumeMlToFlOz;
 *
 * const mlFl = volumeMlToFlOz(29.57);
 * console.log(mlFl); // 1
 * ```
 *
 * @export
 * @param {(number | string)} ml
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeMlToFlOz(ml: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(ml, mlInFlOz, bigString, 'volumeMlToFlOz');
}

/**
 * Takes a number (can be string) of fluid ounces, and optional arg to return as a string, and returns number converted to ml
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeFlOzToMl } from 'stringman-utils';
 * // or
 * const volumeFlOzToMl = require('stringman-utils').volumeFlOzToMl;
 *
 * const flMl = volumeFlOzToMl(0.03381805884342238755);
 * console.log(flMl); // 1
 * ```
 *
 * @export
 * @param {(number | string)} flOz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeFlOzToMl(flOz: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(flOz, mlInFlOz, bigString, 'volumeFlOzToMl');
}

/**
 * Takes a number (can be string) of liters, and optional arg to return as a string, and returns number converted to gallons
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeLitersToGallons } from 'stringman-utils';
 * // or
 * const volumeLitersToGallons = require('stringman-utils').volumeLitersToGallons;
 *
 * const litersGallons = volumeLitersToGallons(3.785411784);
 * console.log(litersGallons); // 1
 * ```
 *
 * @export
 * @param {(number | string)} liters
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeLitersToGallons(
  liters: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(liters, litersInGallon, bigString, 'volumeLitersToGallons');
}

/**
 * Takes a number (can be string) of gallons, and optional arg to return as a string, and returns number converted to liters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeGallonsToLiters } from 'stringman-utils';
 * // or
 * const volumeGallonsToLiters = require('stringman-utils').volumeGallonsToLiters;
 *
 * const gallonsLiters = volumeGallonsToLiters(1);
 * console.log(gallonsLiters); // 3.785411784
 * ```
 *
 * @export
 * @param {(number | string)} gallons
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeGallonsToLiters(
  gallons: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(gallons, litersInGallon, bigString, 'volumeGallonsToLiters');
}

/**
 * Takes a number (can be string) of fluid ounces, and optional arg to return as a string, and returns number converted to gallons
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeFlOzToGallons } from 'stringman-utils';
 * // or
 * const volumeFlOzToGallons = require('stringman-utils').volumeFlOzToGallons;
 *
 * const flGallons = volumeFlOzToGallons(128);
 * console.log(flGallons); // 1
 * ```
 *
 * @export
 * @param {(number | string)} flOz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeFlOzToGallons(flOz: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(flOz, ozInGallons, bigString, 'volumeFlOzToGallons');
}

/**
 * Takes a number (can be string) of gallons, and optional arg to return as a string, and returns number converted to fluid ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeGallonsToFlOz } from 'stringman-utils';
 * // or
 * const volumeGallonsToFlOz = require('stringman-utils').volumeGallonsToFlOz;
 *
 * const gallonsFl = volumeGallonsToFlOz(1);
 * console.log(gallonsFl); // 128
 * ```
 *
 * @export
 * @param {(number | string)} gallons
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeGallonsToFlOz(
  gallons: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(gallons, ozInGallons, bigString, 'volumeGallonsToFlOz');
}

/**
 * Takes a number (can be string) of teaspoons (US), and optional arg to return as a string, and returns number converted to tablespoons (US)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeTspToTbsp } from 'stringman-utils';
 * // or
 * const volumeTspToTbsp = require('stringman-utils').volumeTspToTbsp;
 *
 * const tspTbsp = volumeTspToTbsp(3);
 * console.log(tspTbsp); // 1
 * ```
 *
 * @export
 * @param {(number | string)} tsp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeTspToTbsp(tsp: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(tsp, tspInTbsp, bigString, 'volumeTspToTbsp');
}

/**
 * Takes a number (can be string) of tablespoons (US), and optional arg to return as a string, and returns number converted to teaspoons (US)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeTbspToTsp } from 'stringman-utils';
 * // or
 * const volumeTbspToTsp = require('stringman-utils').volumeTbspToTsp;
 *
 * const tbspTsp = volumeTbspToTsp(1);
 * console.log(tbspTsp); // 3
 * ```
 *
 * @export
 * @param {(number | string)} tbsp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeTbspToTsp(tbsp: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(tbsp, tspInTbsp, bigString, 'volumeTbspToTsp');
}

/**
 * Takes a number (can be string) of tablespoons (US), and optional arg to return as a string, and returns number converted to fluid ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeTbspToFlOz } from 'stringman-utils';
 * // or
 * const volumeTbspToFlOz = require('stringman-utils').volumeTbspToFlOz;
 *
 * const tbspFloz = volumeTbspToFlOz(2);
 * console.log(tbspFloz); // 1
 * ```
 *
 * @export
 * @param {(number | string)} tbsp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeTbspToFlOz(tbsp: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(tbsp, tbspInFlOz, bigString, 'volumeTbspToFlOz');
}

/**
 * Takes a number (can be string) of fluid ounces, and optional arg to return as a string, and returns number converted to tablespoons (US)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeFlOzToTbsp } from 'stringman-utils';
 * // or
 * const volumeFlOzToTbsp = require('stringman-utils').volumeFlOzToTbsp;
 *
 * const flozTbsp = volumeFlOzToTbsp(1);
 * console.log(flozTbsp); // 2
 * ```
 *
 * @export
 * @param {(number | string)} flOz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeFlOzToTbsp(flOz: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(flOz, tbspInFlOz, bigString, 'volumeFlOzToTbsp');
}

/**
 * Takes a number (can be string) of fluid ounces, and optional arg to return as a string, and returns number converted to teaspoons (US)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeFlOzToTsp } from 'stringman-utils';
 * // or
 * const volumeFlOzToTsp = require('stringman-utils').volumeFlOzToTsp;
 *
 * const flozTsp = volumeFlOzToTsp(1);
 * console.log(flozTsp); // 6
 * ```
 *
 * @export
 * @param {(number | string)} flOz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeFlOzToTsp(flOz: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(flOz, tspInFlOz, bigString, 'volumeFlOzToTsp');
}

/**
 * Takes a number (can be string) of teaspoons (US), and optional arg to return as a string, and returns number converted to fluid ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeTspToFlOz } from 'stringman-utils';
 * // or
 * const volumeTspToFlOz = require('stringman-utils').volumeTspToFlOz;
 *
 * const tspFloz = volumeTspToFlOz(6);
 * console.log(tspFloz); // 1
 * ```
 *
 * @export
 * @param {(number | string)} tsp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeTspToFlOz(tsp: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(tsp, tspInFlOz, bigString, 'volumeTspToFlOz');
}

/**
 * Takes a number (can be string) of teaspoons (US), and optional arg to return as a string, and returns number converted to millileters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeTspToMl } from 'stringman-utils';
 * // or
 * const volumeTspToMl = require('stringman-utils').volumeTspToMl;
 *
 * const tspMl = volumeTspToMl(1);
 * console.log(tspMl); // 4.9289215937
 * ```
 *
 * @export
 * @param {(number | string)} tsp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeTspToMl(tsp: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(tsp, mlInTsp, bigString, 'volumeTspToMl');
}

/**
 * Takes a number (can be string) of millileters, and optional arg to return as a string, and returns number converted to teaspoons (US)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeMlToTsp } from 'stringman-utils';
 * // or
 * const volumeMlToTsp = require('stringman-utils').volumeMlToTsp;
 *
 * const mlTsp = volumeMlToTsp(4.9289215937);
 * console.log(mlTsp); // 1
 * ```
 *
 * @export
 * @param {(number | string)} ml
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeMlToTsp(ml: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(ml, mlInTsp, bigString, 'volumeMlToTsp');
}

/**
 * Takes a number (can be string) of tablespoons (US), and optional arg to return as a string, and returns number converted to millileters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeTbspToMl } from 'stringman-utils';
 * // or
 * const volumeTbspToMl = require('stringman-utils').volumeTbspToMl;
 *
 * const tbspMl = volumeTbspToMl(1);
 * console.log(tbspMl); // 14.7867647811
 * ```
 *
 * @export
 * @param {(number | string)} tbsp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeTbspToMl(tbsp: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(tbsp, mlInTbsp, bigString, 'volumeTbspToMl');
}

/**
 * Takes a number (can be string) of millileters, and optional arg to return as a string, and returns number converted to tablespoons (US)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { volumeMlToTbsp } from 'stringman-utils';
 * // or
 * const volumeMlToTbsp = require('stringman-utils').volumeMlToTbsp;
 *
 * const mlTbsp = volumeMlToTbsp(14.7867647811);
 * console.log(mlTbsp); // 1
 * ```
 *
 * @export
 * @param {(number | string)} ml
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function volumeMlToTbsp(ml: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(ml, mlInTbsp, bigString, 'volumeMlToTbsp');
}
