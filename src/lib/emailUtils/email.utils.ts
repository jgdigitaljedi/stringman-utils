// email.utils.ts
/**
 * * This is a collection of helper functions that can be used for manipulating strings
 * in different ways regarding email addresses, verifying valid addresses, and more.
 *
 * General naming convention:
 * - camel cased
 * - `email` prefix
 * - `email<desired operation>`
 * - `emailIsValid`
 *
 * @module email utils
 **/

import { emailRegex } from '../../util/constants/regex.constants';
import { stringIsValid, stringRemove, stringRetrieve, stringSwap } from '../stringUtils';

/**
 * Takes a string and returns a valid email address if one is present in the string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { emailRetrieve } from 'stringman-utils';
 * // or
 * const emailRetrieve = require('stringman-utils').emailRetrieve;
 * const test = emailRetrieve('this is my email address test@test.me');
 * console.log(test); // '[test@test.me]'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function emailRetrieve(str: string): string[] {
  return stringRetrieve(str, emailRegex);
}

/**
 * Takes a string and returns boolean to represent whether or not string is a valid email address
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { emailIsValid } from 'stringman-utils';
 * // or
 * const emailIsValid = require('stringman-utils').emailIsValid;
 * const valid = emailIsValid('test@test.me');
 * const invalid = emailIsValid('test-test.me');
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} str
 * @returns {boolean}
 */
export function emailIsValid(str: string): boolean {
  return stringIsValid(str, emailRegex);
}

/**
 * Takes a string and returns the string with any email addresses removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { emailRemove } from 'stringman-utils';
 * // or
 * const emailRemove = require('stringman-utils').emailRemove;
 * const test = emailRemove('my email address is test@test.me');
 * console.log(test); // 'my email address is';
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function emailRemove(str: string): string {
  return stringRemove(str, emailRegex);
}

/**
 * Takes 2 strings (a string with an email address and another string that is a different email address) and returns the first string with the new address swapped
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { emailSwap } from 'stringman-utils';
 * // or
 * const emailSwap = require('stringman-utils').emailSwap;
 * const test = emailSwap('my email address is test@test.me', 'test@test.com');
 * console.log(test); // 'my email address is test@test.com'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} newEmail
 * @returns {string}
 */
export function emailSwap(str: string, newEmail: string): string {
  return stringSwap(str, newEmail, emailRegex);
}
