// semver.utils.ts
/**
 * * This is a collection of helper functions that can be used for manipulating strings
 * in different ways regarding semantic version strings.
 *
 * General naming convention:
 * - camel cased
 * - `semver` prefix
 * - `semver<desired operation>`
 * - `semverIncrement`
 *
 * @module semver utils
 **/

/**
 * Credit to https://github.com/sindresorhus/semver-regex for the regex string
 * I use this and wanted to include it in my own library for convenience
 * IDEAS:
 * - which is newer
 * - which is older
 * - difference between versions
 */

import { SemverWhich } from '../../models/semver.model';
import { semverStrRegex } from '../../util/constants/regex.constants';
import { stringIsValid, stringRemove, stringRetrieve, stringSwap } from '../stringUtils';

/**
 * returns semver from string if present
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { semverRetrieve } from 'stringman-utils';
 * // or
 * const semverRetrieve = require('stringman-utils').semverRetrieve;
 *
 * const justSemver = semverRetrieve('this project started on version 0.1.0');
 * console.log(justSemver); // ['0.1.0']
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(RegExpMatchArray)}
 */
export function semverRetrieve(str: string): RegExpMatchArray {
  return stringRetrieve(str, semverStrRegex);
}

/**
 * Tests if argument is valid semver
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { semverIsValid } from 'stringman-utils';
 * // or
 * const semverIsValid = require('stringman-utils').semverIsValid;
 *
 * const valid = semverIsValid('0.1.0');
 * const invalid = semverIsValid('0,1.0-rt');
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} str
 * @returns {boolean}
 */
export function semverIsValid(str: string): boolean {
  return stringIsValid(str, semverStrRegex);
}

/**
 * Removes semver from string, trims, and returns result
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { semverRemove } from 'stringman-utils';
 * // or
 * const semverRemove = require('stringman-utils').semverRemove;
 *
 * const removed = semverRemove('this project started on version 0.1.0');
 * console.log(removed); // 'this project started on version'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function semverRemove(str: string): string {
  return stringRemove(str, semverStrRegex);
}

/**
 * Takes a string with a semver, a second string, returns first string with semver swapped for second string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { semverSwap } from 'stringman-utils';
 * // or
 * const semverSwap = require('stringman-utils').semverSwap;
 *
 * const swapped = semverSwap('this project started on version 0.1.0', '1.5.8');
 * console.log(swapped); // 'this project started on version 1.5.8'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} newVer
 * @returns {string}
 */
export function semverSwap(str: string, newVer: string): string {
  return stringSwap(str, newVer, semverStrRegex);
}

/**
 * Takes a semantic version string and an arg to determine which number to incremement and returns semver string with that number incremeneted
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { semverIncrement } from 'stringman-utils';
 * // or
 * const semverIncrement = require('stringman-utils').semverIncrement;
 *
 * const major = semverIncrement('1.2.3', 'major');
 * const minor = semverIncrement('1.2.3', 'minor');
 * const patch = semverIncrement('1.2.3', 'patch');
 * console.log(major); // '2.2.3'
 * console.log(minor); // '1.3.3'
 * console.log(patch); // '1.2.4'
 * ```
 *
 * @export
 * @param {string} str
 * @param {SemverWhich} which
 * @return {*}  {string}
 */
export function semverIncrement(str: string, which: SemverWhich): string {
  const svSplit = str.split('.');
  if (svSplit.length !== 3) {
    throw new Error(`THE SEMANTIC VERSION ${str} CANNOT BE INCREMENTED, CHECK FORMAT ("x.x.x")`);
  }
  switch (which) {
    case 'major':
      return `${parseInt(svSplit[0], 10) + 1}.${svSplit[1]}.${svSplit[2]}`;
    case 'minor':
      return `${svSplit[0]}.${parseInt(svSplit[1], 10) + 1}.${svSplit[2]}`;
    case 'patch':
      return `${svSplit[0]}.${svSplit[1]}.${parseInt(svSplit[2], 10) + 1}`;
  }
}

/**
 * Takes a semantic version string and an arg to determine which number to decremement and returns semver string with that number decremeneted
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { semverDecrement } from 'stringman-utils';
 * // or
 * const semverDecrement = require('stringman-utils').semverDecrement;
 *
 * const major = semverDecrement('1.2.3', 'major');
 * const minor = semverDecrement('1.2.3', 'minor');
 * const patch = semverDecrement('1.2.3', 'patch');
 * console.log(major); // '0.2.3'
 * console.log(minor); // '1.1.3'
 * console.log(patch); // '1.2.2'
 * ```
 *
 * @export
 * @param {string} str
 * @param {SemverWhich} which
 * @return {*}  {string}
 */
export function semverDecrement(str: string, which: SemverWhich): string {
  const svSplit = str.split('.');
  if (svSplit.length !== 3) {
    throw new Error(`THE SEMANTIC VERSION ${str} CANNOT BE DECREMENTED, CHECK FORMAT ("x.x.x")`);
  }
  switch (which) {
    case 'major':
      if (svSplit[0] === '0') {
        throw new Error(`CANNOT DECREMENT MAJOR VERSION BELOW ZERO: ${str}`);
      }
      return `${parseInt(svSplit[0], 10) - 1}.${svSplit[1]}.${svSplit[2]}`;
    case 'minor':
      if (svSplit[1] === '0') {
        throw new Error(`CANNOT DECREMENT MINOR VERSION BELOW ZERO: ${str}`);
      }
      return `${svSplit[0]}.${parseInt(svSplit[1], 10) - 1}.${svSplit[2]}`;
    case 'patch':
      if (svSplit[2] === '0') {
        throw new Error(`CANNOT DECREMENT PATCH VERSION BELOW ZERO: ${str}`);
      }
      return `${svSplit[0]}.${svSplit[1]}.${parseInt(svSplit[2], 10) - 1}`;
  }
}
