// temperature.utils.ts
/**
 * * This is a collection of helper functions that can be used to convert temperature
 * values from one unit to another unit.
 * example: fahrenheit to celcius
 *
 *
 * General naming convention:
 * - camel cased
 * - `temp` prefix
 * - `temp<current unit>To<desired unit>`
 * - `tempFahrenheitToCelcius`
 *
 * @module temperature utils
 **/

import {
  celciusToKelvinDifference,
  celciusFahrenheitRatio,
  celciusFahrenheitDifference,
  fahrenheitKelvinDifference
} from '../../util/constants/temperature.constants';
import {
  precisionMathSubtract,
  precisionMathAdd,
  precisionMathDivide,
  precisionMathMultiply
} from '../precisionMathUtils';

/**
 * Take a temp in celcius and optionally whether to return a string, returns that value in fahrenheit
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { tempCelciusToFahrenheit } from 'stringman-utils';
 * // or
 * const tempCelciusToFahrenheit = require('stringman-utils').tempCelciusToFahrenheit;
 *
 * const test = tempCelciusToFahrenheit(32);
 * console.log(test); // 0
 * ```
 *
 * @export
 * @param {(number | string)} farTemp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function tempCelciusToFahrenheit(
  farTemp: number | string,
  bigString?: boolean
): number | string {
  return precisionMathAdd(
    precisionMathMultiply(farTemp, celciusFahrenheitRatio, bigString, 'tempCelciusToFahrenheit'),
    celciusFahrenheitDifference,
    bigString,
    'tempCelciusToFahrenheit'
  );
}

/**
 * Take a temp in fahrenheit and optionally whether to return a string, returns that value in celcius
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { tempFahrenheitToCelcius } from 'stringman-utils';
 * // or
 * const tempFahrenheitToCelcius = require('stringman-utils').tempFahrenheitToCelcius;
 *
 * const test = tempFahrenheitToCelcius(0);
 * console.log(test); // 32
 * ```
 *
 * @export
 * @param {(number | string)} celTemp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function tempFahrenheitToCelcius(
  celTemp: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    precisionMathSubtract(
      celTemp,
      celciusFahrenheitDifference,
      bigString,
      'tempFahrenheitToCelcius'
    ),
    celciusFahrenheitRatio,
    bigString,
    'tempFahrenheitToCelcius'
  );
}

/**
 * Take a temp in kelvin and optionally whether to return a string, returns that value in celcius
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { tempKelvinToCelcius } from 'stringman-utils';
 * // or
 * const tempKelvinToCelcius = require('stringman-utils').tempKelvinToCelcius;
 *
 * const test = tempKelvinToCelcius(0);
 * console.log(test); // -273.15
 * ```
 *
 * @export
 * @param {(number | string)} kelTemp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function tempKelvinToCelcius(
  kelTemp: number | string,
  bigString?: boolean
): number | string {
  return precisionMathSubtract(
    kelTemp,
    celciusToKelvinDifference,
    bigString,
    'tempKelvinToCelcius'
  );
}

/**
 * Take a temp in celcius and optionally whether to return a string, returns that value in kelvin
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { tempCelciusToKelvin } from 'stringman-utils';
 * // or
 * const tempCelciusToKelvin = require('stringman-utils').tempCelciusToKelvin;
 *
 * const test = tempCelciusToKelvin(-273.15);
 * console.log(test); // 0
 * ```
 *
 * @export
 * @param {(number | string)} celTemp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function tempCelciusToKelvin(
  celTemp: number | string,
  bigString?: boolean
): number | string {
  return precisionMathAdd(celTemp, celciusToKelvinDifference, bigString, 'tempCelciusToKelvin');
}

/**
 * Take a temp in fahrenheit and optionally whether to return a string, returns that value in kelvin
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { tempFahrenheitToKelvin } from 'stringman-utils';
 * // or
 * const tempFahrenheitToKelvin = require('stringman-utils').tempFahrenheitToKelvin;
 *
 * const test = tempFahrenheitToKelvin(-459.67);
 * console.log(test); // 0
 * ```
 *
 * @export
 * @param {(number | string)} farTemp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function tempFahrenheitToKelvin(
  farTemp: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    precisionMathAdd(farTemp, fahrenheitKelvinDifference, bigString, 'tempFahrenheitToKelvin'),
    precisionMathDivide(5, 9, bigString, 'tempFahrenheitToKelvin'),
    bigString,
    'tempFahrenheitToKelvin'
  );
}

/**
 * Take a temp in kelvin and optionally whether to return a string, returns that value in fahrenheit
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { tempKelvinToFahrenheit } from 'stringman-utils';
 * // or
 * const tempKelvinToFahrenheit = require('stringman-utils').tempKelvinToFahrenheit;
 *
 * const test = tempKelvinToFahrenheit(0);
 * console.log(test); // -459.67
 * ```
 *
 * @export
 * @param {(number | string)} kelTemp
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function tempKelvinToFahrenheit(
  kelTemp: number | string,
  bigString?: boolean
): number | string {
  return precisionMathAdd(
    precisionMathDivide(
      precisionMathMultiply(
        precisionMathSubtract(
          kelTemp,
          celciusToKelvinDifference,
          bigString,
          'tempKelvinToFahrenheit'
        ),
        9,
        bigString,
        'tempKelvinToFahrenheit'
      ),
      5,
      bigString,
      'tempKelvinToFahrenheit'
    ),
    32,
    bigString,
    'tempKelvinToFahrenheit'
  );
}
