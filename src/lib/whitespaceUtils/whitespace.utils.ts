// whitespace.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various
 * operations regarding whitespace on the string passed.
 * example: removing line breaks
 *
 *
 * General naming convention:
 * - camel cased
 * - `whitespace` prefix
 * - `whitespace<current operation>`
 * - `whitespaceRemoveBreaks`
 *
 * @module whitespace utils
 **/

import { IReplaceWith } from '../../models/whitespace.model';
import { stringRemove } from '../stringUtils';

/**
 * Takes a string and returns that string with all carriage returns and line breaks removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { whitespaceRemoveBreaks } from 'stringman-utils';
 * // or
 * const whitespaceRemoveBreaks = require('stringman-utils').whitespaceRemoveBreaks;
 *
 * const removed = whitespaceRemoveBreaks('this line\n has a\r dumb amount of\n line breaks');
 * console.log(removed); // 'this line has a dumb amount of line breaks'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function whitespaceRemoveBreaks(str: string): string {
  return stringRemove(str, /\n|\r|\\r|\\n/gim);
}

/**
 * Takes a string, removes tabs, and returns result
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { whitespaceRemoveTabs } from 'stringman-utils';
 * // or
 * const whitespaceRemoveTabs = require('stringman-utils').whitespaceRemoveTabs;
 *
 * const removed = whitespaceRemoveTabs('this line   has some   tabs of \t multiple kinds');
 * console.log(removed); // 'this line has some tabs of multiple kinds'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function whitespaceRemoveTabs(str: string): string {
  return stringRemove(str, /\t|\\t/gim);
}

/**
 * Takes a string, removes all forms of witespace, and returns result
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { whitespaceRemoveAll } from 'stringman-utils';
 * // or
 * const whitespaceRemoveAll = require('stringman-utils').whitespaceRemoveAll;
 *
 * const removed = whitespaceRemoveAll('this string\n   is gonne be\r a\t jumbled mess   !');
 * console.log(removed); // 'thisstringisgonnabeajumbledmess!'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function whitespaceRemoveAll(str: string): string {
  return stringRemove(str, /\t|\\t|\n|\\n|\r|\\r| /gim);
}

/**
 * Takes string and replaces any instance of 2 or more consecutive spaces with a single space and returns the result
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { whitespaceSingleSpace } from 'stringman-utils';
 * // or
 * const whitespaceSingleSpace = require('stringman-utils').whitespaceSingleSpace;
 *
 * const single = whitespaceSingleSpace('this string  has    a dumb amount  of   extra spaces         !');
 * console.log(single); // 'this string has a dumb amount of extra spaces !'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function whitespaceSingleSpace(str: string): string {
  return str.replace(/  +/gm, ' ').trim();
}

/**
 * Takes a string, an enumerable object with boolean values to detrermine what will be replaced, another string to replace things, and an optional 4th argument
 * for whether it should be returned with multiple consecutive spaces changed to single spaces with and returns the result of replacing
 * the values designated in the 2nd argument with the contents of the 3 argument
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { whitespaceReplaceWith } from 'stringman-utils';
 * // or
 * const whitespaceReplaceWith = require('stringman-utils').whitespaceReplaceWith;
 *
 * const simple = whitespaceReplaceWith('gonna just\n remove breaks\n from this\n', {breaks: true}, ' ' true);
 * const goofy = whitespaceReplaceWith('gonna   make\t a   \nridiculous example', {tabs: true, breaks: true, multiSpace: true}, '$');
 * console.log(simple); // 'gonna just remove breaks from this'
 * console.log(goofy); // 'gonna$make$ a$$ridiculous example'
 * ```
 *
 * @export
 * @param {string} str
 * @param {IReplaceWith} toReplace
 * @param {string} newStr
 * @param {boolean} [single]
 * @returns {string}
 */
export function whitespaceReplaceWith(
  str: string,
  toReplace: IReplaceWith,
  newStr: string,
  single?: boolean
): string {
  // I went with self assigning because otherwise there would be a ton of variable declarations which slows things down. I'm not married to it though.
  if (!str) {
    return '';
  }
  let result = str;
  if (toReplace.tabs) {
    result = result.replace(/\t|\\t/gim, newStr);
  }
  if (toReplace.breaks) {
    result = result.replace(/\n|\r|\\r|\\n/gim, newStr);
  }
  if (toReplace.multiSpace) {
    result = result.replace(/  +/gm, newStr);
  }
  if (single) {
    return whitespaceSingleSpace(result);
  }
  return result;
}
