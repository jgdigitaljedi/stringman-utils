import {
  whitespaceRemoveAll,
  whitespaceRemoveBreaks,
  whitespaceRemoveTabs,
  whitespaceReplaceWith,
  whitespaceSingleSpace
} from './whitespace.utils';

export {
  whitespaceRemoveAll,
  whitespaceRemoveBreaks,
  whitespaceRemoveTabs,
  whitespaceReplaceWith,
  whitespaceSingleSpace
};
