// weight.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform weight
 * conversions from one unit to another.
 * example: kilograms to pounds
 *
 *
 * General naming convention:
 * - camel cased
 * - `weight` prefix
 * - `weight<current unit>To<desired unit>`
 * - `weightKgToLbs`
 *
 * @module weight utils
 **/

import { metricStep } from '../../util/constants/common.constants';
import {
  lbsInKg,
  lbsInTons,
  ozInGrams,
  ozInLbs,
  lbsToStones,
  kgToStones
} from '../../util/constants/weight.constants';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

/**
 * Takes a number (can be string) of kilograms, and optional arg to return as a string, and returns number converted to pounds
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightKgToLbs } from 'stringman-utils';
 * // or
 * const weightKgToLbs = require('stringman-utils').weightKgToLbs;
 *
 * const kgLbs = weightKgToLbs(1);
 * console.log(kgLbs); // 2.20462262185
 * ```
 *
 * @export
 * @param {(number | string | string)} kg
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightKgToLbs(
  kg: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  return precisionMathMultiply(kg, lbsInKg, bigString, caller || 'weightKgToLbs');
}

/**
 * Takes a number (can be string) of pounds, and optional arg to return as a string, and returns number converted to kilograms
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightLbsToKg } from 'stringman-utils';
 * // or
 * const weightLbsToKg = require('stringman-utils').weightLbsToKg;
 *
 * const lbsKg = weightLbsToKg(2.20462262185);
 * console.log(lbsKg); // 1
 * ```
 *
 * @export
 * @param {(number | string)} lbs
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightLbsToKg(
  lbs: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  return precisionMathDivide(lbs, lbsInKg, bigString, caller || 'weightLbsToKg');
}

/**
 * Takes a number (can be string) of ounces, and optional arg to return as a string, and returns number converted to grams
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightOzToGrams } from 'stringman-utils';
 * // or
 * const weightOzToGrams = require('stringman-utils').weightOzToGrams;
 *
 * const ozG = weightOzToGrams(0.035274);
 * console.log(ozG); // 1
 * ```
 *
 * @export
 * @param {(number | string)} oz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightOzToGrams(oz: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(oz, ozInGrams, bigString, 'weightOzToGrams');
}

/**
 * Takes a number (can be string) of pounds, and optional arg to return as a string, and returns number converted to ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightGramsToOz } from 'stringman-utils';
 * // or
 * const weightGramsToOz = require('stringman-utils').weightGramsToOz;
 *
 * const gOz = weightGramsToOz(1);
 * console.log(gOz); // 0.035274
 * ```
 *
 * @export
 * @param {(number | string)} grams
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightGramsToOz(grams: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(grams, ozInGrams, bigString, 'weightGramsToOz');
}

/**
 * Takes a number (can be string) of ounces, and optional arg to return as a string, and returns number converted to pounds
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightOzToLbs } from 'stringman-utils';
 * // or
 * const weightOzToLbs = require('stringman-utils').weightOzToLbs;
 *
 * const ozLbs = weightOzToLbs(16);
 * console.log(ozLbs); // 1
 * ```
 *
 * @export
 * @param {(number | string)} oz
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightOzToLbs(oz: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(oz, ozInLbs, bigString, 'weightOzToLbs');
}

/**
 * Takes a number (can be string) of pounds, and optional arg to return as a string, and returns number converted to ounces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightLbsToOz } from 'stringman-utils';
 * // or
 * const weightLbsToOz = require('stringman-utils').weightLbsToOz;
 *
 * const lbsOz = weightLbsToOz(1);
 * console.log(lbsOz); // 16
 * ```
 *
 * @export
 * @param {(number | string)} lbs
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightLbsToOz(lbs: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(lbs, ozInLbs, bigString, 'weightLbsToOz');
}

/**
 * Takes a number (can be string) of grams, and optional arg to return as a string, and returns number converted to kilograms
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightGramsToKg } from 'stringman-utils';
 * // or
 * const weightGramsToKg = require('stringman-utils').weightGramsToKg;
 *
 * const gKg = weightGramsToKg(1000);
 * console.log(gKg); // 1
 * ```
 *
 * @export
 * @param {(number | string)} grams
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightGramsToKg(grams: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(grams, metricStep, bigString, 'weightGramsToKg');
}

/**
 * Takes a number (can be string) of kilograms, and optional arg to return as a string, and returns number converted to grams
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightKgToGrams } from 'stringman-utils';
 * // or
 * const weightKgToGrams = require('stringman-utils').weightKgToGrams;
 *
 * const kgG = weightKgToGrams(1);
 * console.log(kgG); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} kg
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightKgToGrams(kg: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(kg, metricStep, bigString, 'weightKgToGrams');
}

/**
 * Takes a number (can be string) of pounds, and optional arg to return as a string, and returns number converted to tons
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightLbsToTons } from 'stringman-utils';
 * // or
 * const weightLbsToTons = require('stringman-utils').weightLbsToTons;
 *
 * const lbsTons = weightLbsToTons(2000);
 * console.log(lbsTons); // 1
 * ```
 *
 * @export
 * @param {(number | string)} lbs
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightLbsToTons(lbs: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(lbs, lbsInTons, bigString, 'weightLbsToTons');
}

/**
 * Takes a number (can be string) of tons, and optional arg to return as a string, and returns number converted to pounds
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightTonsToLbs } from 'stringman-utils';
 * // or
 * const weightTonsToLbs = require('stringman-utils').weightTonsToLbs;
 *
 * const tonsLbs = weightTonsToLbs(1);
 * console.log(tonsLbs); // 2000
 * ```
 *
 * @export
 * @param {(number | string)} tons
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightTonsToLbs(
  tons: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  return precisionMathMultiply(tons, lbsInTons, bigString, caller || 'weightTonsToLbs');
}

/**
 * Takes a number (can be string) of kilograms, and optional arg to return as a string, and returns number converted to tonnes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightKgToTonnes } from 'stringman-utils';
 * // or
 * const weightKgToTonnes = require('stringman-utils').weightKgToTonnes;
 *
 * const kgTonnes = weightKgToTonnes(1000);
 * console.log(kgTonnes); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kg
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightKgToTonnes(
  kg: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  return precisionMathDivide(kg, metricStep, bigString, caller || 'weightKgToTonnes');
}

/**
 * Takes a number (can be string) of kilograms, and optional arg to return as a string, and returns number converted to kilograms
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightTonnesToKg } from 'stringman-utils';
 * // or
 * const weightTonnesToKg = require('stringman-utils').weightTonnesToKg;
 *
 * const tonnesKg = weightTonnesToKg(1);
 * console.log(tonnesKg); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} tonnes
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightTonnesToKg(
  tonnes: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  return precisionMathMultiply(tonnes, metricStep, bigString, caller || 'weightTonnesToKg');
}

/**
 * Takes a number (can be string) of kilograms, and optional arg to return as a string, and returns number converted to tons
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightKgToTons } from 'stringman-utils';
 * // or
 * const weightKgToTons = require('stringman-utils').weightKgToTons;
 *
 * const kgTons = weightKgToTons(907.184739999);
 * console.log(kgTons); // 0.999999999999453
 * ```
 *
 * @export
 * @param {(number | string)} kg
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightKgToTons(kg: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(
    weightKgToLbs(kg, bigString, 'weightKgToTons'),
    lbsInTons,
    bigString,
    'weightKgToTons'
  );
}

/**
 * Takes a number (can be string) of tons, and optional arg to return as a string, and returns number converted to kilograms
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightTonsToKg } from 'stringman-utils';
 * // or
 * const weightTonsToKg = require('stringman-utils').weightTonsToKg;
 *
 * const tonsKg = weightTonsToKg(0.999999999999453);
 * console.log(tonsKg); // 907.184739999
 * ```
 *
 * @export
 * @param {(number | string)} tons
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightTonsToKg(tons: number | string, bigString?: boolean): number | string {
  return weightLbsToKg(
    weightTonsToLbs(tons, bigString, 'weightTonsToKg'),
    bigString,
    'weightTonsToKg'
  );
}

/**
 * Takes a number (can be string) of lbs, and optional arg to return as a string, and returns number converted to tonnes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightLbsToTonnes } from 'stringman-utils';
 * // or
 * const weightLbsToTonnes = require('stringman-utils').weightLbsToTonnes;
 *
 * const lbsTonnes = weightLbsToTonnes(2204.62262185);
 * console.log(lbsTonnes); // 1
 * ```
 *
 * @export
 * @param {(number | string)} lbs
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightLbsToTonnes(lbs: number | string, bigString?: boolean): number | string {
  return weightKgToTonnes(
    weightLbsToKg(lbs, bigString, 'weightLbsToTonnes'),
    bigString,
    'weightLbsToTonnes'
  );
}

/**
 * Takes a number (can be string) of tonnes, and optional arg to return as a string, and returns number converted to pounds
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightTonnesToLbs } from 'stringman-utils';
 * // or
 * const weightTonnesToLbs = require('stringman-utils').weightTonnesToLbs;
 *
 * const tonnesLbs = weightTonnesToLbs(1);
 * console.log(tonnesLbs); // 2204.62262185
 * ```
 *
 * @export
 * @param {(number | string)} tonnes
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightTonnesToLbs(tonnes: number | string, bigString?: boolean): number | string {
  return weightKgToLbs(
    weightTonnesToKg(tonnes, bigString, 'weightTonnesToLbs'),
    bigString,
    'weightTonnesToLbs'
  );
}

/**
 * Takes a number (can be string) of pounds, and optional arg to return as a string, and returns number converted to stones
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightLbsToStone } from 'stringman-utils';
 * // or
 * const weightLbsToStone = require('stringman-utils').weightLbsToStone;
 *
 * const lbsStone = weightLbsToStone(14);
 * console.log(lbsStone); // 1
 * ```
 *
 * @export
 * @param {(number | string)} pounds
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightLbsToStone(pounds: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(pounds, lbsToStones, bigString, 'weightLbsToStone');
}

/**
 * Takes a number (can be string) of stones, and optional arg to return as a string, and returns number converted to pounds
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightStoneToLbs } from 'stringman-utils';
 * // or
 * const weightStoneToLbs = require('stringman-utils').weightStoneToLbs;
 *
 * const stoneLbs = weightStoneToLbs(1);
 * console.log(stoneLbs); // 14
 * ```
 *
 * @export
 * @param {(number | string)} stone
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightStoneToLbs(stone: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(stone, lbsToStones, bigString, 'weightStoneToLbs');
}

/**
 * Takes a number (can be string) of kilograms, and optional arg to return as a string, and returns number converted to stones
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightKgToStone } from 'stringman-utils';
 * // or
 * const weightKgToStone = require('stringman-utils').weightKgToStone;
 *
 * const kgStone = weightKgToStone(6.350293179999988);
 * console.log(kgStone); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kg
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightKgToStone(kg: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(kg, kgToStones, bigString, 'weightKgToStone');
}

/**
 * Takes a number (can be string) of stones, and optional arg to return as a string, and returns number converted to kilograms
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { weightStoneToKg } from 'stringman-utils';
 * // or
 * const weightStoneToKg = require('stringman-utils').weightStoneToKg;
 *
 * const stoneKg = weightStoneToKg(1);
 * console.log(stoneKg); // 6.350293179999988
 * ```
 *
 * @export
 * @param {(number | string)} stone
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function weightStoneToKg(stone: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(stone, kgToStones, bigString, 'weightStoneToKg');
}
