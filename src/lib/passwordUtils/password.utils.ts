// password.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various
 * operations regarding passwords.
 * example: validating a given password against a supplied Regexp
 *
 *
 * General naming convention:
 * - camel cased
 * - `password` prefix
 * - `password<current operation>`
 * - `passwordIsValid`
 *
 * @module password utils
 **/

import { IPwOptions, IPwOptionsCount } from '../../models/password.model';
import { stringIsValid } from '../stringUtils';

/**
 * Takes min, max, and string of special characters and returns a RegExp for validating passwords
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { passwordBuildRegex } from 'stringman-utils';
 * // or
 * const passwordBuildRegex = require('stringman-utils').passwordBuildRegex;
 *
 * const regEx = passwordBuildRegex({cap: true, lc: true, num: true, min: 8, max: 18, special: '-_=+*&'});
 * console.log(regEx); // /^[A-Za-z0-9-_=+*&]{8,18}$/
 * ```
 *
 * @export
 * @param {IPwOptions} options
 * @returns {RegExp}
 */
export function passwordBuildRegex(options: IPwOptions): RegExp {
  return new RegExp(
    `^[${options.lc ? 'a-z' : ''}${options.cap ? 'A-Z' : ''}${options.num ? '0-9' : ''}${
      options.special || ''
    }]{${options.min},${options.max}}$`
  );
}

/**
 * Takes parameters for building a regular expression then validates the string sent and returns a boolean value
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { passwordIsValid } from 'stringman-utils';
 * // or
 * const passwordIsValid = require('stringman-utils').passwordIsValid;
 *
 * const valid = passwordIsValid('Test-12345*', {cap: true, lc: true, num: true, min: 8, max: 18, special: '-_=+*&'});
 * const invalid = passwordIsValid('Test', {cap: true, lc: true, num: true, min: 8, max: 18, special: '-_=+*&'});
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} str
 * @param {IPwOptions} options
 * @returns {boolean}
 */
export function passwordIsValid(str: string, options: IPwOptions): boolean {
  const re = passwordBuildRegex(options);
  return stringIsValid(str, re);
}

/**
 * Validates using regex like isValid but also checks for specific number of different types of characters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { passwordIsValidAdvanced } from 'stringman-utils';
 * // or
 * const passwordIsValidAdvanced = require('stringman-utils').passwordIsValidAdvanced;
 *
 * const valid = passwordIsValidAdvanced('Test-12345*', {cap: true, lc: true, num: true, min: 8, max: 18, special: '-_=+*&'}, {cap: 2, num: 2, special: 1});
 * const invalid = passwordIsValidAdvanced('Test', {cap: true, lc: true, num: true, min: 8, max: 18, special: '-_=+*&'}, {cap: 4, num: 2, special: 1});
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} str
 * @param {IPwOptions} options
 * @param {IPwOptionsCount} advOptions
 * @returns {boolean}
 */
export function passwordIsValidAdvanced(
  str: string,
  options: IPwOptions,
  advOptions: IPwOptionsCount
): boolean {
  if (passwordIsValid(str, options)) {
    if (advOptions) {
      const checkerObj = {
        capital: advOptions.capital ? '[A-Z]' : undefined,
        lc: advOptions.lc ? '[a-z]' : undefined,
        num: advOptions.num ? '[0-9]' : undefined,
        special: options.special && advOptions.special ? `[${options.special}]` : undefined
      };
      return (
        Object.keys(advOptions)
          .map((key: string) => {
            // @ts-ignore
            const ex = checkerObj[key];
            const re = new RegExp(ex, 'g');
            const matched = str.match(re);
            // @ts-ignore
            return matched && matched.length >= advOptions[key];
          })
          .filter((c) => !c).length < 1
      );
    } else {
      return true;
    }
  } else {
    return false;
  }
}
