import { squareRodsInSection } from '../../util/constants/area.constants';
import { precisionMathMultiply } from '../precisionMathUtils';

export function areaSquareChainsToSquareLinks(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareRodsInSection,
    bigString,
    'areaSectionsToSquareRods'
  );
}
