// areaStandardAcres.utils.ts
/**
 * * This is a collection of helper functions that can be used to convert area
 * units into other area units.
 * example: converting square miles to acres
 *
 *
 * General naming convention:
 * - camel cased
 * - `area` prefix
 * - `area<current operation>`
 * - `areaAcresToHectares`
 *
 * @module area utils
 **/

import {
  acresInSection,
  acresInSqMiles,
  acresInTownship,
  hectaresInAcre,
  sqLinksGunInAcre,
  sqRodsInAcres,
  squareFeetInAcre,
  squareMetersInAcre,
  squareYardInAcre,
  acresInSquareKilometer,
  sqChainGunInAcre
} from '../../util/constants/areaStandardAcres.constants';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

/**
 * Takes a number (can be string) of acres (standard), and optional arg to return as a string, and returns number converted to hectares
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToHectares } from 'stringman-utils';
 * // or
 * const areaAcresToHectares = require('stringman-utils').areaAcresToHectares;
 *
 * const acresHectares = areaAcresToHectares(1);
 * console.log(acresHectares); // 0.404685642
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToHectares(acres: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(acres, hectaresInAcre, bigString, 'areaAcresToHectares');
}

/**
 * Takes a number (can be string) of hectares, and optional arg to return as a string, and returns number converted to acres (standard)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaHectaresToAcres } from 'stringman-utils';
 * // or
 * const areaHectaresToAcres = require('stringman-utils').areaHectaresToAcres;
 *
 * const hectaresAcres = areaHectaresToAcres(0.404685642);
 * console.log(hectaresAcres); // 1
 * ```
 *
 * @export
 * @param {(number | string)} hectares
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaHectaresToAcres(
  hectares: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(hectares, hectaresInAcre, bigString, 'areaHectaresToAcres');
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to acres (standard)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToAcres } from 'stringman-utils';
 * // or
 * const areaSquareMilesToAcres = require('stringman-utils').areaSquareMilesToAcres;
 *
 * const sqMiAcres = areaSquareMilesToAcres(1);
 * console.log(sqMiAcres); // 640
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToAcres(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(sqMi, acresInSqMiles, bigString, 'areaSquareMilesToAcres');
}

/**
 * Takes a number (can be string) of acres (standard), and optional arg to return as a string, and returns number converted to sqaure miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareMiles } from 'stringman-utils';
 * // or
 * const areaAcresToSquareMiles = require('stringman-utils').areaAcresToSquareMiles;
 *
 * const acresSqMi = areaAcresToSquareMiles(640);
 * console.log(acresSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareMiles(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(acres, acresInSqMiles, bigString, 'areaAcresToSquareMiles');
}

/**
 * Takes a number (can be string) of square chains (Gunter/survey), and optional arg to return as a string, and returns number converted to acres (standard)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareChainsGunToAcres } from 'stringman-utils';
 * // or
 * const areaSquareChainsGunToAcres = require('stringman-utils').areaSquareChainsGunToAcres;
 *
 * const chainsAcres = areaSquareChainsGunToAcres(1);
 * console.log(chainsAcres); // 0.09999959036026088
 * ```
 *
 * @export
 * @param {(number | string)} sqChain
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareChainsGunToAcres(
  sqChain: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqChain, sqChainGunInAcre, bigString, 'areaSquareChainsGunToAcres');
}

/**
 * Takes a number (can be string) of acres (standard), and optional arg to return as a string, and returns number converted to square chains (Gunter/survey)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareChainsGun } from 'stringman-utils';
 * // or
 * const areaAcresToSquareChainsGun = require('stringman-utils').areaAcresToSquareChainsGun;
 *
 * const acresChains = areaAcresToSquareChainsGun(0.09999959036026088);
 * console.log(acresChains); // 1
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareChainsGun(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(acres, sqChainGunInAcre, bigString, 'areaAcresToSquareChainsGun');
}

/**
 * Takes a number (can be string) of acres, and optional arg to return as a string, and returns number converted to square feet
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareFeet } from 'stringman-utils';
 * // or
 * const areaAcresToSquareFeet = require('stringman-utils').areaAcresToSquareFeet;
 *
 * const acresSqFt = areaAcresToSquareFeet(1);
 * console.log(acresSqFt); // 43560
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareFeet(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(acres, squareFeetInAcre, bigString, 'areaAcresToSquareFeet');
}

/**
 * Takes a number (can be string) of square feet, and optional arg to return as a string, and returns number converted to acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareFeetToAcres } from 'stringman-utils';
 * // or
 * const areaSquareFeetToAcres = require('stringman-utils').areaSquareFeetToAcres;
 *
 * const sqFtAcres = areaSquareFeetToAcres(43560);
 * console.log(sqFtAcres); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqFt
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareFeetToAcres(sqFt: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(sqFt, squareFeetInAcre, bigString, 'areaSquareFeetToAcres');
}

/**
 * Takes a number (can be string) of acres, and optional arg to return as a string, and returns number converted to square rods
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareRods } from 'stringman-utils';
 * // or
 * const areaAcresToSquareRods = require('stringman-utils').areaAcresToSquareRods;
 *
 * const acresSqRds = areaAcresToSquareRods(1);
 * console.log(acresSqRds); // 159.99935933462
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareRods(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(acres, sqRodsInAcres, bigString, 'areaAcresToSquareRods');
}

/**
 * Takes a number (can be string) of square rods, and optional arg to return as a string, and returns number converted to acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareRodsToAcres } from 'stringman-utils';
 * // or
 * const areaSquareRodsToAcres = require('stringman-utils').areaSquareRodsToAcres;
 *
 * const sqRodsAcres = areaSquareRodsToAcres(159.99935933462);
 * console.log(sqRodsAcres); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqRods
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareRodsToAcres(
  sqRods: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqRods, sqRodsInAcres, bigString, 'areaSquareRodsToAcres');
}

/**
 * Takes a number (can be string) of acres, and optional arg to return as a string, and returns number converted to square links (Gunter/survey)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareLinksGun } from 'stringman-utils';
 * // or
 * const areaAcresToSquareLinksGun = require('stringman-utils').areaAcresToSquareLinksGun;
 *
 * const acresSqLks = areaAcresToSquareLinksGun(1);
 * console.log(acresSqLks); // 99999.600244396
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareLinksGun(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(acres, sqLinksGunInAcre, bigString, 'areaAcresToSquareLinksGun');
}

/**
 * Takes a number (can be string) of square links (Gunter/survey), and optional arg to return as a string, and returns number converted to acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareLinksGunToAcres } from 'stringman-utils';
 * // or
 * const areaSquareLinksGunToAcres = require('stringman-utils').areaSquareLinksGunToAcres;
 *
 * const sqLksAcres = areaSquareLinksGunToAcres(99999.600244396);
 * console.log(sqLksAcres); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqLinks
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareLinksGunToAcres(
  sqLinks: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqLinks, sqLinksGunInAcre, bigString, 'areaSquareLinksGunToAcres');
}

/**
 * Takes a number (can be string) of townships, and optional arg to return as a string, and returns number converted to acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaTownshipsToAcres } from 'stringman-utils';
 * // or
 * const areaTownshipsToAcres = require('stringman-utils').areaTownshipsToAcres;
 *
 * const tsAcres = areaTownshipsToAcres(1);
 * console.log(tsAcres); // 23040.092256185
 * ```
 *
 * @export
 * @param {(number | string)} ts
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaTownshipsToAcres(ts: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(ts, acresInTownship, bigString, 'areaTownshipsToAcres');
}

/**
 * Takes a number (can be string) of acres, and optional arg to return as a string, and returns number converted to townships
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToTownships } from 'stringman-utils';
 * // or
 * const areaAcresToTownships = require('stringman-utils').areaAcresToTownships;
 *
 * const acresTs = areaAcresToTownships(23040.092256185);
 * console.log(acresTs); // 1
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToTownships(acres: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(acres, acresInTownship, bigString, 'areaAcresToTownships');
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToAcres } from 'stringman-utils';
 * // or
 * const areaSectionsToAcres = require('stringman-utils').areaSectionsToAcres;
 *
 * const sectionsAcres = areaSectionsToAcres(1);
 * console.log(sectionsAcres); // 640.00256734189
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToAcres(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(sections, acresInSection, bigString, 'areaSectionsToAcres');
}

/**
 * Takes a number (can be string) of acres, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSections } from 'stringman-utils';
 * // or
 * const areaAcresToSections = require('stringman-utils').areaAcresToSections;
 *
 * const acresSections = areaAcresToSections(640.00256734189);
 * console.log(acresSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSections(acres: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(acres, acresInSection, bigString, 'areaAcresToSections');
}

/**
 * Takes a number (can be string) of acres, and optional arg to return as a string, and returns number converted to square yards
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareYards } from 'stringman-utils';
 * // or
 * const areaAcresToSquareYards = require('stringman-utils').areaAcresToSquareYards;
 *
 * const acresSqYds = areaAcresToSquareYards(1);
 * console.log(acresSqYds); // 4840
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareYards(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(acres, squareYardInAcre, bigString, 'areaAcresToSquareYards');
}

/**
 * Takes a number (can be string) of square yards, and optional arg to return as a string, and returns number converted to acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareYardsToAcres } from 'stringman-utils';
 * // or
 * const areaSquareYardsToAcres = require('stringman-utils').areaSquareYardsToAcres;
 *
 * const sqYdsAcres = areaSquareYardsToAcres(4840);
 * console.log(sqYdsAcres); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqYds
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareYardsToAcres(
  sqYds: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqYds, squareYardInAcre, bigString, 'areaSquareYardsToAcres');
}

/**
 * Takes a number (can be string) of intl acres, and optional arg to return as a string, and returns number converted to square meters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareMeters } from 'stringman-utils';
 * // or
 * const areaAcresToSquareMeters = require('stringman-utils').areaAcresToSquareMeters;
 *
 * const acresSqM = areaAcresToSquareMeters(1);
 * console.log(acresSqM); // 4046.8564224
 * ```
 *
 * @export
 * @param {(number | string)} sqm
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareMeters(
  sqm: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(sqm, squareMetersInAcre, bigString, 'areaAcresToSquareMeters');
}

/**
 * Takes a number (can be string) of square meters, and optional arg to return as a string, and returns number converted to intl acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMetersToAcres } from 'stringman-utils';
 * // or
 * const areaSquareMetersToAcres = require('stringman-utils').areaSquareMetersToAcres;
 *
 * const sqMAcres = areaSquareMetersToAcres(4046.8564224);
 * console.log(sqMAcres); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqm
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMetersToAcres(
  sqm: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqm, squareMetersInAcre, bigString, 'areaSquareMetersToAcres');
}

/**
 * Takes a number (can be string) of square kilometers, and optional arg to return as a string, and returns number converted to intl acres
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareKilometersToAcres } from 'stringman-utils';
 * // or
 * const areaSquareKilometersToAcres = require('stringman-utils').areaSquareKilometersToAcres;
 *
 * const sqKmAcres = areaSquareKilometersToAcres(1);
 * console.log(sqKmAcres); // 247.10538146716533
 * ```
 *
 * @export
 * @param {(number | string)} sqKm
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareKilometersToAcres(
  sqKm: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqKm,
    acresInSquareKilometer,
    bigString,
    'areaSquareKilometersToAcres'
  );
}

/**
 * Takes a number (can be string) of intl acres, and optional arg to return as a string, and returns number converted to square kilometers
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaAcresToSquareKilometers } from 'stringman-utils';
 * // or
 * const areaAcresToSquareKilometers = require('stringman-utils').areaAcresToSquareKilometers;
 *
 * const acresSqKm = areaAcresToSquareKilometers(247.10538146716533);
 * console.log(acresSqKm); // 1
 * ```
 *
 * @export
 * @param {(number | string)} acres
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaAcresToSquareKilometers(
  acres: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    acres,
    acresInSquareKilometer,
    bigString,
    'areaAcresToSquareKilometers'
  );
}
