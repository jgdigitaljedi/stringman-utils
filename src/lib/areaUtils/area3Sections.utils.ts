// areaLandUnit2.utils.ts
/**
 * @module area utils
 */

import {
  hectaresInSection,
  sectionsInTownship,
  squareChainsInSection,
  squareFeetInSection,
  squareKilometersInSection,
  squareLinksInSection,
  squareMetersInSection,
  squareRodsInSection,
  squareYdsInSection
} from '../../util/constants/area.constants';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq chains
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareChains } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareChains = require('stringman-utils').areaSectionsToSquareChains;
 *
 * const sectionsChains = areaSectionsToSquareChains(1);
 * console.log(sectionsChains); // 6400.0000889576
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareChains(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareChainsInSection,
    bigString,
    'areaSectionsToSquareChains'
  );
}

/**
 * Takes a number (can be string) of sq chains, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareChainsToSections } from 'stringman-utils';
 * // or
 * const areaSquareChainsToSections = require('stringman-utils').areaSquareChainsToSections;
 *
 * const chainsSections = areaSquareChainsToSections(6400.0000889576);
 * console.log(chainsSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqChains
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareChainsToSections(
  sqChains: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqChains,
    squareChainsInSection,
    bigString,
    'areaSquareChainsToSections'
  );
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq links
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareLinks } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareLinks = require('stringman-utils').areaSectionsToSquareLinks;
 *
 * const sectionsLinks = areaSectionsToSquareLinks(1);
 * console.log(sectionsLinks); // 64000000.889576
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareLinks(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareLinksInSection,
    bigString,
    'areaSectionsToSquareLinks'
  );
}

/**
 * Takes a number (can be string) of sq links, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareLinksToSections } from 'stringman-utils';
 * // or
 * const areaSquareLinksToSections = require('stringman-utils').areaSquareLinksToSections;
 *
 * const sectionsLinks = areaSquareLinksToSections(64000000.889576);
 * console.log(sectionsLinks); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqLinks
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareLinksToSections(
  sqLinks: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqLinks, squareLinksInSection, bigString, 'areaSquareLinksToSections');
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq yards
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareYards } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareYards = require('stringman-utils').areaSectionsToSquareYards;
 *
 * const sectionsYds = areaSectionsToSquareYards(1);
 * console.log(sectionsYds); // 3097612.4259347
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareYards(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareYdsInSection,
    bigString,
    'areaSectionsToSquareYards'
  );
}

/**
 * Takes a number (can be string) of sq yards, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareYardsToSections } from 'stringman-utils';
 * // or
 * const areaSquareYardsToSections = require('stringman-utils').areaSquareYardsToSections;
 *
 * const ydsSections = areaSquareYardsToSections(3097612.4259347);
 * console.log(ydsSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqYds
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareYardsToSections(
  sqYds: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqYds, squareYdsInSection, bigString, 'areaSquareYardsToSections');
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq meters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareMeters } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareMeters = require('stringman-utils').areaSectionsToSquareMeters;
 *
 * const sectionsMeters = areaSectionsToSquareMeters(1);
 * console.log(sectionsMeters); // 2589998.5
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareMeters(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareMetersInSection,
    bigString,
    'areaSectionsToSquareMeters'
  );
}

/**
 * Takes a number (can be string) of sq meters, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMetersToSections } from 'stringman-utils';
 * // or
 * const areaSquareMetersToSections = require('stringman-utils').areaSquareMetersToSections;
 *
 * const metersSections = areaSquareMetersToSections(2589998.5);
 * console.log(metersSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqMeters
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMetersToSections(
  sqMeters: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqMeters,
    squareMetersInSection,
    bigString,
    'areaSquareMetersToSections'
  );
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq km
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareKilometers } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareKilometers = require('stringman-utils').areaSectionsToSquareKilometers;
 *
 * const sectionsSqKm = areaSectionsToSquareKilometers(1);
 * console.log(sectionsSqKm); // 2.5899985
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareKilometers(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareKilometersInSection,
    bigString,
    'areaSectionsToSquareKilometers'
  );
}

/**
 * Takes a number (can be string) of sq km, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareKilometersToSections } from 'stringman-utils';
 * // or
 * const areaSquareKilometersToSections = require('stringman-utils').areaSquareKilometersToSections;
 *
 * const sqKmSections = areaSquareKilometersToSections(2.5899985);
 * console.log(sqKmSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqKm
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareKilometersToSections(
  sqKm: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqKm,
    squareKilometersInSection,
    bigString,
    'areaSquareKilometersToSections'
  );
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq ft
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareFeet } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareFeet = require('stringman-utils').areaSectionsToSquareFeet;
 *
 * const sectionsSqFt = areaSectionsToSquareFeet(1);
 * console.log(sectionsSqFt); // 27878511.833413
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareFeet(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareFeetInSection,
    bigString,
    'areaSectionsToSquareFeet'
  );
}

/**
 * Takes a number (can be string) of sq ft, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareFeetToSections } from 'stringman-utils';
 * // or
 * const areaSquareFeetToSections = require('stringman-utils').areaSquareFeetToSections;
 *
 * const sqFtSections = areaSquareFeetToSections(27878511.833413);
 * console.log(sqFtSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqFt
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareFeetToSections(
  sqFt: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqFt, squareFeetInSection, bigString, 'areaSquareFeetToSections');
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to hectares
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToHectares } from 'stringman-utils';
 * // or
 * const areaSectionsToHectares = require('stringman-utils').areaSectionsToHectares;
 *
 * const sectionHect = areaSectionsToHectares(1);
 * console.log(sectionHect); // 258.99985
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToHectares(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(sections, hectaresInSection, bigString, 'areaSectionsToHectares');
}

/**
 * Takes a number (can be string) of hectares, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaHectaresToSections } from 'stringman-utils';
 * // or
 * const areaHectaresToSections = require('stringman-utils').areaHectaresToSections;
 *
 * const hectSections = areaHectaresToSections(258.99985);
 * console.log(hectSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} ha
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaHectaresToSections(ha: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(ha, hectaresInSection, bigString, 'areaHectaresToSections');
}

/**
 * Takes a number (can be string) of townships, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaTownshipsToSections } from 'stringman-utils';
 * // or
 * const areaTownshipsToSections = require('stringman-utils').areaTownshipsToSections;
 *
 * const tsSections = areaTownshipsToSections(1);
 * console.log(tsSections); // 35.999999737308
 * ```
 *
 * @export
 * @param {(number | string)} ts
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaTownshipsToSections(ts: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(ts, sectionsInTownship, bigString, 'areaTownshipsToSections');
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to townships
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToTownships } from 'stringman-utils';
 * // or
 * const areaSectionsToTownships = require('stringman-utils').areaSectionsToTownships;
 *
 * const sectionsTs = areaSectionsToTownships(35.999999737308);
 * console.log(sectionsTs); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToTownships(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sections, sectionsInTownship, bigString, 'areaSectionsToTownships');
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to sq rods
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareRods } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareRods = require('stringman-utils').areaSectionsToSquareRods;
 *
 * const sectionsSqRods = areaSectionsToSquareRods(1);
 * console.log(sectionsSqRods); // 102400.00074721
 * ```
 *
 * @export
 * @param {(number | string)} sections
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareRods(
  sections: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sections,
    squareRodsInSection,
    bigString,
    'areaSectionsToSquareRods'
  );
}

/**
 * Takes a number (can be string) of sq rods, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareRodsToSections } from 'stringman-utils';
 * // or
 * const areaSquareRodsToSections = require('stringman-utils').areaSquareRodsToSections;
 *
 * const sqRodsSections = areaSquareRodsToSections(102400.00074721);
 * console.log(sqRodsSections); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqRods
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareRodsToSections(
  sqRods: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqRods, squareRodsInSection, bigString, 'areaSquareRodsToSections');
}
