// areaLandUnit.utils.ts
/**
 * * This is a collection of helper functions that can be used to convert area
 * units into other area units.
 * example: converting square miles to acres
 *
 *
 * General naming convention:
 * - camel cased
 * - `area` prefix
 * - `area<current operation>`
 * - `areaAcresToHectares`
 *
 * @module area utils
 **/

import {
  squareYdsInSquareMiles,
  hectaresInSqMile,
  squareFeetInSquareMile,
  squareMilesInTownship,
  squareMetersInSquareMile,
  squareRodsInSquareMile,
  squareLinksGunInSquareMile,
  squareChainsInSquareMile,
  squareMilesInSection,
  squareKilometersInSquareMiles
} from '../../util/constants/areaSqMiles.constants';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square yards
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareYards } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareYards = require('stringman-utils').areaSquareMilesToSquareYards;
 *
 * const sqMiSqYd = areaSquareMilesToSquareYards(1);
 * console.log(sqMiSqYd); // 3097600
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareYards(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareYdsInSquareMiles,
    bigString,
    'areaSquareMilesToSquareYards'
  );
}

/**
 * Takes a number (can be string) of square yards, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareYardsToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareYardsToSquareMiles = require('stringman-utils').areaSquareYardsToSquareMiles;
 *
 * const sqYdSqMi = areaSquareYardsToSquareMiles(3097600);
 * console.log(sqYdSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqYd
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareYardsToSquareMiles(
  sqYd: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqYd,
    squareYdsInSquareMiles,
    bigString,
    'areaSquareYardsToSquareMiles'
  );
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to hectares
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToHectares } from 'stringman-utils';
 * // or
 * const areaSquareMilesToHectares = require('stringman-utils').areaSquareMilesToHectares;
 *
 * const sqMiHect = areaSquareMilesToHectares(1);
 * console.log(sqMiHect); // 258.9988110336
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToHectares(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(sqMi, hectaresInSqMile, bigString, 'areaSquareMilesToHectares');
}

/**
 * Takes a number (can be string) of hectares, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaHectaresToSquareMiles } from 'stringman-utils';
 * // or
 * const areaHectaresToSquareMiles = require('stringman-utils').areaHectaresToSquareMiles;
 *
 * const hectSqMi = areaHectaresToSquareMiles(258.9988110336);
 * console.log(hectSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} hectares
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaHectaresToSquareMiles(
  hectares: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(hectares, hectaresInSqMile, bigString, 'areaHectaresToSquareMiles');
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square feet
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareFeet } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareFeet = require('stringman-utils').areaSquareMilesToSquareFeet;
 *
 * const sqMiSqFt = areaSquareMilesToSquareFeet(1);
 * console.log(sqMiSqFt); // 27878400
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareFeet(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareFeetInSquareMile,
    bigString,
    'areaSquareMilesToSquareFeet'
  );
}

/**
 * Takes a number (can be string) of square feet, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareFeetToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareFeetToSquareMiles = require('stringman-utils').areaSquareFeetToSquareMiles;
 *
 * const sqFtSqMi = areaSquareFeetToSquareMiles(27878400);
 * console.log(sqFtSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqFt
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareFeetToSquareMiles(
  sqFt: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqFt,
    squareFeetInSquareMile,
    bigString,
    'areaSquareFeetToSquareMiles'
  );
}

/**
 * Takes a number (can be string) of townships, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaTownshipsToSquareMiles } from 'stringman-utils';
 * // or
 * const areaTownshipsToSquareMiles = require('stringman-utils').areaTownshipsToSquareMiles;
 *
 * const tsSqMi = areaTownshipsToSquareMiles(1);
 * console.log(tsSqMi); // 36
 * ```
 *
 * @export
 * @param {(number | string)} ts
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaTownshipsToSquareMiles(
  ts: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(ts, squareMilesInTownship, bigString, 'areaTownshipsToSquareMiles');
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to townships
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToTownships } from 'stringman-utils';
 * // or
 * const areaSquareMilesToTownships = require('stringman-utils').areaSquareMilesToTownships;
 *
 * const sqMiTs = areaSquareMilesToTownships(36);
 * console.log(sqMiTs); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToTownships(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqMi, squareMilesInTownship, bigString, 'areaSquareMilesToTownships');
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square meters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareMeters } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareMeters = require('stringman-utils').areaSquareMilesToSquareMeters;
 *
 * const sqMiSqM = areaSquareMilesToSquareMeters(1);
 * console.log(sqMiSqM); // 2589988.110336
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareMeters(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareMetersInSquareMile,
    bigString,
    'areaSquareMilesToSquareMeters'
  );
}

/**
 * Takes a number (can be string) of square meters, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMetersToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareMetersToSquareMiles = require('stringman-utils').areaSquareMetersToSquareMiles;
 *
 * const SqMSqMi = areaSquareMetersToSquareMiles(2589988.110336);
 * console.log(SqMSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqm
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMetersToSquareMiles(
  sqm: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqm,
    squareMetersInSquareMile,
    bigString,
    'areaSquareMetersToSquareMiles'
  );
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square rods
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareRods } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareRods = require('stringman-utils').areaSquareMilesToSquareRods;
 *
 * const sqMiSqRod = areaSquareMilesToSquareRods(1);
 * console.log(sqMiSqRod); // 102400
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareRods(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareRodsInSquareMile,
    bigString,
    'areaSquareMilesToSquareRods'
  );
}

/**
 * Takes a number (can be string) of square rods, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareRodsToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareRodsToSquareMiles = require('stringman-utils').areaSquareRodsToSquareMiles;
 *
 * const sqRodsSqMi = areaSquareRodsToSquareMiles(102400);
 * console.log(sqRodsSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqRods
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareRodsToSquareMiles(
  sqRods: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqRods,
    squareRodsInSquareMile,
    bigString,
    'areaSquareRodsToSquareMiles'
  );
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square links
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareLinks } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareLinks = require('stringman-utils').areaSquareMilesToSquareLinks;
 *
 * const sqMiSqLink = areaSquareMilesToSquareLinks(1);
 * console.log(sqMiSqLink); // 64000000
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareLinksGun(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareLinksGunInSquareMile,
    bigString,
    'areaSquareMilesToSquareLinksGun'
  );
}

/**
 * Takes a number (can be string) of square links, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareLinksGunToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareLinksGunToSquareMiles = require('stringman-utils').areaSquareLinksGunToSquareMiles;
 *
 * const sqLinkSqMi = areaSquareLinksGunToSquareMiles(64000000);
 * console.log(sqLinkSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqLinks
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareLinksGunToSquareMiles(
  sqLinks: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqLinks,
    squareLinksGunInSquareMile,
    bigString,
    'areaSquareLinksGunToSquareMiles'
  );
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square chains
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareChains } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareChains = require('stringman-utils').areaSquareMilesToSquareChains;
 *
 * const sqMiSqChain = areaSquareMilesToSquareChains(1);
 * console.log(sqMiSqChain); // 6400
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareChains(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareChainsInSquareMile,
    bigString,
    'areaSquareMilesToSquareChains'
  );
}

/**
 * Takes a number (can be string) of square chains, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareChainsToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareChainsToSquareMiles = require('stringman-utils').areaSquareChainsToSquareMiles;
 *
 * const sqChainSqMi = areaSquareChainsToSquareMiles(6400);
 * console.log(sqChainSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqChains
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareChainsToSquareMiles(
  sqChains: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqChains,
    squareChainsInSquareMile,
    bigString,
    'areaSquareChainsToSquareMiles'
  );
}

/**
 * Takes a number (can be string) of sections, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSectionsToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSectionsToSquareMiles = require('stringman-utils').areaSectionsToSquareMiles;
 *
 * const sectionSqMi = areaSectionsToSquareMiles(1);
 * console.log(sectionSqMi); // 1.0000000001297
 * ```
 *
 * @export
 * @param {(number | string)} sec
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSectionsToSquareMiles(
  sec: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(sec, squareMilesInSection, bigString, 'areaSectionsToSquareMiles');
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to sections
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSections } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSections = require('stringman-utils').areaSquareMilesToSections;
 *
 * const sqMiSection = areaSquareMilesToSections(1.0000000001297);
 * console.log(sqMiSection); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSections(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(sqMi, squareMilesInSection, bigString, 'areaSquareMilesToSections');
}

/**
 * Takes a number (can be string) of square miles, and optional arg to return as a string, and returns number converted to square kilometers
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareMilesToSquareKilometers } from 'stringman-utils';
 * // or
 * const areaSquareMilesToSquareKilometers = require('stringman-utils').areaSquareMilesToSquareKilometers;
 *
 * const sqMiSqKm = areaSquareMilesToSquareKilometers(1);
 * console.log(sqMiSqKm); // 2.5899881103360003
 * ```
 *
 * @export
 * @param {(number | string)} sqMi
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareMilesToSquareKilometers(
  sqMi: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    sqMi,
    squareKilometersInSquareMiles,
    bigString,
    'areaSquareMilesToSquareKilometers'
  );
}

/**
 * Takes a number (can be string) of square kilometers, and optional arg to return as a string, and returns number converted to square miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { areaSquareKilometersToSquareMiles } from 'stringman-utils';
 * // or
 * const areaSquareKilometersToSquareMiles = require('stringman-utils').areaSquareKilometersToSquareMiles;
 *
 * const sqKmSqMi = areaSquareKilometersToSquareMiles(2.5899881103360003);
 * console.log(sqKmSqMi); // 1
 * ```
 *
 * @export
 * @param {(number | string)} sqKm
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function areaSquareKilometersToSquareMiles(
  sqKm: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    sqKm,
    squareKilometersInSquareMiles,
    bigString,
    'areaSquareKilometersToSquareMiles'
  );
}
