// ip.utils.ts
/**
 * * This is a collection of helper functions that can be used for manipulating strings
 * in different ways regarding ip addresses, verifying valid addresses, and more.
 *
 * General naming convention:
 * - camel cased
 * - `ip` prefix
 * - `ip<desired operation>`
 * - `ipIsValid`
 *
 * @module ip address utils
 **/

import { ipAddrContains, ipAddrOnlyRegex } from '../../util/constants/regex.constants';
import { stringIsValid, stringRemove, stringRetrieve, stringSwap } from '../stringUtils';

/**
 * Tests if argument is valid IP address and return boolean
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { ipIsValid } from 'stringman-utils';
 * // or
 * const ipIsValid = require('stringman-utils').ipIsValid;
 * const valid = ipIsValid('192.168.0.1');
 * const invalid = ipIsValid('192.168.0.256');
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} addr
 * @returns {boolean}
 */
export function ipIsValid(addr: string): boolean {
  return stringIsValid(addr, ipAddrOnlyRegex);
}

/**
 * Gets any and all valid IP addresses from a string and returns in array
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { ipRetrieve } from 'stringman-utils';
 * // or
 * const ipRetrieve = require('stringman-utils').ipRetrieve;
 * const ips = ipRetrieve('my router is at 192.168.0.1');
 * console.log(ips); // ['192.168.0.1'];
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string[]}
 */
export function ipRetrieve(str: string): string[] {
  return stringRetrieve(str, ipAddrContains);
}

/**
 * Takes a string and returns the string with all valid IP addresses removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { ipRemove } from 'stringman-utils';
 * // or
 * const ipRemove = require('stringman-utils').ipRemove;
 * const removed = ipRemove('my router is at 192.168.0.1');
 * console.log(removed); // 'my router is at'
 * ```
 *
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function ipRemove(str: string): string {
  return stringRemove(str, ipAddrContains);
}

/**
 * Takes a string with an IP and another string and returns the first string with all IP addresses swapped with the second string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { ipSwap } from 'stringman-utils';
 * // or
 * const ipSwap = require('stringman-utils').ipSwap;
 * const swapped = ipSwap('my router is at 192.168.0.1', '***.***.***.***');
 * console.log(swapped); // 'my router is at ***.***.***.***'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} newStr
 * @returns {string}
 */
export function ipSwap(str: string, newStr: string): string {
  return stringSwap(str, newStr, ipAddrContains);
}
