import {
  numberContainsDecimal,
  numberContainsFraction,
  numberContainsNum,
  numberConvertToHex,
  numberIsDecimal,
  numberIsFraction,
  numberIsPhoneNumber,
  numberIsWhole,
  numberRemoveLeadingZeros,
  numberRemoveTrailingZeros,
  numberTruncate
} from './numbers.utils';

export {
  numberContainsDecimal,
  numberContainsFraction,
  numberContainsNum,
  numberConvertToHex,
  numberIsDecimal,
  numberIsFraction,
  numberIsPhoneNumber,
  numberIsWhole,
  numberRemoveLeadingZeros,
  numberRemoveTrailingZeros,
  numberTruncate
};
