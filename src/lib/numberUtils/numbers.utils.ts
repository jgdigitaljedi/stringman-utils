// numbers.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various
 * operations regarding numbers on the values passed.
 * example: truncating a number to a certain decimal place without rounding like toFixed()
 *
 *
 * General naming convention:
 * - camel cased
 * - `number` prefix
 * - `number<current operation>`
 * - `numberTruncate`
 *
 * @module numbers utils
 **/

import { stringIsValid } from '../stringUtils';

/**
 * Takes string and returns boolean for whether string contains at least 1 number
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberContainsNum } from 'stringman-utils';
 * // or
 * const numberContainsNum = require('stringman-utils').numberContainsNum;
 * const yes = numberContainsNum('this has a number 33');
 * const no = numberContainsNum('this has a number thirty three');
 * console.log(yes); // true
 * console.log(no); // false
 * ```
 *
 * @export
 * @param {string} str
 * @returns {boolean}
 */
export function numberContainsNum(num: string | number): boolean {
  if (typeof num === 'number') {
    return true;
  }
  if (typeof num === 'string') {
    return stringIsValid(num, /-?\d/g);
  }
  return false;
}

/**
 * Takes a string or a number and returns boolean for whether it is a valid whole number
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberIsWhole } from 'stringman-utils';
 * // or
 * const numberIsWhole = require('stringman-utils').numberIsWhole;
 * const isWhole = numberIsWhole(123456);
 * const notWhole = numberIsWhole(123456.789);
 * console.log(isWhole); // true
 * console.log(notWhole); // false
 * ```
 *
 * @export
 * @param {(string | number)} num
 * @returns {boolean}
 */
export function numberIsWhole(num: string | number): boolean {
  if (typeof num !== 'string' && typeof num !== 'number') {
    return false;
  }
  return stringIsValid(num.toString(), /^-?\d+$/g);
  // return (typeof num === 'number' ? num : parseFloat(num)) % 1 === 0;
}

/**
 * Takes a string or a number and returns a boolean for whether it is a valid decimal
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberIsDecimal } from 'stringman-utils';
 * // or
 * const numberIsDecimal = require('stringman-utils').numberIsDecimal;
 * const isDec = numberIsDecimal(123456.789);
 * const notDec = numberIsDecimal(123456);
 * console.log(isDec); // true
 * console.log(notDec); // false
 * ```
 *
 * @export
 * @param {(string | number)} num
 * @returns {boolean}
 */
export function numberIsDecimal(num: string | number): boolean {
  if (typeof num !== 'string' && typeof num !== 'number') {
    return false;
  }
  return stringIsValid(num.toString(), /^-?\d*\.\d+$/g);
  // return (typeof num === 'number' ? num : parseFloat(num)) % 1 > 0;
}

/**
 * Takes a string and returns boolean indicating whether string contains a decimal somwhere in it
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberContainsDecimal } from 'stringman-utils';
 * // or
 * const numberContainsDecimal = require('stringman-utils').numberContainsDecimal;
 * const hasDec = numberContainsDecimal('has a decimal 123456.789');
 * const noDec = numberContainsDecimal('no decimal 123456');
 * console.log(hasDec); // true
 * console.log(noDec); // false
 * ```
 *
 * @export
 * @param {string | number} num
 * @returns {boolean}
 */
export function numberContainsDecimal(num: string | number): boolean {
  if (typeof num === 'string') {
    return stringIsValid(num.toString(), /-?\d*\.\d+/g);
  }
  if (typeof num === 'number') {
    return numberIsDecimal(num);
  }
  return false;
}

/**
 * Takes a string and returns a boolean for whether it is a valid fraction.
 * ```js
 * import { numberIsFraction } from 'stringman-utils';
 * // or
 * const numberIsFraction = require('stringman-utils').numberIsFraction;
 * const valid = numberIsFraction('1/3');
 * const invalid = numberIsFraction('3.33');
 * console.log(isDec); // true
 * console.log(notDec); // false
 * ```
 *
 * @export
 * @param {string} num
 * @returns {boolean}
 */
export function numberIsFraction(num: string): boolean {
  if (typeof num !== 'string') {
    return false;
  }
  return stringIsValid(num, /^-?\d*\/\d+$/g);
}

/**
 * Takes a string and returns a boolean indicating whether the string contains a fraction
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberContainsFraction } from 'stringman-utils';
 * // or
 * const numberContainsFraction = require('stringman-utils').numberContainsFraction;
 * const hasFraction = numberContainsFraction('this string has a fraction 1/2');
 * const noFraction = numberContainsFraction('this string does not have a fraction');
 * console.log(hasFraction) // true
 * console.log(noFraction) // false
 * ```
 *
 * @export
 * @param {string} num
 * @returns {boolean}
 */
export function numberContainsFraction(num: string): boolean {
  if (typeof num !== 'string') {
    return false;
  }
  return stringIsValid(num, /-?\d*\/\d+/g);
}

/**
 * Takes a number as a string or number form and returns the hexidecimal equivalent
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberConvertToHex } from 'stringman-utils';
 * // or
 * const numberConvertToHex = require('stringman-utils').numberConvertToHex;
 * const hex = numberConvertToHex('255');
 * const moreHex = numberConvertToHex(0);
 * const notHex = numberConvertToHex('nope');
 * console.log(hex) // 'ff'
 * console.log(moreHex) // '00'
 * console.log(notHex) // 'NAN'
 * ```
 *
 * @export
 * @param {(string | number)} num
 * @returns {(string | null)}
 */
export function numberConvertToHex(num: string | number): string | null {
  if (typeof num !== 'string' && typeof num !== 'number') {
    return null;
  }
  const value = typeof num === 'string' ? parseFloat(num) : num;
  const hex = value.toString(16);
  return hex.length === 1 ? '0' + hex.toUpperCase() : hex.toUpperCase();
}

/**
 * Takes a string or number and validates whether it is a valid phone number format.
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberIsPhoneNumber } from 'stringman-utils';
 * // or
 * const numberIsPhoneNumber = require('stringman-utils').numberIsPhoneNumber;
 * const valid = numberIsPhoneNumber('(888) 555-1234');
 * const invalid = numberIsPhoneNumber('888-55-2345');
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * Valid formats:
 * ```
 * (123) 456-7890
 * (123)456-7890
 * 123-456-7890
 * 123.456.7890
 * 1234567890
 * +31636363634
 * 075-63546725
 * ```
 *
 * @export
 * @param {(string | number)} str
 * @returns {boolean}
 */
export function numberIsPhoneNumber(str: string | number): boolean {
  if (typeof str !== 'string' && typeof str !== 'number') {
    return false;
  }
  const test = typeof str === 'number' ? str.toString() : str;
  return stringIsValid(test, /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
}

/**
 * Takes a number as string or number, 2nd arg as amount of digits on right of decimal to truncate,
 * optional 3rd arg for string return, and returns number truncated to that amount of decimal places
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberTruncate } from 'stringman-utils';
 * // or
 * const numberTruncate = require('stringman-utils').numberTruncate;
 * const example1 = numberTruncate(4500.459, 2);
 * const example2 = numberTruncate('450.4', 2, true);
 * console.log(example1); // 4500.45
 * console.log(example2); // '450.40'
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {number} digits
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string | null)}
 */
export const numberTruncate = (
  num: number | string,
  digits: number,
  strReturn?: boolean
): number | string => {
  if ((typeof num !== 'string' && typeof num !== 'number') || typeof digits !== 'number') {
    return '';
  }
  const truncated =
    Math.trunc((typeof num === 'string' ? parseFloat(num) : num) * Math.pow(10, digits)) /
    Math.pow(10, digits);
  if (strReturn) {
    let tSplit = truncated.toString().split('.');
    if (tSplit.length > 1 && tSplit[1].length < digits) {
      let dec = tSplit[1].toString();
      while (dec.length < digits) dec += '0';
      return `${tSplit[0]}.${dec}`;
    }
    return truncated.toString();
  }
  return truncated;
};

/**
 * Takes a string and optional arg for a number return and returns string with leading zeros removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberRemoveLeadingZeros } from 'stringman-utils';
 * // or
 * const numberRemoveLeadingZeros = require('stringman-utils').numberRemoveLeadingZeros;
 * const example1 = numberRemoveLeadingZeros('00450.4500');
 * const example2 = numberRemoveLeadingZeros('00450.4500'. true);
 * console.log(example1); // '450.4500'
 * console.log(example2); // 450.45
 * ```
 *
 * @export
 * @param {string} num
 * @param {boolean} [numReturn]
 * @return {*}  {(number | string)}
 */
export const numberRemoveLeadingZeros = (
  num: string,
  numReturn?: boolean
): number | string | null => {
  if (typeof num !== 'string') {
    return null;
  }
  return numReturn ? parseFloat(num) : num.replace(/^0+/, '');
};

/**
 * Takes number as string, optional arg for number return, optional arg to remove zeros just left of decimal, and returns
 * the result with the appropriate zeros removed from the right side of the number. Note this will always remove
 * leading zeros as well.
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { numberRemoveTrailingZeros } from 'stringman-utils';
 * // or
 * const numberRemoveTrailingZeros = require('stringman-utils').numberRemoveTrailingZeros;
 * const example1 = numberRemoveTrailingZeros('4500.4500');
 * const example2 = numberRemoveTrailingZeros('450.4500'. true);
 * const example3 = numberRemoveTrailingZeros('00450.4500'. false, true);
 * const example4 = numberRemoveTrailingZeros('00450.4500'. true, true);
 * console.log(example1); // '4500.45'
 * console.log(example2); // 4500.45
 * console.log(example3); // '45'
 * console.log(example4); // 45
 * ```
 *
 * @export
 * @param {string} num
 * @param {boolean} [numReturn]
 * @param {boolean} [leftOfDecimal]
 * @return {*}  {(string | number | null)}
 */
export const numberRemoveTrailingZeros = (
  num: string,
  numReturn?: boolean,
  leftOfDecimal?: boolean
): string | number | null => {
  if (typeof num !== 'string') {
    return null;
  }
  if (leftOfDecimal && numberContainsDecimal(num)) {
    let n = parseInt(num.split('.')[0], 10);
    while (n % 10 === 0) n /= 10;
    return numReturn ? n : n.toString();
  } else if (leftOfDecimal && !numberContainsDecimal(num)) {
    let n = parseInt(num, 10);
    while (n % 10 === 0) n /= 10;
    return numReturn ? n : n.toString();
  }
  return numReturn ? parseFloat(num) : parseFloat(num).toString();
};
