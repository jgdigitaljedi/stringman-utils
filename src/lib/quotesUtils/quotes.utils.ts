// quotes.utils.ts
/**
 * * This is a collection of helper functions that can be used for manipulating strings
 * in different ways regarding quotes.
 *
 * General naming convention:
 * - camel cased
 * - `quotes` prefix
 * - `quotes<desired operation>`
 * - `quotesDoubleToSingle`
 *
 * @module quotes utils
 **/

import { doubleQuotes, strInsideQuotes } from '../../util/constants/regex.constants';
import { stringRemove, stringSwap } from '../stringUtils';

/**
 * Takes a string and returns the string with all double quotes replaced with single quotes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesDoubleToSingle } from 'stringman-utils';
 * // or
 * const quotesDoubleToSingle = require('stringman-utils').quotesDoubleToSingle;
 *
 * const singleQuotes = quotesDoubleToSingle('this will come back "and this will be in single quotes"');
 * console.log(singleQuotes); // "this will come back 'and this will be in single quotes'"
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function quotesDoubleToSingle(str: string): string {
  if (typeof str !== 'string') {
    throw new Error('quotesDoubleToSingle: ARGUMENT MUST BE A STRING.');
  }
  return str.replace(/"/g, "'");
}

/**
 * Takes a string and returns the string with all double quotes replaced with single quotes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesSingleToDouble } from 'stringman-utils';
 * // or
 * const quotesSingleToDouble = require('stringman-utils').quotesSingleToDouble;
 *
 * const doubleQuotes = quotesSingleToDouble("this will come back 'and this will be in double quotes'");
 * console.log(doubleQuotes); // 'this will come back "and this will be in double quotes"'
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function quotesSingleToDouble(str: string): string {
  if (typeof str !== 'string') {
    throw new Error('quotesSingleToDouble: ARGUMENT MUST BE A STRING.');
  }
  return str.replace(/'/g, '"');
}

/**
 * Takes string and returns string with single quotes and contain between single quotes removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesRemoveSingle } from 'stringman-utils';
 * // or
 * const quotesRemoveSingle = require('stringman-utils').quotesRemoveSingle;
 *
 * const noQuotes = quotesRemoveSingle("this will come back 'and this will be removed'");
 * console.log(noQuotes); // 'this will come back'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function quotesRemoveSingle(str: string): string {
  if (typeof str !== 'string') {
    throw new Error('quotesRemoveSingle: ARGUMENT MUST BE A STRING.');
  }
  return stringRemove(str, strInsideQuotes).trim();
}

/**
 * Takes string and returns string with double quotes and contain between double quotes removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesRemoveDouble } from 'stringman-utils';
 * // or
 * const quotesRemoveDouble = require('stringman-utils').quotesRemoveDouble;
 *
 * const noQuotes = quotesRemoveDouble(this will come back "and this will be removed"');
 * console.log(noQuotes); // 'this will come back'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function quotesRemoveDouble(str: string): string {
  if (typeof str !== 'string') {
    throw new Error('quotesRemoveDouble: ARGUMENT MUST BE A STRING.');
  }
  return stringRemove(str, doubleQuotes).trim();
}

/**
 * Takes string and returns array of strings containing what was inside any number of sets of single quotes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesRetrieveSingle } from 'stringman-utils';
 * // or
 * const quotesRetrieveSingle = require('stringman-utils').quotesRetrieveSingle;
 *
 * const fromInside = quotesRetrieveSingle("this is ignored 'and this is returned'"");
 * console.log(fromInside); // 'and this is returned'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function quotesRetrieveSingle(str: string): string[] {
  if (typeof str !== 'string') {
    throw new Error('quotesRetrieveSingle: ARGUMENT MUST BE A STRING.');
  }
  const matched = str.match(strInsideQuotes);
  return matched ? matched.map((m) => m.replace("'", '').replace("'", '')) : [];
}

/**
 * Takes string and returns array of strings containing what was inside any number of sets of double quotes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesRetrieveDouble } from 'stringman-utils';
 * // or
 * const quotesRetrieveDouble = require('stringman-utils').quotesRetrieveDouble;
 *
 * const fromInside = quotesRetrieveDouble('this is ignored "and this is returned"');
 * console.log(fromInside); // 'and this is returned'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function quotesRetrieveDouble(str: string): string[] {
  if (typeof str !== 'string') {
    throw new Error('quotesRetrieveDouble: ARGUMENT MUST BE A STRING.');
  }
  const matched = str.match(doubleQuotes);
  return matched ? matched.map((m) => m.replace('"', '').replace('"', '')) : [];
}

/**
 * Takes 2 strings, swaps anything in single quotes INCLUDING THE single quotes from the first string with the second string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesSwapSingle } from 'stringman-utils';
 * // or
 * const quotesSwapSingle = require('stringman-utils').quotesSwapSingle;
 *
 * const noquotes = quotesSwapSingle("this has 'stuff' in quotes", 'things');
 * const withquotes = quotesSwapSingle("this has 'other stuff' in quotes too', "'more things'");
 * console.log(noquotes); // 'this has things in quotes'
 * console.log(withquotes); // "this has 'more things' in quotes too"
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} other
 * @returns {string}
 */
export function quotesSwapSingle(str: string, other: string): string {
  if (typeof str !== 'string') {
    throw new Error('quotesSwapSingle: ARGUMENT MUST BE A STRING.');
  }
  return stringSwap(str, other, strInsideQuotes);
}

/**
 * Takes 2 strings, swaps anything in double quotes INCLUDING THE DOUBLE QUOTES from the first string with the second string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { quotesSwapDouble } from 'stringman-utils';
 * // or
 * const quotesSwapDouble = require('stringman-utils').quotesSwapDouble;
 *
 * const noquotes = quotesSwapDouble('this has "stuff" in quotes', 'things');
 * const withquotes = quotesSwapDouble('this has "other stuff" in quotes too', '"more things"');
 * console.log(noquotes); // 'this has things in quotes'
 * console.log(withquotes); // 'this has "more things" in quotes too'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} other
 * @returns {string}
 */
export function quotesSwapDouble(str: string, other: string): string {
  if (typeof str !== 'string') {
    throw new Error('quotesSwapDouble: ARGUMENT MUST BE A STRING.');
  }
  return stringSwap(str, other, doubleQuotes);
}
