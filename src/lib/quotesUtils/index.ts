import {
  quotesRemoveSingle,
  quotesSwapSingle,
  quotesRetrieveSingle,
  quotesRetrieveDouble,
  quotesDoubleToSingle,
  quotesSingleToDouble,
  quotesRemoveDouble,
  quotesSwapDouble
} from './quotes.utils';

export {
  quotesRemoveSingle,
  quotesSwapSingle,
  quotesRetrieveSingle,
  quotesDoubleToSingle,
  quotesSingleToDouble,
  quotesRemoveDouble,
  quotesRetrieveDouble,
  quotesSwapDouble
};
