// clockRate.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting clock rates value to
 * different units. For example, converting MHz to GHz.
 *
 * General naming convention:
 * - camel cased
 * - `clockRate` prefix
 * - `clockRate<current value unit>To<desired value unit>`
 * - `clockRateMhzToGhz`
 *
 * @module clock rate utils
 **/

import { metricStep } from '../../util/constants/common.constants';
import {
  precisionMathDivide,
  precisionMathMultiply,
  precisionMathPower
} from '../precisionMathUtils';

/**
 * Takes a value for MHz as number or string, optional arg for string return, and returns conversion to GHz
 *
 * Basic usage example:
 * ```js
 * import { clockRateMhzToGhz } from  'strinigman-utils';
 * //or
 * const clockRateMhzToGhz = require('stringman-utils').clockRateMhzToGhz;
 *
 * console.log(clockRateMhzToGhz(1000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} mhz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateMhzToGhz(mhz: number | string, stringReturn?: boolean): number | string {
  return precisionMathDivide(mhz, metricStep, stringReturn, 'clockRateMhzToGhz');
}

/**
 * Takes a value for GHz as number or string, optional arg for string return, and returns conversion to MHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateGhzToMhz } from  'strinigman-utils';
 * //or
 * const clockRateGhzToMhz = require('stringman-utils').clockRateGhzToMhz;
 *
 * console.log(clockRateGhzToMhz(1)); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} ghz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateGhzToMhz(ghz: number | string, stringReturn?: boolean): number | string {
  return precisionMathMultiply(ghz, metricStep, stringReturn, 'clockRateGhzToMhz');
}

/**
 * Takes a value for KHz as number or string, optional arg for string return, and returns conversion to MHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateKhzToMhz } from  'strinigman-utils';
 * //or
 * const clockRateKhzToMhz = require('stringman-utils').clockRateKhzToMhz;
 *
 * console.log(clockRateKhzToMhz(1000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} khz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateKhzToMhz(khz: number | string, stringReturn?: boolean): number | string {
  return precisionMathDivide(khz, metricStep, stringReturn, 'clockRateKhzToMhz');
}

/**
 * Takes a value for MHz as number or string, optional arg for string return, and returns conversion to KHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateMhzToKhz } from  'strinigman-utils';
 * //or
 * const clockRateMhzToKhz = require('stringman-utils').clockRateMhzToKhz;
 *
 * console.log(clockRateMhzToKhz(1)); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} mhz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateMhzToKhz(mhz: number | string, stringReturn?: boolean): number | string {
  return precisionMathMultiply(mhz, metricStep, stringReturn, 'clockRateMhzToKhz');
}

/**
 * Takes a value for Hz as number or string, optional arg for string return, and returns conversion to KHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateHzToKhz } from  'strinigman-utils';
 * //or
 * const clockRateHzToKhz = require('stringman-utils').clockRateHzToKhz;
 *
 * console.log(clockRateHzToKhz(1000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} hz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateHzToKhz(hz: number | string, stringReturn?: boolean): number | string {
  return precisionMathDivide(hz, metricStep, stringReturn, 'clockRateHzToKhz');
}

/**
 * Takes a value for KHz as number or string, optional arg for string return, and returns conversion to Hz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateKhzToHz } from  'strinigman-utils';
 * //or
 * const clockRateKhzToHz = require('stringman-utils').clockRateKhzToHz;
 *
 * console.log(clockRateKhzToHz(1)); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} khz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateKhzToHz(khz: number | string, stringReturn?: boolean): number | string {
  return precisionMathMultiply(khz, metricStep, stringReturn, 'clockRateKhzToHz');
}

/**
 * Takes a value for Hz as number or string, optional arg for string return, and returns conversion to MHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateHzToMhz } from  'strinigman-utils';
 * //or
 * const clockRateHzToMhz = require('stringman-utils').clockRateHzToMhz;
 *
 * console.log(clockRateHzToMhz(1000000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} hz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateHzToMhz(hz: number | string, stringReturn?: boolean): number | string {
  return precisionMathDivide(
    hz,
    precisionMathPower(metricStep, 2),
    stringReturn,
    'clockRateHzToMhz'
  );
}

/**
 * Takes a value for MHz as number or string, optional arg for string return, and returns conversion to Hz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateMhzToHz } from  'strinigman-utils';
 * //or
 * const clockRateMhzToHz = require('stringman-utils').clockRateMhzToHz;
 *
 * console.log(clockRateMhzToHz(1)); // 1000000
 * ```
 *
 * @export
 * @param {(number | string)} hz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateMhzToHz(mhz: number | string, stringReturn?: boolean): number | string {
  return precisionMathMultiply(
    mhz,
    precisionMathPower(metricStep, 2),
    stringReturn,
    'clockRateMhzToHz'
  );
}

/**
 * Takes a value for KHz as number or string, optional arg for string return, and returns conversion to GHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateKhzToGhz } from  'strinigman-utils';
 * //or
 * const clockRateKhzToGhz = require('stringman-utils').clockRateKhzToGhz;
 *
 * console.log(clockRateKhzToGhz(1000000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} khz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateKhzToGhz(khz: number | string, stringReturn?: boolean): number | string {
  return precisionMathDivide(
    khz,
    precisionMathPower(metricStep, 2),
    stringReturn,
    'clockRateKhzToGhz'
  );
}

/**
 * Takes a value for GHz as number or string, optional arg for string return, and returns conversion to KHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateGhzToKhz } from  'strinigman-utils';
 * //or
 * const clockRateGhzToKhz = require('stringman-utils').clockRateGhzToKhz;
 *
 * console.log(clockRateGhzToKhz(1)); // 1000000
 * ```
 *
 * @export
 * @param {(number | string)} ghz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateGhzToKhz(ghz: number | string, stringReturn?: boolean): number | string {
  return precisionMathMultiply(
    ghz,
    precisionMathPower(metricStep, 2),
    stringReturn,
    'clockRateGhzToKhz'
  );
}

/**
 * Takes a value for GHz as number or string, optional arg for string return, and returns conversion to Hz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateGhzToHz } from  'strinigman-utils';
 * //or
 * const clockRateGhzToHz = require('stringman-utils').clockRateGhzToHz;
 *
 * console.log(clockRateGhzToHz(1)); // 1000000000
 * ```
 *
 * @export
 * @param {(number | string)} ghz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateGhzToHz(ghz: number | string, stringReturn?: boolean): number | string {
  return precisionMathMultiply(
    ghz,
    precisionMathPower(metricStep, 3),
    stringReturn,
    'clockRateGhzToHz'
  );
}

/**
 * Takes a value for Hz as number or string, optional arg for string return, and returns conversion to GHz
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { clockRateHzToGhz } from  'strinigman-utils';
 * //or
 * const clockRateHzToGhz = require('stringman-utils').clockRateHzToGhz;
 *
 * console.log(clockRateHzToGhz(1000000000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} hz
 * @param {boolean} [stringReturn]
 * @return {*}  {(number | string)}
 */
export function clockRateHzToGhz(hz: number | string, stringReturn?: boolean): number | string {
  return precisionMathDivide(
    hz,
    precisionMathPower(metricStep, 3),
    stringReturn,
    'clockRateHzToGhz'
  );
}
