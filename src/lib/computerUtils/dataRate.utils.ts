// dataRate.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting data transfer rates value to
 * different units. For example, converting Mbps to Gbps.
 *
 * General naming convention:
 * - camel cased
 * - `dataRate` prefix
 * - `dataRate<current value unit>To<desired value unit>`
 * - `dataRateMbpsToGbps`
 *
 * You can chain these for more combinations. Example (you want kilobytes per second converted to bits per second):
 * `dataRateBytesToBits(dataRateKbpsToBps(2)); // 16000 (16,000 bits/s is 2 kilobytes/s or kBps)`
 *
 * @module data rate utils
 **/

import { metricStep } from '../../util/constants/common.constants';
import { bitsInByte } from '../../util/constants/computer.constants';
import {
  precisionMathDivide,
  precisionMathMultiply,
  precisionMathPower
} from '../precisionMathUtils';

/**
 * Takes a value for bits per second, optional arg for string return, and returns conversion to kilobits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateBpsToKbps } from 'stringman-utils';
 * // or
 * const dataRateBpsToKbps = require('stringman-utils').dataRateBpsToKbps;
 *
 * console.log('dataRateBpsToKbps(1000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateBpsToKbps(bps: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(bps, metricStep, strReturn, 'dataRateBpsToKbps');
}

/**
 * Takes a value for kilobits per second, optional arg for string return, and returns conversion to bits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateKbpsToBps } from 'stringman-utils';
 * // or
 * const dataRateKbpsToBps = require('stringman-utils').dataRateKbpsToBps;
 *
 * console.log('dataRateKbpsToBps(1)); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} kbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateKbpsToBps(kbps: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(kbps, metricStep, strReturn, 'dataRateKbpsToBps');
}

/**
 * Takes a value for kilobits per second, optional arg for string return, and returns conversion to megabits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateKbpsToMbps } from 'stringman-utils';
 * // or
 * const dataRateKbpsToMbps = require('stringman-utils').dataRateKbpsToMbps;
 *
 * console.log('dataRateKbpsToMbps(1000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateKbpsToMbps(kbps: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(kbps, metricStep, strReturn, 'dataRateKbpsToMbps');
}

/**
 * Takes a value for megabits per second, optional arg for string return, and returns conversion to kilobits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateMbpsToKbps } from 'stringman-utils';
 * // or
 * const dataRateMbpsToKbps = require('stringman-utils').dataRateMbpsToKbps;
 *
 * console.log('dataRateMbpsToKbps(1)); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} mbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateMbpsToKbps(mbps: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(mbps, metricStep, strReturn, 'dataRateMbpsToKbps');
}

/**
 * Takes a value for megabits per second, optional arg for string return, and returns conversion to gigabits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateMbpsToGbps } from 'stringman-utils';
 * // or
 * const dataRateMbpsToGbps = require('stringman-utils').dataRateMbpsToGbps;
 *
 * console.log('dataRateMbpsToGbps(1000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} mbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateMbpsToGbps(mbps: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(mbps, metricStep, strReturn, 'dataRateMbpsToGbps');
}

/**
 * Takes a value for gigabits per second, optional arg for string return, and returns conversion to megabits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateGbpsToMbps } from 'stringman-utils';
 * // or
 * const dataRateGbpsToMbps = require('stringman-utils').dataRateGbpsToMbps;
 *
 * console.log('dataRateGbpsToMbps(1)); // 1000
 * ```
 *
 * @export
 * @param {(number | string)} gbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateGbpsToMbps(gbps: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(gbps, metricStep, strReturn, 'dataRateGbpsToMbps');
}

/**
 * Takes a value for bits per second, optional arg for string return, and returns conversion to megabits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateBpsToMbps } from 'stringman-utils';
 * // or
 * const dataRateBpsToMbps = require('stringman-utils').dataRateBpsToMbps;
 *
 * console.log('dataRateBpsToMbps(1000000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateBpsToMbps(bps: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    bps,
    precisionMathPower(metricStep, 2),
    strReturn,
    'dataRateBpsToMbps'
  );
}

/**
 * Takes a value for megabits per second, optional arg for string return, and returns conversion to bits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateMbpsToBps } from 'stringman-utils';
 * // or
 * const dataRateMbpsToBps = require('stringman-utils').dataRateMbpsToBps;
 *
 * console.log('dataRateMbpsToBps(1)); // 1000000
 * ```
 *
 * @export
 * @param {(number | string)} mbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateMbpsToBps(mbps: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    mbps,
    precisionMathPower(metricStep, 2),
    strReturn,
    'dataRateMbpsToBps'
  );
}

/**
 * Takes a value for kilobits per second, optional arg for string return, and returns conversion to gigabits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateKbpsToGbps } from 'stringman-utils';
 * // or
 * const dataRateKbpsToGbps = require('stringman-utils').dataRateKbpsToGbps;
 *
 * console.log('dataRateKbpsToGbps(1000000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateKbpsToGbps(kbps: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    kbps,
    precisionMathPower(metricStep, 2),
    strReturn,
    'dataRateKbpsToGbps'
  );
}

/**
 * Takes a value for gigabits per second, optional arg for string return, and returns conversion to kilobits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateGbpsToKbps } from 'stringman-utils';
 * // or
 * const dataRateGbpsToKbps = require('stringman-utils').dataRateGbpsToKbps;
 *
 * console.log('dataRateGbpsToKbps(1)); // 1000000
 * ```
 *
 * @export
 * @param {(number | string)} gbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateGbpsToKbps(gbps: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    gbps,
    precisionMathPower(metricStep, 2),
    strReturn,
    'dataRateGbpsToKbps'
  );
}

/**
 * Takes a value for bits per second, optional arg for string return, and returns conversion to gigabits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateBpsToGbps } from 'stringman-utils';
 * // or
 * const dataRateBpsToGbps = require('stringman-utils').dataRateBpsToGbps;
 *
 * console.log('dataRateBpsToGbps(1000000000)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateBpsToGbps(bps: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    bps,
    precisionMathPower(metricStep, 3),
    strReturn,
    'dataRateBpsToGbps'
  );
}

/**
 * Takes a value for gigabits per second, optional arg for string return, and returns conversion to bits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateGbpsToBps } from 'stringman-utils';
 * // or
 * const dataRateGbpsToBps = require('stringman-utils').dataRateGbpsToBps;
 *
 * console.log('dataRateGbpsToBps(1)); // 1000000000
 * ```
 *
 * @export
 * @param {(number | string)} mbps
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateGbpsToBps(mbps: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    mbps,
    precisionMathPower(metricStep, 3),
    strReturn,
    'dataRateGbpsToBps'
  );
}

/**
 * Takes a value for bytes per second, optional arg for string return, and returns conversion to bits per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateBytesToBits } from 'stringman-utils';
 * // or
 * const dataRateBytesToBits = require('stringman-utils').dataRateBytesToBits;
 *
 * console.log('dataRateBytesToBits(1)); // 8
 * ```
 *
 * @export
 * @param {(number | string)} bytes
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateBytesToBits(bytes: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(bytes, bitsInByte, strReturn, 'dataRateBytesToBits');
}

/**
 * Takes a value for bits per second, optional arg for string return, and returns conversion to bytes per second
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataRateBitsToBytes } from 'stringman-utils';
 * // or
 * const dataRateBitsToBytes = require('stringman-utils').dataRateBitsToBytes;
 *
 * console.log('dataRateBitsToBytes(8)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bits
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataRateBitsToBytes(bits: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(bits, bitsInByte, strReturn, 'dataRateBitsToBytes');
}
