// dataStorage.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting data storage values to
 * different units. For example, converting MB to GB.
 *
 * General naming convention:
 * - camel cased
 * - `dataStorage` prefix
 * - `dataStorage<current value unit>To<desired value unit>`
 * - `dataStorageMBToGB`
 *
 * @module data storage utils
 **/

import { StorageUnits } from '../../models/computers.model';
import {
  dataDecimalStep,
  gigsOnBd,
  gigsOnCd,
  gigsOnDvd
} from '../../util/constants/computer.constants';
import { INVALID_UNIT, ONE_NUM_ERROR } from '../../util/errors';
import { isNumberString } from '../../util/validateArgs';
import {
  precisionMathDivide,
  precisionMathMultiply,
  precisionMathPower
} from '../precisionMathUtils';

const unitArr = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

function convertToGb(amount: number | string, unit: StorageUnits, caller: string): string {
  switch (unit) {
    case 'b':
      return dataStorageBToGb(amount, true) as string;
    case 'kb':
      return dataStorageKbToGb(amount, true) as string;
    case 'mb':
      return dataStorageMbToGb(amount, true) as string;
    case 'gb':
      return amount.toString();
    case 'tb':
      return dataStorageTbToGb(amount, true) as string;
    case 'pb':
      return dataStoragePbToGb(amount, true) as string;
    default:
      throw new Error(`${caller}: ${INVALID_UNIT}`);
  }
}

/**
 * Takes a value for bytes as number or string, optional arg for string return, and returns the conversion to kilobytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageBToKb } from 'stringman-utils';
 * // or
 * const dataStorageBToKb = require('stringman-utils').dataStorageBToKb;
 *
 * console.log(dataStorageBToKb(1024)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bytes
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageBToKb(bytes: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(bytes, dataDecimalStep, strReturn, 'dataStorageBToKb');
}

/**
 * Takes a value for kilobytes as number or string, optional arg for string return, and returns the conversion to bytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageKbToB } from 'stringman-utils';
 * // or
 * const dataStorageKbToB = require('stringman-utils').dataStorageKbToB;
 *
 * console.log(dataStorageKbToB(1)); // 1024
 * ```
 *
 * @export
 * @param {(number | string)} kb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageKbToB(kb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(kb, dataDecimalStep, strReturn, 'dataStorageKbToB');
}

/**
 * Takes a value for kilobytes as number or string, optional arg for string return, and returns the conversion to megabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageKbToMb } from 'stringman-utils';
 * // or
 * const dataStorageKbToMb = require('stringman-utils').dataStorageKbToMb;
 *
 * console.log(dataStorageKbToMb(1024)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageKbToMb(kb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(kb, dataDecimalStep, strReturn, 'dataStorageKbToMb');
}

/**
 * Takes a value for megabytes as number or string, optional arg for string return, and returns the conversion to kilobytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageMbToKb } from 'stringman-utils';
 * // or
 * const dataStorageMbToKb = require('stringman-utils').dataStorageMbToKb;
 *
 * console.log(dataStorageMbToKb(1)); // 1024
 * ```
 *
 * @export
 * @param {(number | string)} mb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageMbToKb(mb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(mb, dataDecimalStep, strReturn, 'dataStorageMbToKb');
}

/**
 * Takes a value for megabytes as number or string, optional arg for string return, and returns the conversion to gigabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageMbToGb } from 'stringman-utils';
 * // or
 * const dataStorageMbToGb = require('stringman-utils').dataStorageMbToGb;
 *
 * console.log(dataStorageMbToGb(1024)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} mb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageMbToGb(mb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(mb, dataDecimalStep, strReturn, 'dataStorageMbToGb');
}

/**
 * Takes a value for gigabytes as number or string, optional arg for string return, and returns the conversion to megabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageGbToMb } from 'stringman-utils';
 * // or
 * const dataStorageGbToMb = require('stringman-utils').dataStorageGbToMb;
 *
 * console.log(dataStorageGbToMb(1)); // 1024
 * ```
 *
 * @export
 * @param {(number | string)} gb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageGbToMb(gb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(gb, dataDecimalStep, strReturn, 'dataStorageGbToMb');
}

/**
 * Takes a value for gigabytes as number or string, optional arg for string return, and returns the conversion to terabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageGbToTb } from 'stringman-utils';
 * // or
 * const dataStorageGbToTb = require('stringman-utils').dataStorageGbToTb;
 *
 * console.log(dataStorageGbToTb(1024)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} gb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageGbToTb(gb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(gb, dataDecimalStep, strReturn, 'dataStorageGbToTb');
}

/**
 * Takes a value for terabytes as number or string, optional arg for string return, and returns the conversion to gigabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageTbToGb } from 'stringman-utils';
 * // or
 * const dataStorageTbToGb = require('stringman-utils').dataStorageTbToGb;
 *
 * console.log(dataStorageTbToGb(1)); // 1024
 * ```
 *
 * @export
 * @param {(number | string)} tb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageTbToGb(tb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(tb, dataDecimalStep, strReturn, 'dataStorageTbToGb');
}

/**
 * Takes a value for terabytes as number or string, optional arg for string return, and returns the conversion to petabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageTbToPb } from 'stringman-utils';
 * // or
 * const dataStorageTbToPb = require('stringman-utils').dataStorageTbToPb;
 *
 * console.log(dataStorageTbToPb(1024)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} tb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageTbToPb(tb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(tb, dataDecimalStep, strReturn, 'dataStorageTbToPb');
}

/**
 * Takes a value for petabytes as number or string, optional arg for string return, and returns the conversion to terabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStoragePbToTb } from 'stringman-utils';
 * // or
 * const dataStoragePbToTb = require('stringman-utils').dataStoragePbToTb;
 *
 * console.log(dataStoragePbToTb(1)); // 1024
 * ```
 *
 * @export
 * @param {(number | string)} pb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStoragePbToTb(pb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(pb, dataDecimalStep, strReturn, 'dataStoragePbToTb');
}

/**
 * Takes a value for bytes as number or string, optional arg for string return, and returns the conversion to megabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageBToMb } from 'stringman-utils';
 * // or
 * const dataStorageBToMb = require('stringman-utils').dataStorageBToMb;
 *
 * console.log(dataStorageBToMb(1048576)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bytes
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageBToMb(bytes: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    bytes,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageBToMb'
  );
}

/**
 * Takes a value for megabytes as number or string, optional arg for string return, and returns the conversion to bytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageMbToB } from 'stringman-utils';
 * // or
 * const dataStorageMbToB = require('stringman-utils').dataStorageMbToB;
 *
 * console.log(dataStorageMbToB(1)); // 1048576
 * ```
 *
 * @export
 * @param {(number | string)} kb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageMbToB(kb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    kb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageMbToB'
  );
}

/**
 * Takes a value for kilobytes as number or string, optional arg for string return, and returns the conversion to gigabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageKbToGb } from 'stringman-utils';
 * // or
 * const dataStorageKbToGb = require('stringman-utils').dataStorageKbToGb;
 *
 * console.log(dataStorageKbToGb(1048576)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageKbToGb(kb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    kb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageKbToGb'
  );
}

/**
 * Takes a value for gigabytes as number or string, optional arg for string return, and returns the conversion to kilobytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageGbToKb } from 'stringman-utils';
 * // or
 * const dataStorageGbToKb = require('stringman-utils').dataStorageGbToKb;
 *
 * console.log(dataStorageGbToKb(1)); // 1048576
 * ```
 *
 * @export
 * @param {(number | string)} gb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageGbToKb(gb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    gb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageGbToKb'
  );
}

/**
 * Takes a value for megabytes as number or string, optional arg for string return, and returns the conversion to terabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageMbToTb } from 'stringman-utils';
 * // or
 * const dataStorageMbToTb = require('stringman-utils').dataStorageMbToTb;
 *
 * console.log(dataStorageMbToTb(1048576)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} mb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageMbToTb(mb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    mb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageMbToTb'
  );
}

/**
 * Takes a value for terabytes as number or string, optional arg for string return, and returns the conversion to megabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageTbToMb } from 'stringman-utils';
 * // or
 * const dataStorageTbToMb = require('stringman-utils').dataStorageTbToMb;
 *
 * console.log(dataStorageTbToMb(1)); // 1048576
 * ```
 *
 * @export
 * @param {(number | string)} tb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageTbToMb(tb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    tb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageTbToMb'
  );
}

/**
 * Takes a value for bytes as number or string, optional arg for string return, and returns the conversion to gigabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageBToGb } from 'stringman-utils';
 * // or
 * const dataStorageBToGb = require('stringman-utils').dataStorageBToGb;
 *
 * console.log(dataStorageBToGb(1073741824)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bytes
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageBToGb(bytes: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    bytes,
    precisionMathPower(dataDecimalStep, 3),
    strReturn,
    'dataStorageBToGb'
  );
}

/**
 * Takes a value for gigabytes as number or string, optional arg for string return, and returns the conversion to bytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageGbToB } from 'stringman-utils';
 * // or
 * const dataStorageGbToB = require('stringman-utils').dataStorageGbToB;
 *
 * console.log(dataStorageGbToB(1)); // 1073741824
 * ```
 *
 * @export
 * @param {(number | string)} gb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageGbToB(gb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    gb,
    precisionMathPower(dataDecimalStep, 3),
    strReturn,
    'dataStorageGbToB'
  );
}

/**
 * Takes a value for kilobytes as number or string, optional arg for string return, and returns the conversion to terabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageKbToTb } from 'stringman-utils';
 * // or
 * const dataStorageKbToTb = require('stringman-utils').dataStorageKbToTb;
 *
 * console.log(dataStorageKbToTb(1073741824)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageKbToTb(kb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    kb,
    precisionMathPower(dataDecimalStep, 3),
    strReturn,
    'dataStorageKbToTb'
  );
}

/**
 * Takes a value for terabytes as number or string, optional arg for string return, and returns the conversion to kilobytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageTbToKb } from 'stringman-utils';
 * // or
 * const dataStorageTbToKb = require('stringman-utils').dataStorageTbToKb;
 *
 * console.log(dataStorageTbToKb(1)); // 1073741824
 * ```
 *
 * @export
 * @param {(number | string)} tb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageTbToKb(tb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    tb,
    precisionMathPower(dataDecimalStep, 3),
    strReturn,
    'dataStorageTbToKb'
  );
}

/**
 * Takes a value for megabytes as number or string, optional arg for string return, and returns the conversion to petabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageMbToPb } from 'stringman-utils';
 * // or
 * const dataStorageMbToPb = require('stringman-utils').dataStorageMbToPb;
 *
 * console.log(dataStorageMbToPb(1073741824)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} mb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageMbToPb(mb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    mb,
    precisionMathPower(dataDecimalStep, 3),
    strReturn,
    'dataStorageMbToPb'
  );
}

/**
 * Takes a value for petabytes as number or string, optional arg for string return, and returns the conversion to megabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStoragePbToMb } from 'stringman-utils';
 * // or
 * const dataStoragePbToMb = require('stringman-utils').dataStoragePbToMb;
 *
 * console.log(dataStoragePbToMb(1)); // 1073741824
 * ```
 *
 * @export
 * @param {(number | string)} pb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStoragePbToMb(pb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    pb,
    precisionMathPower(dataDecimalStep, 3),
    strReturn,
    'dataStoragePbToMb'
  );
}

/**
 * Takes a value for bytes as number or string, optional arg for string return, and returns the conversion to terabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageBToTb } from 'stringman-utils';
 * // or
 * const dataStorageBToTb = require('stringman-utils').dataStorageBToTb;
 *
 * console.log(dataStorageBToTb(1099511627776)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bytes
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageBToTb(bytes: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    bytes,
    precisionMathPower(dataDecimalStep, 4),
    strReturn,
    'dataStorageBToTb'
  );
}

/**
 * Takes a value for terabytes as number or string, optional arg for string return, and returns the conversion to bytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageTbToB } from 'stringman-utils';
 * // or
 * const dataStorageTbToB = require('stringman-utils').dataStorageTbToB;
 *
 * console.log(dataStorageTbToB(1)); // 1099511627776
 * ```
 *
 * @export
 * @param {(number | string)} tb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageTbToB(tb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    tb,
    precisionMathPower(dataDecimalStep, 4),
    strReturn,
    'dataStorageTbToB'
  );
}

/**
 * Takes a value for kilobytes as number or string, optional arg for string return, and returns the conversion to petabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageKbToPb } from 'stringman-utils';
 * // or
 * const dataStorageKbToPb = require('stringman-utils').dataStorageKbToPb;
 *
 * console.log(dataStorageKbToPb(1099511627776)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} kb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageKbToPb(kb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    kb,
    precisionMathPower(dataDecimalStep, 4),
    strReturn,
    'dataStorageKbToPb'
  );
}

/**
 * Takes a value for petabytes as number or string, optional arg for string return, and returns the conversion to kilobytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStoragePbToKb } from 'stringman-utils';
 * // or
 * const dataStoragePbToKb = require('stringman-utils').dataStoragePbToKb;
 *
 * console.log(dataStoragePbToKb(1)); // 1099511627776
 * ```
 *
 * @export
 * @param {(number | string)} pb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStoragePbToKb(pb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    pb,
    precisionMathPower(dataDecimalStep, 4),
    strReturn,
    'dataStoragePbToKb'
  );
}

/**
 * Takes a value for bytes as number or string, optional arg for string return, and returns the conversion to petabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageBToPb } from 'stringman-utils';
 * // or
 * const dataStorageBToPb = require('stringman-utils').dataStorageBToPb;
 *
 * console.log(dataStorageBToPb(1125899906842624)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} bytes
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageBToPb(bytes: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    bytes,
    precisionMathPower(dataDecimalStep, 5),
    strReturn,
    'dataStorageBToPb'
  );
}

/**
 * Takes a value for petabytes as number or string, optional arg for string return, and returns the conversion to bytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStoragePbToB } from 'stringman-utils';
 * // or
 * const dataStoragePbToB = require('stringman-utils').dataStoragePbToB;
 *
 * console.log(dataStoragePbToB(1)); // 1125899906842624
 * ```
 *
 * @export
 * @param {(number | string)} pb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStoragePbToB(pb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    pb,
    precisionMathPower(dataDecimalStep, 5),
    strReturn,
    'dataStoragePbToB'
  );
}

/**
 * Takes a value for petabytes as number or string, optional arg for string return, and returns the conversion to gigabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStoragePbToGb } from 'stringman-utils';
 * // or
 * const dataStoragePbToGb = require('stringman-utils').dataStoragePbToGb;
 *
 * console.log(dataStoragePbToGb(1)); // 1048576
 * ```
 *
 * @export
 * @param {(number | string)} pb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStoragePbToGb(pb: number | string, strReturn?: boolean): number | string {
  return precisionMathMultiply(
    pb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStoragePbToGb'
  );
}

/**
 * Takes a value for gigabytes as number or string, optional arg for string return, and returns the conversion to petabytes
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { dataStorageGbToPb } from 'stringman-utils';
 * // or
 * const dataStorageGbToPb = require('stringman-utils').dataStorageGbToPb;
 *
 * console.log(dataStorageGbToPb(1048576)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} pb
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageGbToPb(pb: number | string, strReturn?: boolean): number | string {
  return precisionMathDivide(
    pb,
    precisionMathPower(dataDecimalStep, 2),
    strReturn,
    'dataStorageGbToPb'
  );
}

/**
 * Takes a value, an arg for units, and an optional arg for a string return and returns how many CD's it would take
 * to store the value/unit (650MB for a CD)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Unit values:
 * - 'b' = bytes
 * - 'kb' = kilobytes
 * - 'mb' = megabytes
 * - 'gb' = gigabytes
 * - 'tb' = terabytes
 * - 'pb' = petabytes
 *
 * Basic usage example:
 * ```js
 * import { dataStorageToCd } from 'stringman-utils';
 * // or
 * const dataStorageToCd = require('stringman-utils').dataStorageToCd;
 *
 * console.log(dataStorageToCd(650, 'mb')); // 1
 * ```
 *
 * @export
 * @param {(number | string)} amount
 * @param {StorageUnits} unit
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageToCd(
  amount: number | string,
  unit: StorageUnits,
  strReturn?: boolean
): number | string {
  if (unitArr.indexOf(unit) < 0) {
    throw new Error(`dataStorageToCd: ${INVALID_UNIT}`);
  }
  if (!isNumberString(amount)) {
    throw new Error(`dataStorageToCd: ${ONE_NUM_ERROR}`);
  }
  const inGb = convertToGb(amount, unit, 'dataStorageToCd');
  return precisionMathDivide(inGb, gigsOnCd, strReturn, 'dataStorageToCd');
}

/**
 *  Takes a value, an arg for units, and an optional arg for a string return and returns how many DVD's it would take
 * to store the value/unit (4.38GB for a DVD)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Unit values:
 * - 'b' = bytes
 * - 'kb' = kilobytes
 * - 'mb' = megabytes
 * - 'gb' = gigabytes
 * - 'tb' = terabytes
 * - 'pb' = petabytes
 *
 * Basic usage example:
 * ```js
 * import { dataStorageToDvd } from 'stringman-utils';
 * // or
 * const dataStorageToDvd = require('stringman-utils').dataStorageToDvd;
 *
 * console.log(dataStorageToDvd(4.38, 'gb')); // 1
 * ```
 *
 * @export
 * @param {(number | string)} amount
 * @param {StorageUnits} unit
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageToDvd(
  amount: number | string,
  unit: StorageUnits,
  strReturn?: boolean
): number | string {
  if (unitArr.indexOf(unit) < 0) {
    throw new Error(`dataStorageToDvd: ${INVALID_UNIT}`);
  }
  if (!isNumberString(amount)) {
    throw new Error(`dataStorageToDvd: ${ONE_NUM_ERROR}`);
  }
  const inGb = convertToGb(amount, unit, 'dataStorageToCd');
  return precisionMathDivide(inGb, gigsOnDvd, strReturn, 'dataStorageToCd');
}

/**
 *  Takes a value, an arg for units, and an optional arg for a string return and returns how many BD's it would take
 * to store the value/unit (25GB for a BD)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Unit values:
 * - 'b' = bytes
 * - 'kb' = kilobytes
 * - 'mb' = megabytes
 * - 'gb' = gigabytes
 * - 'tb' = terabytes
 * - 'pb' = petabytes
 *
 * Basic usage example:
 * ```js
 * import { dataStorageToBd } from 'stringman-utils';
 * // or
 * const dataStorageToBd = require('stringman-utils').dataStorageToBd;
 *
 * console.log(dataStorageToBd(25, 'gb')); // 1
 * ```
 *
 * @export
 * @param {(number | string)} amount
 * @param {StorageUnits} unit
 * @param {boolean} [strReturn]
 * @return {*}  {(number | string)}
 */
export function dataStorageToBd(
  amount: number | string,
  unit: StorageUnits,
  strReturn?: boolean
): number | string {
  if (unitArr.indexOf(unit) < 0) {
    throw new Error(`dataStorageToBd: ${INVALID_UNIT}`);
  }
  if (!isNumberString(amount)) {
    throw new Error(`dataStorageToBd: ${ONE_NUM_ERROR}`);
  }
  const inGb = convertToGb(amount, unit, 'dataStorageToCd');
  return precisionMathDivide(inGb, gigsOnBd, strReturn, 'dataStorageToCd');
}
