// case.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting strings to a different case.
 * example: camel case to snake case
 *
 * General naming convention:
 * - camel cased
 * - `case` prefix
 * - `case<desired stringCase>`
 * - `caseKebabCase`
 *
 * @module case utils
 **/

import { matchChars } from '../../util/constants/regex.constants';
import { MUST_BE_STRING } from '../../util/errors';
import { wordCapitalize } from '../wordUtils';

/**
 * Takes a string and returns it in kebab case
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { caseKebabCase } from 'stringman-utils';
 * // or
 * const caseKebabCase = require('stringman-utils').caseKebabCase;
 *
 * const test = caseKebabCase('ThisIsATest');
 * console.log(test); // 'this-is-a-test'
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function caseKebabCase(str: string): string {
  if (typeof str !== 'string') {
    throw new Error(`caseKebabCase: ${MUST_BE_STRING}`);
  }

  return (str.match(matchChars) || []).map((x: string) => x.toLowerCase()).join('-') || '';
}

/**
 * Takes a string and returns it in snake case
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { caseSnakeCase } from 'stringman-utils';
 * // or
 * const caseSnakeCase = require('stringman-utils').caseSnakeCase;
 *
 * const test = caseSnakeCase('ThisIsATest');
 * console.log(test); // 'this_is_a_test'
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function caseSnakeCase(str: string): string {
  if (typeof str !== 'string') {
    throw new Error(`caseSnakeCase: ${MUST_BE_STRING}`);
  }
  return (str.match(matchChars) || []).map((x: string) => x.toLowerCase()).join('_') || '';
}

/**
 * Takes a string and returns the resulting string delimited with single spaces
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { caseSpaceCase } from 'stringman-utils';
 * // or
 * const caseSpaceCase = require('stringman-utils').caseSpaceCase;
 *
 * const test = caseSpaceCase('ThisIsATest');
 * console.log(test); // 'this is a test'
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function caseSpaceCase(str: string): string {
  if (typeof str !== 'string') {
    throw new Error(`caseSpaceCase: ${MUST_BE_STRING}`);
  }
  return (str.match(matchChars) || []).map((x: string) => x.toLowerCase()).join(' ') || '';
}

/**
 * Takes a string and returns the string as camel case
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { caseCamelCase } from 'stringman-utils';
 * // or
 * const caseCamelCase = require('stringman-utils').caseCamelCase;
 *
 * const test = caseCamelCase('This-Is-A-Test');
 * console.log(test); // 'thisIsATest'
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function caseCamelCase(str: string): string {
  if (typeof str !== 'string') {
    throw new Error(`caseCamelCase: ${MUST_BE_STRING}`);
  }
  return (
    (str.match(matchChars) || [])
      .map((x: string, i: number) => {
        if (i === 0) {
          return x.toLowerCase();
        }
        return wordCapitalize(x.toLowerCase());
      })
      .join('') || ''
  );
}

/**
 * Takes a string and returns it as Pascal case
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { casePascalCase } from 'stringman-utils';
 * // or
 * const casePascalCase = require('stringman-utils').casePascalCase;
 *
 * const test = casePascalCase('This-Is-A-Test');
 * console.log(test); // 'ThisIsATest'
 * ```
 *
 * @export
 * @param {string} str
 * @return {*}  {string}
 */
export function casePascalCase(str: string): string {
  if (typeof str !== 'string') {
    throw new Error(`casePascalCase: ${MUST_BE_STRING}`);
  }
  return (
    (str.match(matchChars) || []).map((x: string) => wordCapitalize(x.toLowerCase())).join('') || ''
  );
}
