// url.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various string
 * manipulation operations regarding URLs on the string passed.
 * example: retrieving a URL from a string
 *
 *
 * General naming convention:
 * - camel cased
 * - `url` prefix
 * - `url<desired operation>`
 * - `urlRetrieve`
 *
 * @module url utils
 **/

import { domainExp, urlStrRegex } from '../../util/constants/regex.constants';
import { stringIsValid, stringRemove, stringRetrieve, stringSwap } from '../stringUtils';

/**
 * Takes string and returns any matches for a valid URL from the string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { urlRetrieve } from 'stringman-utils';
 * // or
 * const urlRetrieve = require('stringman-utils').urlRetrieve;
 *
 * const justUrl = urlRetrieve('the url to my portfolio is https://joeyg.me');
 * console.log(justUrl); // 'https://joeyg.me'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function urlRetrieve(str: string): string[] {
  return stringRetrieve(str, urlStrRegex);
}

/**
 * Takes a string and returns whether string is a valid URL as a boolean
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { urlIsValid } from 'stringman-utils';
 * // or
 * const urlIsValid = require('stringman-utils').urlIsValid;
 *
 * const valid = urlIsValid('https://joeyg.me');
 * const invalid = urlIsValid('https/joeyg.me');
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} str
 * @returns {boolean}
 */
export function urlIsValid(str: string): boolean {
  return stringIsValid(str, urlStrRegex);
}

/**
 * Takes string, removes any URLs, trims, and returns string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { urlRemove } from 'stringman-utils';
 * // or
 * const urlRemove = require('stringman-utils').urlRemove;
 *
 * const removed = urlRemove('the url to my portfolio is https://joeyg.me');
 * console.log(removed); // 'the url to my portfolio is'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function urlRemove(str: string): string {
  return stringRemove(str, urlStrRegex);
}

/**
 * takes a string with a url and a second string, swaps any urls in the first string with the second string, and returns the result
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { urlSwap } from 'stringman-utils';
 * // or
 * const urlSwap = require('stringman-utils').urlSwap;
 *
 * const swapped = urlSwap('this is a test for https://test.com', 'http://www.fakesite.org');
 * console.log(swapped); // 'this is a test for http://www.fakesite.org'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} other
 * @returns {string}
 */
export function urlSwap(str: string, other: string): string {
  return stringSwap(str, other, urlStrRegex);
}

/**
 * Takes url string, tries to return just the domain
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { urlGetDomain } from 'stringman-utils';
 * // or
 * const urlGetDomain = require('stringman-utils').urlGetDomain;
 *
 * const domain = urlGetDomain('https://www.google.com/test');
 * console.log(domain); // '[google.com]'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function urlGetDomain(str: string): string[] {
  return stringRetrieve(str, domainExp).splice(1);
}
