// colors.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting colors from
 * one format to another.
 * example: RGB to hex
 *
 * General naming convention:
 * - camel cased
 * - `color` prefix
 * - `color<current color format>To<desired stringCase>`
 * - `colorRgbToHex`
 *
 * @module color utils
 **/

import { IHsl } from '../../models/colors.model';
import { NOT_VALID_HEX_COLOR, TWO_NUM_ERROR } from '../../util/errors';
import { isNumberString } from '../../util/validateArgs';
import { numberConvertToHex, numberIsWhole } from '../numberUtils';
import { stringIsValid } from '../stringUtils';

/**
 * Takes rgb color values and returns hexidecimal equivalent
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { colorRgbToHex } from 'stringman-utils';
 * // or
 * const colorRgbToHex = require('stringman-utils').colorRgbToHex;
 *
 * const test = colorRgbToHex(22, 33, 44);
 * console.log(test); // '#16212C'
 * ```
 *
 * @export
 * @param {(number | string)} r
 * @param {(number | string)} g
 * @param {(number| string)} b
 * @returns {string}
 */
export function colorRgbToHex(r: number | string, g: number | string, b: number | string): string {
  if (isNumberString(r) && isNumberString(g) && isNumberString(b)) {
    return '#' + numberConvertToHex(r) + numberConvertToHex(g) + numberConvertToHex(b);
  } else {
    throw new Error(`colorRgbToHex: ${TWO_NUM_ERROR}`);
  }
}

/**
 * Takes a string and returns boolean indicating whether it is a valid hexidecimal color
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { colorIsHexColor } from 'stringman-utils';
 * // or
 * const colorIsHexColor = require('stringman-utils').colorIsHexColor;
 *
 * const valid = colorIsHexColor('16212c');
 * const invalid = colorIsHexColor('nope');
 * console.log(valid); // true
 * console.log(invalid); // false
 * ```
 *
 * @export
 * @param {string} color
 * @returns {boolean}
 */
export function colorIsHexColor(color: string): boolean {
  if (typeof color !== 'string') {
    return false;
  }
  const colorCleaned = color.charAt(0) === '#' ? color.slice(1) : color;
  return stringIsValid(colorCleaned, /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/);
}

/**
 * Takes a hexidecimal color string and returns the rgb value
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 *  Basic usage example:
 * ```js
 * import { colorHexToRgb } from 'stringman-utils';
 * // or
 * const colorHexToRgb = require('stringman-utils').colorHexToRgb;
 *
 * const test = colorHexToRgb('#16212c');
 * console.log(test); // [22, 33, 44]
 * ```
 *
 * @export
 * @param {string} hex
 * @returns {(number[] | null)}
 */
export function colorHexToRgb(hex: string): number[] | null {
  if (stringIsValid(hex, /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3}|[a-fA-F0-9]{2})$/)) {
    if (hex.charAt(0) === '#') {
      hex = hex.substr(1);
    }
    if (hex.length < 2 || hex.length > 6) {
      return null;
    }
    const values = hex.split('');
    let r;
    let g;
    let b;

    if (hex.length === 2) {
      r = parseInt(values[0].toString() + values[1].toString(), 16);
      g = r;
      b = r;
    } else if (hex.length === 3) {
      r = parseInt(values[0].toString() + values[0].toString(), 16);
      g = parseInt(values[1].toString() + values[1].toString(), 16);
      b = parseInt(values[2].toString() + values[2].toString(), 16);
    } else if (hex.length === 6) {
      r = parseInt(values[0].toString() + values[1].toString(), 16);
      g = parseInt(values[2].toString() + values[3].toString(), 16);
      b = parseInt(values[4].toString() + values[5].toString(), 16);
    } else {
      return null;
    }
    return [r, g, b];
  }
  throw new Error(`colorHexToRgb: ${NOT_VALID_HEX_COLOR}`);
}

/**
 * Takes a hex color value and a percentage and returns a hex color string that is the result of lightening or darkening the original color by the percentage
 * NOTE: this does not work on black or white!
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { colorLuminance } from 'stringman-utils';
 * // or
 * const colorLuminance = require('stringman-utils').colorLuminance;
 *
 * const lightened = colorLuminance('#63C6FF', 40);
 * const darkened = colorLuminance('#63C6FF', -40);
 * console.log(lightened); // '#8affff'
 * console.log(darkened); // '#3b7699'
 * ```
 *
 * @export
 * @param {string} color
 * @param {number} percent
 * @returns {string}
 */
export function colorLuminance(color: string, percent: number): string | null {
  // if color argument isn't actually a color
  if (!colorIsHexColor(color)) {
    throw new Error(`colorLuminance: ${NOT_VALID_HEX_COLOR}`);
  }
  // convert to hex
  const Rhex = parseInt(color.substring(1, 3), 16);
  const Ghex = parseInt(color.substring(3, 5), 16);
  const Bhex = parseInt(color.substring(5, 7), 16);

  // lighten or darken
  const Rcon = (Rhex * (100 + percent)) / 100;
  const Gcon = (Ghex * (100 + percent)) / 100;
  const Bcon = (Bhex * (100 + percent)) / 100;

  // make sure values aren't greater than 255 (max hex value)
  const R = Rcon < 255 ? parseInt(Rcon.toString(), 10) : 255;
  const G = Gcon < 255 ? parseInt(Gcon.toString(), 10) : 255;
  const B = Bcon < 255 ? parseInt(Bcon.toString(), 10) : 255;

  // make sure each color value is 2 characters and convert back to hex
  const RR = R.toString(16).length === 1 ? '0' + R.toString(16) : R.toString(16);
  const GG = G.toString(16).length === 1 ? '0' + G.toString(16) : G.toString(16);
  const BB = B.toString(16).length === 1 ? '0' + B.toString(16) : B.toString(16);

  return '#' + RR + GG + BB;
}

/**
 * Takes a hex color string and returns a string array with hex color converted to HSL [H, S, L]
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { colorHexToHsl } from 'stringman-utils';
 * // or
 * const colorHexToHsl = require('stringman-utils').colorHexToHsl;
 *
 * const toHsl = colorHexToHsl('#63C6FF');
 * console.log(toHsl); // {h: 201.9, s: '100%', l: '69.4%'}
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(IHsl | null)}
 */
export function colorHexToHsl(str: string): IHsl | null {
  if (colorIsHexColor(str)) {
    const hex = str.charAt(0) === '#' ? str.slice(1) : str;
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if (result && result.length) {
      const r = parseInt(result[1], 16);
      const g = parseInt(result[2], 16);
      const b = parseInt(result[3], 16);
      return colorRgbToHsl(r, g, b);
    } else {
      return null;
    }
  } else {
    throw new Error(`colorHexToHsl: ${NOT_VALID_HEX_COLOR}`);
  }
}

/**
 * Takes r, g, and b and numbers in 3 separate agurments, convert to hsl, and returns in object with h, s, and l as keys
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { colorRgbToHsl } from 'stringman-utils';
 * // or
 * const colorRgbToHsl = require('stringman-utils').colorRgbToHsl;
 *
 * const toHsl = colorRgbToHsl(99, 198, 255);
 * console.log(toHsl); // {h: 201.9, s: '100%', l: '69.4%'}
 * ```
 *
 * @export
 * @param {number} r
 * @param {number} g
 * @param {number} b
 * @returns {(IHsl | null)}
 */
export function colorRgbToHsl(r: number, g: number, b: number): IHsl | null {
  if (typeof r !== 'number' || typeof g !== 'number' || typeof b !== 'number') {
    throw new Error(`colorRgbToHsl: ${TWO_NUM_ERROR}`);
  }
  if (!numberIsWhole(r) || !numberIsWhole(g) || !numberIsWhole(b)) {
    throw new Error(`colorRgbToHsl: Arguments for RGB values must be whole numbers.`);
  }
  r /= 255;
  g /= 255;
  b /= 255;
  const max = Math.max(r, g, b);
  const min = Math.min(r, g, b);
  let h;
  let s;
  const l = (max + min) / 2;
  if (max === min) {
    h = s = 0; // achromatic
  } else {
    const d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;
    }
    h = h ? h / 6 : 0;
  }
  return {
    h: parseFloat((h * 360).toFixed(1)),
    l: parseFloat((l * 100).toFixed(1)),
    s: parseFloat((s * 100).toFixed(1))
  };
}
