// words.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various
 * operations regarding words on the string passed.
 * example: getting the estimated reading time of a blog post
 *
 *
 * General naming convention:
 * - camel cased
 * - `words` prefix
 * - `words<current operation>`
 * - `wordReadingTime`
 *
 * @module words utils
 **/

import { allWords, wordsSplitChars } from '../../util/constants/regex.constants';
import { whitespaceReplaceWith } from '../whitespaceUtils';

/**
 * Takes 2 strings and returns a number for the number of times the second string exists in the first string; optionally takes third boolean argument to make case sensitive
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { wordSpecificWordCount } from 'stringman-utils';
 * // or
 * const wordSpecificWordCount = require('stringman-utils').wordSpecificWordCount;
 *
 * const insensitive = wordSpecificWordCount('Maybe-this will, trip it up! Will it? We WILL see.', 'will');
 * const sensitive = wordSpecificWordCount('Maybe-this will, trip it up! Will it? We WILL see.', 'will', true);
 * console.log(insensitive); // 3
 * console.log(sensitive); // 1
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} word
 * @returns {number}
 */
export function wordSpecificWordCount(str: string, word: string, strict?: boolean): number {
  const strSplit = str.split(wordsSplitChars);
  return (
    strSplit
      .filter((t) => {
        return allWords.test(t);
      })
      .filter((w) => {
        const num = !isNaN(parseFloat(w));
        if (num) {
          return false;
        } else if (strict) {
          return word === w;
        } else {
          return word.toLowerCase() === w.toLowerCase();
        }
      }).length || 0
  );
}

/**
 * Takes a string and returns the number of words in the string as a number
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { wordUtils } from 'stringman-utils';
 * // or
 * const wordUtils = require('stringman-utils').wordUtils;
 *
 * const count = wordUtils.wordCount('This sentence has 4 words.');
 * const countAgain = wordUtils.wordCount('This sentence has five words.');
 * console.log(count); // 4
 * console.log(countAgain); // 5
 * ```
 *
 * @export
 * @param {string} str
 * @returns {number}
 */
export function wordCount(str: string): number {
  const strSplit = str.split(wordsSplitChars);
  const cleaned = strSplit.filter((t) => {
    return allWords.test(t);
  });
  if (cleaned && cleaned.length) {
    const counted = cleaned.filter((w) => {
      return isNaN(parseFloat(w));
    });
    return counted && counted.length ? counted.length : 0;
  }
  return 0;
}

/**
 * Takes a string, counts all instances of words in the string, and returns an object with each word and the number of occurences
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { wordAllWordCount } from 'stringman-utils';
 * // or
 * const wordAllWordCount = require('stringman-utils').wordAllWordCount;
 *
 * const test = `This is my stupid test.
 *  Its a small test, but it is my test.
 *  `;
 * const lax = wordAllWordCount(test);
 * const strict = wordAllWordCount(test, true);
 * console.log(lax); // {"this": 1, "is": 2, "my": 2, "stupid": 1, "test": 3, "its": 1, "a": 1, "small": 1, "but": 1, "it": 1}
 * console.log(strict); // {"This": 1, "is": 2, "my": 2, "stupid": 1, "test": 3, "Its": 1, "a": 1, "small": 1, "but": 1, "it": 1}
 * ```
 *
 * @export
 * @param {string} str
 * @param {boolean} [strict]
 * @returns {*}
 */
export function wordAllWordCount(str: string, strict?: boolean): any {
  const strSplit = whitespaceReplaceWith(
    str,
    { tabs: true, breaks: true, multiSpace: true },
    ' '
  ).split(wordsSplitChars);
  if (strSplit && strSplit.length) {
    const cleaned = strSplit.filter((t) => {
      return allWords.test(t);
    });
    if (cleaned && cleaned.length) {
      return cleaned.reduce((acc: any, word: string) => {
        if (strict) {
          if (acc[word]) {
            acc[word] += 1;
          } else {
            acc[word] = 1;
          }
        } else {
          const lower = word.toLowerCase();
          if (acc[lower]) {
            acc[lower] += 1;
          } else {
            acc[lower] = 1;
          }
        }
        return acc;
      }, {});
    } else {
      return {};
    }
  } else {
    return {};
  }
}

/**
 * Takes a string and returns with the first character capitalized
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { wordCapitalize } from 'stringman-utils';
 * // or
 * const wordCapitalize = require('stringman-utils').wordCapitalize;
 *
 * const cap = wordCapitalize('test');
 * const failure = wordCapitalize(true);
 * console.log(test); // 'Test'
 * console.log(failure); // null
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string | null)}
 */
export function wordCapitalize(str: string): string | null {
  if (typeof str !== 'string') {
    return null;
  }
  return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * Takes a string, the string to replace, the string to replace the other string with, and the optional boolean argument for case sensitivity and returns the
 * original string with the 2nd argument replaced with the 3rd. Can return null if a failure happens along the way.
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { wordReplaceWord } from 'stringman-utils';
 * // or
 * const wordReplaceWord = require('stringman-utils').wordReplaceWord;
 *
 * const test = 'We hope this works because we put some serious work into this and we are invested.';
 * const sens = wordReplaceWord(test, 'we', 'they', true);
 * const ins = wordReplaceWord(test, 'we', 'they');
 * const failure = wordReplaceWord(test, 5, 'they');
 * console.log(sens); // 'They hope this works because they put some serious work into this and they are invested.'
 * console.log(ins); // 'they hope this works because they put some serious work into this and they are invested.'
 * console.log(failure); // null
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} word
 * @param {string} replaceWith
 * @param {boolean} [cases]
 * @returns {(string | null)}
 */
export function wordReplaceWord(
  str: string,
  word: string,
  replaceWith: string,
  cases?: boolean
): string | null {
  if (typeof str !== 'string' || typeof word !== 'string' || typeof replaceWith !== 'string') {
    return null;
  }
  if (cases) {
    const regExp = new RegExp(word, 'gm');
    const capWord = wordCapitalize(word);
    const capRw = wordCapitalize(replaceWith);
    const capExp = capWord ? new RegExp(capWord, 'gm') : null;
    const lc = str.replace(regExp, replaceWith);
    return capExp && capRw ? lc.replace(capExp, capRw) : null;
  } else {
    const insExp = new RegExp(word, 'gim');
    return str.replace(insExp, replaceWith);
  }
}

/**
 * Takes word count, image count, boolean for valueOnly which will return only the minutes if set to 'true',
 * and optionally a words per minute value (defaults to 270) and returns estimated reading time
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 *  * Basic usage example:
 * ```js
 * import { wordReadingTime } from 'stringman-utils';
 * // or
 * const wordReadingTime = require('stringman-utils').wordReadingTime;
 *
 * const wordCount = 1200;
 * const imageCount = 5;
 * const readTime = wordReadingTime(wordCount, imageCount);
 * const readTimeValueOnly = wordReadingTime(wordCount, imageCount, true);
 * const readTimeSpeedReader = wordReadingTime(wordCount, imageCount, false, 400);
 * console.log(readTime); // '5 min read'
 * console.log(readTimeValueOnly); // 5
 * console.log(readTimeSpeedReader); // 4
 * ```
 *
 * @export
 * @param {number} wordCount
 * @param {number} imageCount
 * @param {boolean} [valueOnly=false]
 * @param {number} [wordsPerMinute=270]
 * @return {string | number}
 */
export function wordReadingTime(
  wCount: number,
  imageCount: number,
  valueOnly = false,
  wordsPerMinute = 270
): string | number {
  if (valueOnly) {
    return Math.round(wCount / wordsPerMinute) + imageCount * 0.2;
  }
  return `${Math.round(wCount / wordsPerMinute) + imageCount * 0.2} min read`;
}
