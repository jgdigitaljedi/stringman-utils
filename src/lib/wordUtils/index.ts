import {
  wordAllWordCount,
  wordCapitalize,
  wordCount,
  wordReadingTime,
  wordReplaceWord,
  wordSpecificWordCount
} from './words.utils';

export {
  wordAllWordCount,
  wordCapitalize,
  wordCount,
  wordReadingTime,
  wordReplaceWord,
  wordSpecificWordCount
};
