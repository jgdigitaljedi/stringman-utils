// precisionMath.utils.ts
/**
 * * This is a collection of helper functions that can be used for accurate
 * math operations with much higher floating point precision.
 *
 * General naming convention:
 * - camel cased
 * - `precisionMath` prefix
 * - `precisionMath<desired operation>`
 * - `precisionMathAdd`
 *
 * @module precisionMath utils
 **/

import Big from 'big.js';
import { ONE_NUM_ERROR, TWO_NUM_ERROR } from '../../util/errors';
import { isNumberString } from '../../util/validateArgs';

Big.DP = 30;
Big.NE = -30;
Big.PE = 30;

/**
 * Takes to numbers (number or string format), an optional arg for string return (longer precision allowed)
 * and returns the sum (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMacthAdd } from 'stringman-utils';
 * // or
 * const precisionMacthAdd = require('stringman-utils').precisionMacthAdd;
 *
 * const sum = precisionMacthAdd(0.1, 0.2);
 * console.log(sum); // 0.3
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathAdd(
  num1: number | string,
  num2: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  if (isNumberString(num1) && isNumberString(num2)) {
    return bigString ? Big(num1).plus(num2).toString() : Big(num1).plus(num2).toNumber();
  } else {
    throw new Error(`${caller ? caller + ': ' : ''}${TWO_NUM_ERROR}`);
  }
}

/**
 * Takes 2 numbers (number or string), optional arg for string return, and returns difference
 * (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMacthSubtract } from 'stringman-utils';
 * // or
 * const precisionMacthSubtract = require('stringman-utils').precisionMacthSubtract;
 *
 * const difference = precisionMacthSubtract(0.3, 0.1);
 * console.log('difference); // 0.2
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathSubtract(
  num1: number | string,
  num2: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  if (isNumberString(num1) && isNumberString(num2)) {
    return bigString ? Big(num1).minus(num2).toString() : Big(num1).minus(num2).toNumber();
  } else {
    throw new Error(`${caller ? caller + ': ' : ''}${TWO_NUM_ERROR}`);
  }
}

/**
 * Takes 2 numbers (number or string), optional arg for string return, and returns product
 * (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathMultiply } from 'stringman-utils';
 * // or
 * const precisionMathMultiply = require('stringman-utils').precisionMathMultiply;
 *
 * const product = precisionMathMultiply(0.6, 3);
 * console.log(product); // 1.8
 * ```
 *
 * @export
 * @param {(number | string | Big)} num1
 * @param {(number | string | Big)} num2
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathMultiply(
  num1: number | string | Big,
  num2: number | string | Big,
  bigString?: boolean,
  caller?: string
): number | string {
  if (isNumberString(num1) && isNumberString(num2)) {
    return bigString ? Big(num1).times(num2).toString() : Big(num1).times(num2).toNumber();
  } else {
    throw new Error(`${caller ? caller + ': ' : ''}${TWO_NUM_ERROR}`);
  }
}

/**
 * Takes 2 numbers (number or string), optional arg for string return, and returns result of first
 * number divided by second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMatchDivide } from 'stringman-utils';
 * // or
 * const precisionMatchDivide = require('stringman-utils').precisionMatchDivide;
 *
 * const divided = precisionMatchDivide(355, 113, true);
 * console.log(divided); // '3.14159292035398230088'
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathDivide(
  num1: number | string,
  num2: number | string,
  bigString?: boolean,
  caller?: string
): number | string {
  if (isNumberString(num1) && isNumberString(num2)) {
    return bigString ? Big(num1).div(num2).toString() : Big(num1).div(num2).toNumber();
  } else {
    throw new Error(`${caller ? caller + ': ' : ''}${TWO_NUM_ERROR}`);
  }
}

/**
 * Takes 2 numbers (number or string), optional arg for string return, returns remainder of
 * first number divided by second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathModulo } from 'stringman-utils';
 * // or
 * const precisionMathModulo = require('stringman-utils').precisionMathModulo;
 *
 * const remainder = precisionMathModulo(1, 0.9);
 * console.log(remainder); // 0.1
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathModulo(
  num1: number | string,
  num2: number | string,
  bigString?: boolean
): number | string {
  if (isNumberString(num1) && isNumberString(num2)) {
    return bigString ? Big(num1).mod(num2).toString() : Big(num1).mod(num2).toNumber();
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string) and returns boolean to represent if first is greater
 * than the second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathGt } from 'stringman-utils';
 * // or
 * const precisionMathGt = require('stringman-utils').precisionMathGt;
 *
 * const greaterThan = precisionMathGt(0.1, 0.3 - 0.2);
 * console.log(greaterThan); // false
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @return {*}  {boolean}
 */
export function precisionMatchGt(num1: number | string, num2: number | string): boolean {
  if (isNumberString(num1) && isNumberString(num2)) {
    return Big(num1).gt(num2);
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string) and returns boolean to represent if first is greater
 * than or equal to the second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathGte } from 'stringman-utils';
 * // or
 * const precisionMathGte = require('stringman-utils').precisionMathGte;
 *
 * const greaterThanEq = precisionMathGte(0.1, 0.3 - 0.2);
 * console.log(greaterThanEq); // true
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @return {*}  {boolean}
 */
export function precisionMathGte(num1: number | string, num2: number | string): boolean {
  if (isNumberString(num1) && isNumberString(num2)) {
    return Big(num1).gte(num2);
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string) and returns boolean to represent if first is less
 * than the second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathLt } from 'stringman-utils';
 * // or
 * const precisionMathLt = require('stringman-utils').precisionMathLt;
 *
 * const lessThan = precisionMathLt(0.1, 0.3 - 0.2);
 * console.log(lessThan); // false
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @return {*}  {boolean}
 */
export function precisionMathLt(num1: number | string, num2: number | string): boolean {
  if (isNumberString(num1) && isNumberString(num2)) {
    return Big(num1).lt(num2);
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string) and returns boolean to represent if first is less
 * than or equal to than the second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathLte } from 'stringman-utils';
 * // or
 * const precisionMathLte = require('stringman-utils').precisionMathLte;
 *
 * const lessThanEq = precisionMathLte(0.1, 0.3 - 0.2);
 * console.log(lessThanEq); // true
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @return {*}  {boolean}
 */
export function precisionMathLte(num1: number | string, num2: number | string): boolean {
  if (isNumberString(num1) && isNumberString(num2)) {
    return Big(num1).lte(num2);
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string) and returns boolean to represent if they are equal
 * (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathEquals } from 'stringman-utils';
 * // or
 * const precisionMathEquals = require('stringman-utils').precisionMathEquals;
 *
 * const eq = precisionMathEquals(0.1, 0.3 - 0.2);
 * console.log(eq); // true
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {(number | string)} num2
 * @return {*}  {boolean}
 */
export function precisionMathEquals(num1: number | string, num2: number | string): boolean {
  if (isNumberString(num1) && isNumberString(num2)) {
    return Big(num1).eq(num2);
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 1 number (number or string) and returns the square root of that number
 * (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathSqrt } from 'stringman-utils';
 * // or
 * const precisionMathSqrt = require('stringman-utils').precisionMathSqrt;
 *
 * const squareRoot = precisionMathSqrt(3, true);
 * console.log(squareRoot); // '1.73205080756887729353'
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathSqrt(num: number | string, bigString?: boolean): number | string {
  if (isNumberString(num)) {
    return bigString ? Big(num).sqrt().toString() : Big(num).sqrt().toNumber();
  } else {
    throw new Error(ONE_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string for first, number only for second), optional arg for string return, and returns
 * first number to the second number's power (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathPower } from 'stringman-utils';
 * // or
 * const precisionMathPower = require('stringman-utils').precisionMathPower;
 *
 * const power = precisionMathPower(0.7, 2);
 * console.log(power); // 0.49
 * ```
 *
 * @export
 * @param {(number | string)} num1
 * @param {number} num2
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathPower(
  num1: number | string,
  num2: number,
  bigString?: boolean
): number | string {
  if (isNumberString(num1) && isNumberString(num2)) {
    return bigString ? Big(num1).pow(num2).toString() : Big(num1).pow(num2).toNumber();
  } else {
    throw new Error(TWO_NUM_ERROR);
  }
}

/**
 * Takes 1 number (number or string), optional arg for string return, and returns the absolute value
 * (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathAbsolute } from 'stringman-utils';
 * // or
 * const precisionMathAbsolute = require('stringman-utils').precisionMathAbsolute;
 *
 * const absolute = precisionMathAbsolute(-0.8);
 * console.log(absolute); // 0.8
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function precisionMathAbsolute(num: number | string, bigString?: boolean): number | string {
  if (isNumberString(num)) {
    return bigString ? Big(num).abs().toString() : Big(num).abs().toNumber();
  } else {
    throw new Error(ONE_NUM_ERROR);
  }
}

/**
 * Takes 2 numbers (number or string, number only for second) and returns returns exponential
 * notation of first number to 10 to the power of the second (with higher precision than raw JS)
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```
 * import { precisionMathExponential } from 'stringman-utils';
 * // or
 * const precisionMathExponential = require('stringman-utils').precisionMathExponential;
 *
 * const exp = precisionMathExponential(45.6);
 * console.log(exp); // '4.56e+1'
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {number} [decimals]
 * @return {*}  {string}
 */
export function precisionMathExponential(num: number | string, decimals?: number): string {
  if (isNumberString(num)) {
    return decimals !== undefined ? Big(num).toExponential(decimals) : Big(num).toExponential();
  } else {
    throw new Error(ONE_NUM_ERROR);
  }
}
