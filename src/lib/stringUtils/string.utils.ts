// string.utils.ts
/**
 * * This is a collection of helper functions that can be used for taking strings
 * and regular expressions and performing various tasks with them.
 * example: swapping a string with another according to matches made by Regexp
 *
 *
 * General naming convention:
 * - camel cased
 * - `string` prefix
 * - `string<desired operation>`
 * - `stringSwap`
 *
 * @module string utils
 **/

/**
 * Takes a string and a RegExp and returns a boolean to see if there was a match
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { stringIsValid } from 'stringman-utils';
 * // or
 * const stringIsValid = require('stringman-utils').stringIsValid;
 * const valid = stringIsValid('word', /\w+/g);
 * console.log(valid); // true
 * ```
 *
 * @export
 * @param {string} str
 * @param {RegExp} exp
 * @return {*}  {boolean}
 */
export function stringIsValid(str: string, exp: RegExp): boolean {
  return typeof str !== 'string' ? false : exp.test(str);
}

/**
 * Takes a string containing something to be replace, a string to replace that something with,
 * and a RegExp to test for and returns the result of the replacement
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { stringSwap, emailRegex } from 'stringman-utils';
 * // or
 * const stringSwap = require('stringman-utils').stringSwap;
 * const swapped = stringSwap('email me at test@test.com', 'none@none.com', emailRegex);
 * console.log(swapped); // 'email me at none@none.com'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} str2
 * @param {RegExp} exp
 * @return {*}  {string}
 */
export function stringSwap(str: string, str2: string, exp: RegExp): string {
  if (str && str2 && exp) {
    return str.replace(exp, str2);
  } else {
    return '';
  }
}

/**
 * Takes a string and a RegExp and retruns the string with the matches from the RegExp removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { stringRemove, emailRegex } from 'stringman-utils';
 * // or
 * const stringRemove = require('stringman-utils').stringRemove;
 * const removed = stringRemove('email me at test@test.com', emailRegex);
 * console.log(removed); // 'email me at'
 * ```
 *
 * @export
 * @param {string} str
 * @param {RegExp} exp
 * @return {*}  {string}
 */
export function stringRemove(str: string, exp: RegExp): string {
  if (typeof str !== 'string') {
    return str;
  }
  return str.replace(exp, '').trim();
}

/**
 * Takes a string and a RegExp to match and returns a string array of matches
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { stringRetrieve, emailRegex } from 'stringman-utils';
 * // or
 * const stringRetrieve = require('stringman-utils').stringRetrieve;
 * const retrieved = stringRetrieve('email me at test@test.com', emailRegex);
 * console.log(retrieved); // 'test@test.com'
 * ```
 *
 * @export
 * @param {string} str
 * @param {RegExp} exp
 * @return {*}  {string[]}
 */
export function stringRetrieve(str: string, exp: RegExp): string[] {
  if (typeof str !== 'string') {
    return [];
  }
  return str.match(exp) || [];
}
