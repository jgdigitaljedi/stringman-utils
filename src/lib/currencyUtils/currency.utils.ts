// currency.utils.ts
/**
 * * This has a helper function to determine if the Intl object is usable in
 * a browser or Node.js environment. It also returns a class that can be used
 * if the Intl object is available that has several public functions for
 * handling currency.
 *
 * General naming convention:
 * - the class is pascal cased, public functions and external function and camel cased
 * - `currency` prefix for external function
 *
 * @module currency utils
 **/

import { LocaleForCurrency } from '../../models/currency.model';
import {
  precisionMathDivide,
  precisionMathMultiply,
  precisionMathPower
} from '../precisionMathUtils';

/**
 * Class extraction of Intl object for easier use
 *
 * **Environment(s)**<br>
 * [ ] server/Node.js<br>
 * [X] client/browser<br>
 * *Note: this can work in Node if you install full-icu*
 *
 * Basic usage example:
 * ```js
 * import { CurrencyUtils } from 'stringman-utils';
 *
 * const cu = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
 * console.log(cu.getCurrencySymbol()); // '$'
 * console.log(cu.getLocaleStr()); // 'en-US'
 * console.log(cu.formatCurrencyDisplay(1234.56)); // '$1,234.56'
 * console.log(cu.majorToMinorUnits(1000)); // 100000
 * console.log(cu.minorToMajorUnits(123456)); // 1234.56
 *
 * // if you want to change locale, spin up another instance
 * const cuVnd = new CurrencyUtils({ language: 'vi', country: 'VN' }, 'VND');
 * console.log(cuVnd.formatCurrencyDisplay(1234)); // '1.234 ₫'
 * ```
 *
 * @export
 * @class CurrencyUtils
 */
export class CurrencyUtils {
  public localeStr: string;
  public intlNumberInstance: Intl.NumberFormat;

  constructor(locale: LocaleForCurrency, currencyCode: string) {
    this.localeStr = this.getLocaleStr(locale);
    this.intlNumberInstance = new Intl.NumberFormat(this.localeStr, {
      style: 'currency',
      currency: currencyCode
    });
  }

  /**
   * Returns a locale string based on arguments given to class constructor when instantiated
   *
   * **Environment(s)**<br>
   * [ ] server/Node.js<br>
   * [X] client/browser<br>
   * *Note: this can work in Node if you install full-icu*
   *
   * Basic usage example:
   * ```js
   * import { CurrencyUtils } from 'stringman-utils';
   *
   * const cu = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
   * console.log(cu.getLocaleStr()); // 'en-US';
   * ```
   *
   * @public
   * @param {LocaleForCurrency} locale
   * @return {*}  {string}
   * @memberof CurrencyUtils
   */
  public getLocaleStr(locale: LocaleForCurrency): string {
    if (locale.country) {
      return `${locale.language}-${locale.country}`;
    }
    return locale.language;
  }

  /**
   * Takes a number and returns it formatted as currency based on arguments given to class constructor when instantiated
   *
   * **Environment(s)**<br>
   * [ ] server/Node.js<br>
   * [X] client/browser<br>
   * *Note: this can work in Node if you install full-icu*
   *
   * Basic usage example:
   * ```js
   * import { CurrencyUtils } from 'stringman-utils';
   *
   * const cu = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
   * console.log(cu.formatCurrencyDisplay(1000)); // '$1,000';
   * ```
   *
   * @public
   * @param {number} amount
   * @return {*}  {string}
   * @memberof CurrencyUtils
   */
  public formatCurrencyDisplay(amount: number): string {
    return this.intlNumberInstance.format(amount);
  }

  /**
   * Returns currency symbol based on arguments given to class constructor when instantiated
   *
   * **Environment(s)**<br>
   * [ ] server/Node.js<br>
   * [X] client/browser<br>
   * *Note: this can work in Node if you install full-icu*
   *
   * Basic usage example:
   * ```js
   * import { CurrencyUtils } from 'stringman-utils';
   *
   * const cu = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
   * console.log(cu.getCurrencySymbol()); // '$';
   * ```
   *
   * @public
   * @return {*}  {string}
   * @memberof CurrencyUtils
   */
  public getCurrencySymbol(): string {
    try {
      const currencyParts = this.intlNumberInstance.formatToParts(0);
      const symbolObj = currencyParts.filter(
        (part: Intl.NumberFormatPart) => part.type === 'currency'
      );
      return symbolObj[0].value;
    } catch (err) {
      throw new Error(`Could not parse currency for locale ${this.localeStr}`);
    }
  }

  /**
   * Takes a number in major units format and returns it in minor format basedon arguments
   * given to class constructor when instantiated
   *
   * **Environment(s)**<br>
   * [X] server/Node.js<br>
   * [X] client/browser
   *
   * Basic usage example:
   * ```js
   * import { CurrencyUtils } from 'stringman-utils';
   *
   * const cu = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
   * console.log(cu.majorToMinorUnits(1000)); // 100000;
   * ```
   *
   * @public
   * @param {number} amount
   * @return {*}  {(number | string)}
   * @memberof CurrencyUtils
   */
  public majorToMinorUnits(amount: number): number | string {
    const parts = this.intlNumberInstance.formatToParts(amount);
    const majorDecimals = parts.find((p) => p.type === 'fraction')?.value?.length;
    return majorDecimals
      ? precisionMathMultiply(amount, precisionMathPower(10, majorDecimals))
      : amount;
  }

  /**
   * Takes a number in minor units format and returns it in major format basedon arguments
   * given to class constructor when instantiated
   *
   * **Environment(s)**<br>
   * [X] server/Node.js<br>
   * [X] client/browser
   *
   * Basic usage example:
   * ```js
   * import { CurrencyUtils } from 'stringman-utils';
   *
   * const cu = new CurrencyUtils({ language: 'en', country: 'US' }, 'USD');
   * console.log(cu.minorToMajorUnits(1000, true)); // '10.00';
   * ```
   *
   * @public
   * @param {number} amount
   * @param {boolean} [stringReturn]
   * @return {*}  {(number | string)}
   * @memberof CurrencyUtils
   */
  public minorToMajorUnits(amount: number, stringReturn?: boolean): number | string {
    const parts = this.intlNumberInstance.formatToParts(amount);
    const majorDecimals = parts.find((p) => p.type === 'fraction')?.value?.length;

    if (stringReturn) {
      return Number(
        precisionMathDivide(amount, precisionMathPower(10, majorDecimals || 0))
      ).toFixed(majorDecimals || 0);
    } else {
      return precisionMathDivide(amount, precisionMathPower(10, majorDecimals || 0));
    }
  }
}

/**
 * Checks if your current environment can use Intl (client side browser, node.js, etc)
 *
 * **Environment(s)**<br>
 * [ ] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { currencySupportsIntl } from 'stringman-utils';
 * // or
 * const currencySupportsIntl = require('stringman-utils').currencySupportsIntl;
 *
 * console.log(currencySupportsIntl(true)) // true because I have full-icu, but this would query the browser if client side
 * console.log(currencySupportsIntl()) // true because I have full-icu, but this would query the browser if client side
 * ```
 *
 * @export
 * @param {boolean} [doFullCheck]
 * @return {*}  {boolean}
 */
export const currencySupportsIntl = (doFullCheck?: boolean): boolean => {
  if (!doFullCheck) {
    return typeof Intl === 'object';
  }
  try {
    const january = new Date(9e8);
    const spanish = new Intl.DateTimeFormat('es', { month: 'long' });
    return spanish.format(january) === 'enero';
  } catch (err) {
    return false;
  }
};
