// parens.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various
 * operations regarding parenthesis to the passed strings
 * example: removing what's inside parenthesis from a string
 *
 *
 * General naming convention:
 * - camel cased
 * - `parens` prefix
 * - `parens<current operation>`
 * - `parensRemove`
 *
 * @module parenthesis utils
 **/

import { parens, strInsideParens } from '../../util/constants/regex.constants';
import { stringRemove, stringSwap } from '../stringUtils';

/**
 * Takes string and returns string with parenthesis and contain between parenthesis removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { parensRemove } from 'stringman-utils';
 * // or
 * const parensRemove = require('stringman-utils').parensRemove;
 *
 * const noParens = parensRemove('this will come back (and this will be removed)');
 * console.log(noParens); // 'this will come back'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function parensRemove(str: string): string {
  return stringRemove(str, parens);
}

/**
 * Takes string and returns array of strings containing what was inside any number of sets of parenthesis
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { parensRetrieve } from 'stringman-utils';
 * // or
 * const parensRetrieve = require('stringman-utils').parensRetrieve;
 *
 * const fromInside = parensRetrieve('this is ignored (and this is returned)');
 * console.log(fromInside); // 'and this is returned'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function parensRetrieve(str: string): string[] {
  const matched = str.match(strInsideParens);
  return matched ? matched.map((m) => m.replace('(', '').replace(')', '')) : [];
}

/**
 * Takes 2 strings, swaps anything in parenthesis INCLUDING THE PARENTHESIS from the first string with the second string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { parensSwap } from 'stringman-utils';
 * // or
 * const parensSwap = require('stringman-utils').parensSwap;
 *
 * const noParens = parensSwap('this has (stuff) in parens', 'things');
 * const withParens = parensSwap('this has (other stuff) in parens too', '(more things)');
 * console.log(noParens); // 'this has things in parens'
 * console.log(withParens); // 'this has (more things) in parens too'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} other
 * @returns {string}
 */
export function parensSwap(str: string, other: string): string {
  return stringSwap(str, other, strInsideParens);
}
