// speed.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting speed
 * values from one unit to another unit.
 * example: km/h to mph
 *
 *
 * General naming convention:
 * - camel cased
 * - `speed` prefix
 * - `speed<current unit>To<desired unit>`
 * - `speedKmhToMph`
 * - units shorthand
 *   - `kmh` = kilometers per hour (km/h)
 *   - `mph` = miles per hour
 *   - `kn` = knots
 *   - `fts` = feet per second (ft/s)
 *   - `ms` = meters per second (m/s)
 *
 * @module speed utils
 **/

import { SpeedPercentUnits } from '../../models/speed.model';
import {
  kmphInFtPerSec,
  kmphInKn,
  kmphInMph,
  knotsInFtPerSec,
  mPerSecInFtPerSec,
  kmhInMs,
  mPerSecInKn,
  mPerSecInMph,
  mphInFtPerSec,
  mphInKn,
  ftsInSpeedOfLight,
  ftsInSpeedOfSound,
  msInSpeedOfLight,
  kmphInSpeedOfLight,
  mphInSpeedOfLight,
  knInSpeedOfLight,
  milesPsInSpeedOfLight,
  msInSpeedOfSound,
  kmphInSpeedOfSound,
  mphInSpeedOfSound,
  knotsInSpeedOfSound,
  milesPsInSpeedOfSound
} from '../../util/constants/speed.constants';
import { INVALID_UNIT } from '../../util/errors';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

function getAmountInSol(unit: SpeedPercentUnits, caller: string): number {
  switch (unit) {
    case 'ft/s':
      return ftsInSpeedOfLight;
    case 'm/s':
      return msInSpeedOfLight;
    case 'km/h':
      return kmphInSpeedOfLight;
    case 'mph':
      return mphInSpeedOfLight;
    case 'kn':
      return knInSpeedOfLight;
    case 'miles/s':
      return milesPsInSpeedOfLight;
    default:
      throw new Error(`${caller}: ${INVALID_UNIT}`);
  }
}

function getAmountInSos(unit: SpeedPercentUnits, caller: string): number {
  switch (unit) {
    case 'ft/s':
      return ftsInSpeedOfSound;
    case 'm/s':
      return msInSpeedOfSound;
    case 'km/h':
      return kmphInSpeedOfSound;
    case 'mph':
      return mphInSpeedOfSound;
    case 'kn':
      return knotsInSpeedOfSound;
    case 'miles/s':
      return milesPsInSpeedOfSound;
    default:
      throw new Error(`${caller}: ${INVALID_UNIT}`);
  }
}

/**
 * Takes speed in km/h as number or string, optional arg for string return, and returns speed in mph
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKmhToMph } from 'stringman-utils';
 * // or
 * const speedKmhToMph = require('stringman-utils').speedKmhToMph;
 *
 * console.log(speedKmhToMph(1.609344)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKmhToMph(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, kmphInMph, bigString, 'speedKmhToMph');
}

/**
 * Takes speed in mph as number or string, optional arg for string return, and returns speed in km/h
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMphToKmh } from 'stringman-utils';
 * // or
 * const speedMphToKmh = require('stringman-utils').speedMphToKmh;
 *
 * console.log(speedMphToKmh(1)); // 1.609344
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMphToKmh(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, kmphInMph, bigString, 'speedMphToKmh');
}

/**
 * Takes speed in mph as number or string, optional arg for string return, and returns speed in knots
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMphToKn } from 'stringman-utils';
 * // or
 * const speedMphToKn = require('stringman-utils').speedMphToKn;
 *
 * console.log(speedMphToKn(1.1507794480235)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMphToKn(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, mphInKn, bigString, 'speedMphToKn');
}

/**
 * Takes speed in knots as number or string, optional arg for string return, and returns speed in mph
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKnToMph } from 'stringman-utils';
 * // or
 * const speedKnToMph = require('stringman-utils').speedKnToMph;
 *
 * console.log(speedKnToMph(1)); // 1.1507794480235
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKnToMph(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, mphInKn, bigString, 'speedKnToMph');
}

/**
 * Takes speed in km/h as number or string, optional arg for string return, and returns speed in knots
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKmhToKn } from 'stringman-utils';
 * // or
 * const speedKmhToKn = require('stringman-utils').speedKmhToKn;
 *
 * console.log(speedKmhToKn(1.8519995164223486)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKmhToKn(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, kmphInKn, bigString, 'speedKmhToKn');
}

/**
 * Takes speed in knots as number or string, optional arg for string return, and returns speed in km/h
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKnToKmh } from 'stringman-utils';
 * // or
 * const speedKnToKmh = require('stringman-utils').speedKnToKmh;
 *
 * console.log(speedKnToKmh(1)); // 1.8519995164223486
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKnToKmh(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, kmphInKn, bigString, 'speedKnToKmh');
}

/**
 * Takes speed in ft/s as number or string, optional arg for string return, and returns speed in mph
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedFtsToMph } from 'stringman-utils';
 * // or
 * const speedFtsToMph = require('stringman-utils').speedFtsToMph;
 *
 * console.log(speedFtsToMph(1)); // 0.6818181802686
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedFtsToMph(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, mphInFtPerSec, bigString, 'speedFtsToMph');
}

/**
 * Takes speed in mph as number or string, optional arg for string return, and returns speed in ft/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMphToFts } from 'stringman-utils';
 * // or
 * const speedMphToFts = require('stringman-utils').speedMphToFts;
 *
 * console.log(speedMphToFts(0.6818181802686)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMphToFts(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, mphInFtPerSec, bigString, 'speedMphToFts');
}

/**
 * Takes speed in km/h as number or string, optional arg for string return, and returns speed in ft/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKmhToFts } from 'stringman-utils';
 * // or
 * const speedKmhToFts = require('stringman-utils').speedKmhToFts;
 *
 * console.log(speedKmhToFts(1.09728)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKmhToFts(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, kmphInFtPerSec, bigString, 'speedKmhToFts');
}

/**
 * Takes speed in ft/s as number or string, optional arg for string return, and returns speed in km/h
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedFtsToKmh } from 'stringman-utils';
 * // or
 * const speedFtsToKmh = require('stringman-utils').speedFtsToKmh;
 *
 * console.log(speedFtsToKmh(1)); // 1.09728
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedFtsToKmh(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, kmphInFtPerSec, bigString, 'speedFtsToKmh');
}

/**
 * Takes speed in knots as number or string, optional arg for string return, and returns speed in ft/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKnToFts } from 'stringman-utils';
 * // or
 * const speedKnToFts = require('stringman-utils').speedKnToFts;
 *
 * console.log(speedKnToFts(1)); // 0.5924838012959
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKnToFts(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, knotsInFtPerSec, bigString, 'speedKnToFts');
}

/**
 * Takes speed in ft/s as number or string, optional arg for string return, and returns speed in knots
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedFtsToKn } from 'stringman-utils';
 * // or
 * const speedFtsToKn = require('stringman-utils').speedFtsToKn;
 *
 * console.log(speedFtsToKn(0.5924838012959)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedFtsToKn(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, knotsInFtPerSec, bigString, 'speedFtsToKn');
}

/**
 * Takes speed in meters/s as number or string, optional arg for string return, and returns speed in ft/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMsToFts } from 'stringman-utils';
 * // or
 * const speedMsToFts = require('stringman-utils').speedMsToFts;
 *
 * console.log(speedMsToFts(0.3048)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMsToFts(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, mPerSecInFtPerSec, bigString, 'speedMsToFts');
}

/**
 * Takes speed in ft/s as number or string, optional arg for string return, and returns speed in meters/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedFtsToMs } from 'stringman-utils';
 * // or
 * const speedFtsToMs = require('stringman-utils').speedFtsToMs;
 *
 * console.log(speedFtsToMs(1)); // 0.3048
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedFtsToMs(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, mPerSecInFtPerSec, bigString, 'speedFtsToMs');
}

/**
 * Takes speed in mph as number or string, optional arg for string return, and returns speed in meters/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMphToMs } from 'stringman-utils';
 * // or
 * const speedMphToMs = require('stringman-utils').speedMphToMs;
 *
 * console.log(speedMphToMs(1)); // 0.44704
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMphToMs(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, mPerSecInMph, bigString, 'speedMphToMs');
}

/**
 * Takes speed in meters/s as number or string, optional arg for string return, and returns speed in mph
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMsToMph } from 'stringman-utils';
 * // or
 * const speedMsToMph = require('stringman-utils').speedMsToMph;
 *
 * console.log(speedMsToMph(0.44704)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMsToMph(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, mPerSecInMph, bigString, 'speedMsToMph');
}

/**
 * Takes speed in km/h as number or string, optional arg for string return, and returns speed in meters/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKmhToMs } from 'stringman-utils';
 * // or
 * const speedKmhToMs = require('stringman-utils').speedKmhToMs;
 *
 * console.log(speedKmhToMs(3.6)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKmhToMs(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, kmhInMs, bigString, 'speedKmhToMs');
}

/**
 * Takes speed in meters/s as number or string, optional arg for string return, and returns speed in km/h
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMsToKmh } from 'stringman-utils';
 * // or
 * const speedMsToKmh = require('stringman-utils').speedMsToKmh;
 *
 * console.log(speedMsToKmh(1)); // 3.6
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMsToKmh(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, kmhInMs, bigString, 'speedMsToKmh');
}

/**
 * Takes speed in knots as number or string, optional arg for string return, and returns speed in meters/s
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedKnToMs } from 'stringman-utils';
 * // or
 * const speedKnToMs = require('stringman-utils').speedKnToMs;
 *
 * console.log(speedKnToMs(1)); // 0.51444444444444
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedKnToMs(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, mPerSecInKn, bigString, 'speedKnToMs');
}

/**
 * Takes speed in meters/s as number or string, optional arg for string return, and returns speed in knots
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedMsToKn } from 'stringman-utils';
 * // or
 * const speedMsToKn = require('stringman-utils').speedMsToKn;
 *
 * console.log(speedMsToKn(0.51444444444444)); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedMsToKn(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, mPerSecInKn, bigString, 'speedMsToKn');
}

/**
 * Takes a value representing speed as number or string, string argument for unit type, optional boolean arg for
 * string return (more rpecision), and returns the percentage of the speed of light
 * Valid 'unit' argument values:
 * - 'mph' = miles per hour
 * - 'ft/s' = feet per second
 * - 'm/s' = meters per second
 * - 'km/h' = kilometers per hour
 * - 'kn' = knots
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedPercentSpeedOfLight } from 'stringman-utils';
 * // or
 * const speedPercentSpeedOfLight = require('stringman-utils').speedPercentSpeedOfLight;
 *
 * console.log(speedPercentSpeedOfLight(983571056.43045, 'ft/s')); // 100
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {SpeedPercentUnits} unit
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedPercentSpeedOfLight(
  num: number | string,
  unit: SpeedPercentUnits,
  bigString?: boolean
): number | string {
  const ratio = getAmountInSol(unit, 'speedPercentSpeedOfLight');
  const dec = precisionMathDivide(num, ratio, bigString, 'speedPercentSpeedOfLight');
  return precisionMathMultiply(dec, 100, bigString, 'speedPercentSpeedOfLight');
}

/**
 * Takes a value representing speed as number or string, string argument for unit type, optional boolean arg for
 * string return (more rpecision), and returns the mach number
 * NOTE THAT SPEED OF SOUND IS RELATIVE TO TEMPERATURE AND ELEVATION SO ONLY RETURNS BASED ON 68F AT SEA LEVEL
 * Valid 'unit' argument values:
 * - 'mph' = miles per hour
 * - 'ft/s' = feet per second
 * - 'm/s' = meters per second
 * - 'km/h' = kilometers per hour
 * - 'kn' = knots
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { speedToMach } from 'stringman-utils';
 * // or
 * const speedToMach = require('stringman-utils').speedToMach;
 *
 * console.log(speedToMach(1125, 'ft/s')); // 1
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {SpeedPercentUnits} unit
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function speedToMach(
  num: number | string,
  unit: SpeedPercentUnits,
  bigString?: boolean
): number | string {
  const ratio = getAmountInSos(unit, 'speedToMach');
  return precisionMathDivide(num, ratio, bigString, 'speedToMach');
}
