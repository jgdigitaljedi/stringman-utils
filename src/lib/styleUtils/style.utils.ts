// style.utils.ts
/**
 * * This is a collection of helper functions that can be used for various css
 * related operations.
 * example: getting an element's font size
 *
 *
 * General naming convention:
 * - camel cased
 * - `style` prefix
 * - `style<desired operation>`
 * - `styleGetElementFontSize`
 *
 * @module style utils
 **/

import Big from 'big.js';

/**
 * Take an element and returns the font size of the element in pixels
 *
 * **Environment(s)**<br>
 * [ ] server/Node.js<br>
 * [X] client/browser<br>
 * *Note: can be run in Node with a DOM library*
 *
 * Basic usage example:
 * ```js
 * import { styleGetElementFontSize } from 'stringman-utils';
 * // or
 * const styleGetElementFontSize = require('stringman-utils').styleGetElementFontSize;
 * const ele = document.createElement('div);
 * const px = styleGetElementFontSize(ele);
 * console.log(px); // 16
 * ```
 *
 * @export
 * @param {HTMLElement} context
 * @return {*}  {number}
 */
export function styleGetElementFontSize(context: HTMLElement): number {
  return parseFloat(getComputedStyle(context).fontSize);
}

/**
 * Takes a number of rem, and optional arg for an element to be passed, and returns that value converted to pixels.
 * If no element is passed, it attempts to use document.documentElement as its baseline
 *
 * **Environment(s)**<br>
 * [ ] server/Node.js<br>
 * [X] client/browser<br>
 * *Note: can be run in Node with a DOM library*
 *
 * Basic usage example:
 * ```js
 * import { styleConvertRemToPixels } from 'stringman-utils';
 * // or
 * const styleConvertRemToPixels = require('stringman-utils').styleConvertRemToPixels;
 * const ele = document.createElement('div);
 * const px = styleConvertRemToPixels(2, ele);
 * console.log(px); // 32
 * ```
 *
 * @export
 * @param {number} rem
 * @param {HTMLElement} [context]
 * @return {*}  {number}
 */
export function styleConvertRemToPixels(rem: number, context?: HTMLElement): number {
  return new Big(rem)
    .times(styleGetElementFontSize(context || document.documentElement))
    .toNumber();
}
