import { styleGetElementFontSize, styleConvertRemToPixels } from './style.utils';

export { styleGetElementFontSize, styleConvertRemToPixels };
