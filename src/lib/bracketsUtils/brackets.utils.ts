// brackets.utils.ts
/**
 * * This is a collection of helper functions that can be used to perform various
 * operations regarding square brackets to the passed strings
 * example: removing what's inside brackets from a string
 *
 *
 * General naming convention:
 * - camel cased
 * - `brackets` prefix
 * - `brackets<current operation>`
 * - `bracketsRemove`
 *
 * @module brackets utils
 **/

import { brackets, strInsideBrackets } from '../../util/constants/regex.constants';
import { stringRemove, stringSwap } from '../stringUtils';

/**
 * Takes string and returns string with brackets and contain between brackets removed
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { bracketsRemove } from 'stringman-utils';
 * // or
 * const bracketsRemove = require('stringman-utils').bracketsRemove;
 *
 * const nobrackets = bracketsRemove('this will come back [and this will be removed]');
 * console.log(nobrackets); // 'this will come back'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {string}
 */
export function bracketsRemove(str: string): string {
  return stringRemove(str, brackets);
}

/**
 * Takes string and returns array of strings containing what was inside any number of sets of brackets
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { bracketsRetrieve } from 'stringman-utils';
 * // or
 * const bracketsRetrieve = require('stringman-utils').bracketsRetrieve;
 *
 * const fromInside = bracketsRetrieve('this is ignored [and this is returned]');
 * console.log(fromInside); // 'and this is returned'
 * ```
 *
 * @export
 * @param {string} str
 * @returns {(string[])}
 */
export function bracketsRetrieve(str: string): string[] {
  const matched = str.match(strInsideBrackets);
  return matched ? matched.map((m) => m.replace('[', '').replace(']', '')) : [];
}

/**
 * Takes 2 strings, swaps anything in brackets INCLUDING THE BRACKETS from the first string with the second string
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 * ```js
 * import { bracketsSwap } from 'stringman-utils';
 * // or
 * const bracketsSwap = require('stringman-utils').bracketsSwap;
 *
 * const noBrackets = bracketsSwap('this has [stuff] in brackets', 'things');
 * const withBrackets = bracketsSwap('this has [other stuff] in brackets too', '[more things]');
 * console.log(nobrackets); // 'this has things in brackets'
 * console.log(withbrackets); // 'this has [more things] in brackets too'
 * ```
 *
 * @export
 * @param {string} str
 * @param {string} other
 * @returns {string}
 */
export function bracketsSwap(str: string, other: string): string {
  return stringSwap(str, other, strInsideBrackets);
}
