// distance.utils.ts
/**
 * * This is a collection of helper functions that can be used for converting distance values to
 * different units. For example, converting miles to kilometers.
 *
 * General naming convention:
 * - camel cased
 * - `distance` prefix
 * - `distance<current value unit>To<desired value unit>`
 * - `distanceMetersToKm`
 *
 * @module distance utils
 **/

import { metricStep } from '../../util/constants/common.constants';

import {
  feetInYard,
  feetInMeter,
  feetInMile,
  yardsInMile,
  inchesInCm,
  inchesInMeter,
  inchesInFoot,
  metersInMile,
  intlNauticalMileToMeters,
  intlNauticalMileToMile,
  uKNauticalMileToMeters,
  uKNauticalMileToMiles
} from '../../util/constants/distance.constants';
import { precisionMathDivide, precisionMathMultiply } from '../precisionMathUtils';

/**
 * Takes distance in meters and returns conversion to kilometers
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMetersToKm } from 'stringman-utils';
 * // or
 * const distanceMetersToKm = require('stringman-utils').distanceMetersToKm;
 * const mToKm = distanceMetersToKm(1000);
 * console.log(mToKm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMetersToKm(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, metricStep, bigString, 'distanceMetersToKm');
}

/**
 * Takes distance in kilometers and returns conversion to meters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceKmToMeters } from 'stringman-utils';
 * // or
 * const distanceKmToMeters = require('stringman-utils').distanceKmToMeters;
 * const kmToM = distanceKmToMeters(1);
 * console.log(kmToM); // 1000;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceKmToMeters(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, metricStep, bigString, 'distanceKmToMeters');
}

/**
 * Takes distance in feet and returns conversion to yards
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToYards } from 'stringman-utils';
 * // or
 * const distanceFeetToYards = require('stringman-utils').distanceFeetToYards;
 * const fToY = distanceFeetToYards(3);
 * console.log(fToY); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToYards(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, feetInYard, bigString, 'distanceFeetToYards');
}

/**
 * Takes distance in yards and returns conversion to feet
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceYardsToFeet } from 'stringman-utils';
 * // or
 * const distanceYardsToFeet = require('stringman-utils').distanceYardsToFeet;
 * const yToF = distanceYardsToFeet(1);
 * console.log(yToF); // 3;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceYardsToFeet(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, feetInYard, bigString, 'distanceYardsToFeet');
}

/**
 * Takes distance in feet and returns conversion to meters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToMeters } from 'stringman-utils';
 * // or
 * const distanceFeetToMeters = require('stringman-utils').distanceFeetToMeters;
 * const fToM = distanceFeetToMeters(3.28084);
 * console.log(mToKm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToMeters(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, feetInMeter, bigString, 'distanceFeetToMeters');
}

/**
 * Takes distance in meters and returns conversion to feet
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMetersToFeet } from 'stringman-utils';
 * // or
 * const distanceMetersToFeet = require('stringman-utils').distanceMetersToFeet;
 * const mToF = distanceMetersToFeet(3.28084);
 * console.log(mToF); // 0.30479999;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMetersToFeet(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, feetInMeter, bigString, 'distanceMetersToFeet');
}

/**
 * Takes distance in meters and returns conversion to miles
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMetersToMiles } from 'stringman-utils';
 * // or
 * const distanceMetersToMiles = require('stringman-utils').distanceMetersToMiles;
 * const mToMi = distanceMetersToMiles(1609.344);
 * console.log(mToMi); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMetersToMiles(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, metersInMile, bigString, 'distanceMetersToMiles');
}

/**
 * Takes distance in miles and returns conversion to meters
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMilesToMeters } from 'stringman-utils';
 * // or
 * const distanceMilesToMeters = require('stringman-utils').distanceMilesToMeters;
 * const miToM = distanceMilesToMeters(1);
 * console.log(mToMi); // 1609.344;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMilesToMeters(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, metersInMile, bigString, 'distanceMilesToMeters');
}

/**
 * Takes distance in feet and returns conversion to kilometers
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToKm } from 'stringman-utils';
 * // or
 * const distanceFeetToKm = require('stringman-utils').distanceFeetToKm;
 * const fToKm = distanceFeetToKm(3280.84);
 * console.log(fToKm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToKm(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, feetInMeter * metricStep, bigString, 'distanceFeetToKm');
}

/**
 * Takes distance in kilometers and returns conversion to feet
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceKmToFeet } from 'stringman-utils';
 * // or
 * const distanceKmToFeet = require('stringman-utils').distanceKmToFeet;
 * const kmToF = distanceKmToFeet(1);
 * console.log(kmToF); // 3280.84;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceKmToFeet(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, feetInMeter * metricStep, bigString, 'distanceKmToFeet');
}

/**
 * Takes distance in inches and returns conversion to feet
 *
 * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceInchesToFeet } from 'stringman-utils';
 * // or
 * const distanceInchesToFeet = require('stringman-utils').distanceInchesToFeet;
 * const inToF = distanceInchesToFeet(12);
 * console.log(inToF); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceInchesToFeet(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, inchesInFoot, bigString, 'distanceInchesToFeet');
}

/**
 * Takes distance in feet and returns conversion to inches
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToInches } from 'stringman-utils';
 * // or
 * const distanceFeetToInches = require('stringman-utils').distanceFeetToInches;
 * const fToIn = distanceFeetToInches(1);
 * console.log(fToIn); // 12;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToInches(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, inchesInFoot, bigString, 'distanceFeetToInches');
}

/**
 * Takes distance in inches and returns conversion to meters
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceInchesToMeters } from 'stringman-utils';
 * // or
 * const distanceInchesToMeters = require('stringman-utils').distanceInchesToMeters;
 * const inToM = distanceInchesToMeters(39.3700787401);
 * console.log(inToM); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceInchesToMeters(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, inchesInMeter, bigString, 'distanceInchesToMeters');
}

/**
 * Takes distance in meters and returns conversion to inches
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMetersToInches } from 'stringman-utils';
 * // or
 * const distanceMetersToInches = require('stringman-utils').distanceMetersToInches;
 * const mToIn = distanceMetersToInches(1);
 * console.log(mToIn); // 39.3700787401;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMetersToInches(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, inchesInMeter, bigString, 'distanceMetersToInches');
}

/**
 * Takes distance in centimeters and returns conversion to inches
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceCmToInches } from 'stringman-utils';
 * // or
 * const distanceCmToInches = require('stringman-utils').distanceCmToInches;
 * const cmToIn = distanceCmToInches(1);
 * console.log(cmToIn); // 0.393700787401;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceCmToInches(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, inchesInCm, bigString, 'distanceCmToInches');
}

/**
 * Takes distance in inches and returns conversion to centimeters
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceInchesToCm } from 'stringman-utils';
 * // or
 * const distanceInchesToCm = require('stringman-utils').distanceInchesToCm;
 * const inToCm = distanceInchesToCm(1);
 * console.log(inToCm); // 2.540000000003708;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceInchesToCm(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, inchesInCm, bigString, 'distanceInchesToCm');
}

/**
 * Takes distance in yards and returns conversion to miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceYardsToMiles } from 'stringman-utils';
 * // or
 * const distanceYardsToMiles = require('stringman-utils').distanceYardsToMiles;
 * const yToMi = distanceYardsToMiles(1760);
 * console.log(yToMi); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceYardsToMiles(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, yardsInMile, bigString, 'distanceYardsToMiles');
}

/**
 * Takes distance in miles and returns conversion to yards
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMilesToYards } from 'stringman-utils';
 * // or
 * const distanceMilesToYards = require('stringman-utils').distanceMilesToYards;
 * const miToY = distanceMilesToYards(1);
 * console.log(miToY); // 1760;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMilesToYards(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, yardsInMile, bigString, 'distanceMilesToYards');
}

/**
 * Takes distance in feet and returns conversion to miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToMiles } from 'stringman-utils';
 * // or
 * const distanceFeetToMiles = require('stringman-utils').distanceFeetToMiles;
 * const fToMi = distanceFeetToMiles(5280);
 * console.log(fToMi); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToMiles(num: number | string, bigString?: boolean): number | string {
  return precisionMathDivide(num, feetInMile, bigString, 'distanceFeetToMiles');
}

/**
 * Takes distance in miles and returns conversion to feet
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMilesToFeet } from 'stringman-utils';
 * // or
 * const distanceMilesToFeet = require('stringman-utils').distanceMilesToFeet;
 * const miToF = distanceMilesToFeet(1);
 * console.log(miToF); // 5280;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMilesToFeet(num: number | string, bigString?: boolean): number | string {
  return precisionMathMultiply(num, feetInMile, bigString, 'distanceMilesToFeet');
}

/**
 * Takes a numbers of international nautical miles and returns conversion to miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceIntlNauticalMiToMiles } from 'stringman-utils';
 * // or
 * const distanceIntlNauticalMiToMiles = require('stringman-utils').distanceIntlNauticalMiToMiles;
 * const inmMi = distanceIntlNauticalMiToMiles(1);
 * console.log(inmMi); // 1.1507794480235425;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceIntlNauticalMiToMiles(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    num,
    intlNauticalMileToMile,
    bigString,
    'distanceIntlNauticalMiToMiles'
  );
}

/**
 * Takes a number of miles and returns the conversion to international nautical miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMilesToIntlNauticalMi } from 'stringman-utils';
 * // or
 * const distanceMilesToIntlNauticalMi = require('stringman-utils').distanceMilesToIntlNauticalMi;
 * const miInm = distanceMilesToIntlNauticalMi(1.1507794480235425);
 * console.log(miInm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMilesToIntlNauticalMi(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    num,
    intlNauticalMileToMile,
    bigString,
    'distanceMilesToIntlNauticalMi'
  );
}

/**
 * Takes a number of international nautical miles and returns the conversion to feet
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceIntlNauticalMiToFeet } from 'stringman-utils';
 * // or
 * const distanceIntlNauticalMiToFeet = require('stringman-utils').distanceIntlNauticalMiToFeet;
 * const inmFt = distanceIntlNauticalMiToFeet(1);
 * console.log(inmFt); // 6076.115485564;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceIntlNauticalMiToFeet(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    precisionMathMultiply(num, intlNauticalMileToMile, bigString, 'distanceIntlNauticalMiToFeet'),
    feetInMile,
    bigString,
    'distanceIntlNauticalMiToFeet'
  );
}

/**
 * Takes a number of feet and returns the conversion to international nautical miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToIntlNauticalMi } from 'stringman-utils';
 * // or
 * const distanceFeetToIntlNauticalMi = require('stringman-utils').distanceFeetToIntlNauticalMi;
 * const ftInm = distanceFeetToIntlNauticalMi(6076.115485564);
 * console.log(ftInm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToIntlNauticalMi(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    precisionMathDivide(num, intlNauticalMileToMile, bigString, 'distanceFeetToIntlNauticalMi'),
    feetInMile,
    bigString,
    'distanceFeetToIntlNauticalMi'
  );
}

/**
 * Takes a number of international nautical miles and returns the conversion to meters
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceIntlNauticalMiToMeters } from 'stringman-utils';
 * // or
 * const distanceIntlNauticalMiToMeters = require('stringman-utils').distanceIntlNauticalMiToMeters;
 * const inmM = distanceIntlNauticalMiToMeters(1);
 * console.log(inmM); // 1852;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceIntlNauticalMiToMeters(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    num,
    intlNauticalMileToMeters,
    bigString,
    'distanceIntlNauticalMiToMeters'
  );
}

/**
 * Takes a number of meters and returns the conversion to international nautical miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMetersToIntlNauticalMi } from 'stringman-utils';
 * // or
 * const distanceMetersToIntlNauticalMi = require('stringman-utils').distanceMetersToIntlNauticalMi;
 * const mInm = distanceMetersToIntlNauticalMi(1852);
 * console.log(mInm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMetersToIntlNauticalMi(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    num,
    intlNauticalMileToMeters,
    bigString,
    'distanceMetersToIntlNauticalMi'
  );
}

/**
 * Takes a numbers of UK nautical miles and returns conversion to miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceUkNauticalMiToMiles } from 'stringman-utils';
 * // or
 * const distanceUkNauticalMiToMiles = require('stringman-utils').distanceUkNauticalMiToMiles;
 * const uknmMi = distanceUkNauticalMiToMiles(1);
 * console.log(uknmMi); // 1.1515151515151514;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceUkNauticalMiToMiles(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    num,
    uKNauticalMileToMiles,
    bigString,
    'distanceUkNauticalMiToMiles'
  );
}

/**
 * Takes a number of miles and returns the conversion to UK nautical miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMilesToUkNauticalMi } from 'stringman-utils';
 * // or
 * const distanceMilesToUkNauticalMi = require('stringman-utils').distanceMilesToUkNauticalMi;
 * const miUknm = distanceMilesToUkNauticalMi(1.1515151515151514);
 * console.log(miUknm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMilesToUkNauticalMi(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(num, uKNauticalMileToMiles, bigString, 'distanceMilesToUkNauticalMi');
}

/**
 * Takes a number of UK nautical miles and returns the conversion to feet
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceUkNauticalMiToFeet } from 'stringman-utils';
 * // or
 * const distanceUkNauticalMiToFeet = require('stringman-utils').distanceUkNauticalMiToFeet;
 * const uknmFt = distanceUkNauticalMiToFeet(1);
 * console.log(ukmFt); // 6080;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceUkNauticalMiToFeet(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    precisionMathMultiply(num, uKNauticalMileToMiles, bigString, 'distanceUkNauticalMiToFeet'),
    feetInMile,
    bigString,
    'distanceUkNauticalMiToFeet'
  );
}

/**
 * Takes a number of feet and returns the conversion to UK nautical miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceFeetToUkNauticalMi } from 'stringman-utils';
 * // or
 * const distanceFeetToUkNauticalMi = require('stringman-utils').distanceFeetToUkNauticalMi;
 * const ftUknm = distanceFeetToUkNauticalMi(6080);
 * console.log(ftUknm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceFeetToUkNauticalMi(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    precisionMathDivide(num, uKNauticalMileToMiles, bigString, 'distanceFeetToUkNauticalMi'),
    feetInMile,
    bigString,
    'distanceFeetToUkNauticalMi'
  );
}

/**
 * Takes a number of UK nautical miles and returns the conversion to meters
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceUkNauticalMiToMeters } from 'stringman-utils';
 * // or
 * const distanceUkNauticalMiToMeters = require('stringman-utils').distanceUkNauticalMiToMeters;
 * const uknmM = distanceUkNauticalMiToMeters(1);
 * console.log(uknmM); // 1853.184;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceUkNauticalMiToMeters(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathMultiply(
    num,
    uKNauticalMileToMeters,
    bigString,
    'distanceUkNauticalMiToMeters'
  );
}

/**
 * Takes a number of meters and returns the conversion to UK nautical miles
 *
 * * **Environment(s)**<br>
 * [X] server/Node.js<br>
 * [X] client/browser
 *
 * Basic usage example:
 *  ```js
 * import { distanceMetersToUkNauticalMi } from 'stringman-utils';
 * // or
 * const distanceMetersToUkNauticalMi = require('stringman-utils').distanceMetersToUkNauticalMi;
 * const mUknm = distanceMetersToUkNauticalMi(1853.184);
 * console.log(mUknm); // 1;
 * ```
 *
 * @export
 * @param {(number | string)} num
 * @param {boolean} [bigString]
 * @return {*}  {(number | string)}
 */
export function distanceMetersToUkNauticalMi(
  num: number | string,
  bigString?: boolean
): number | string {
  return precisionMathDivide(
    num,
    uKNauticalMileToMeters,
    bigString,
    'distanceMetersToUkNauticalMi'
  );
}
