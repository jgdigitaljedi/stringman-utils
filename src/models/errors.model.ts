// file errors.model.ts
/**
 * @module models
 */

export interface ErrorAndMessage {
  error: boolean;
  message: string;
}
