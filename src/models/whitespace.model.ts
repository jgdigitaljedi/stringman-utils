// file whitespace.model.ts
/**
 * @module models
 */

export interface IReplaceWith {
  tabs?: boolean;
  breaks?: boolean;
  multiSpace?: boolean;
}
