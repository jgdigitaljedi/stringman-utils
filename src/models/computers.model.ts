// file computers.model.ts
/**
 * @module models
 */
export type StorageUnits = 'b' | 'kb' | 'mb' | 'gb' | 'tb' | 'pb';
