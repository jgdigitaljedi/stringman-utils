// file semver.model.ts
/**
 * @module models
 */

export type SemverWhich = 'major' | 'minor' | 'patch';
