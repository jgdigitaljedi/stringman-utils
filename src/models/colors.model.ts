// file colors.model.ts
/**
 * @module models
 */

export interface IHsl {
  h: number;
  s: number;
  l: number;
}
