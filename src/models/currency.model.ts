// file currency.model.ts
/**
 * @module models
 */

export interface CurrencyAmount {
  amount: string;
  symbol: string;
}

export interface LocaleForCurrency {
  language: string;
  country?: string | undefined;
}
