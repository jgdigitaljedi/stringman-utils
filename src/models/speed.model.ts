// file speed.model.ts
/**
 * @module models
 */
export type SpeedPercentUnits = 'ft/s' | 'm/s' | 'km/h' | 'mph' | 'kn' | 'miles/s';
