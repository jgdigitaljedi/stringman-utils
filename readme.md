# Stringman Utils

Stringman-Utils is a collection of 320 small helper scripts that can be imported individually. While most
of the functionality within the scripts is farily simple to write, the value of using Stringman-Utils
is comes from not having to figure out how to write it. Note that TypeScript types are included as well.

Some of the functionality provided:

- several modules centered around regular expressions for finding, replacing, removing, etc from a given string
  - brackets, email addresses, ip addresses, numbers, parenthesis, quotes, URLs, whitespace, and more
- several unit conversion modules that utilize Big.js to provide accurate conversions without having to look up formulas
  - unit conversions include area, clock rate, data rate, data storage, distance, speed, temperature, volume, and weight
- estimated reading time of a blog/article, word counts, etc
- color unit conversions and tools
  - HSL, hex, rgb, change color luminance, and more
- some style helpers
- an exported class that provides currency formatting functionality and internationalization
- modules for changing string case, validating password complexity, working with semantic versions, and more
- much more

Note that any conversion modules have the option to return a string instead of a number. This is to enable extended numeric
values beyond the normal constraints of JavaScript. JavaScript limits decimals to 16 digits before
it begins to return a rounded result or exponential notation. Passing the optional arg for a string return in the conversion modules will allow
you to get returned strings with 30 places of precision on either side of the decimal!

example:

```js
9 * 1111111111111111; // 10000000000000000 (it will round)
precisionMathMultiply(9, 1111111111111111, true); // '9999999999999999' (it will not round)

9 / 1111111111111111; // 8.100000000000001e-15 (it will return exponentional notation)
precisionMathDivide(9, 1111111111111111); // '0.0000000000000081' (it will return a regular float as a string)
```

This is useful when using the conversion functions if you need extreme accuracy and precision. The usage of Big.js also means
that the functions are not prone to JavaScript's weird/inaccurate behavior sometimes experienced with floating point math.

## Install

```js
npm i stringman-utils
```

You can view this on npm [here](https://www.npmjs.com/package/stringman-utils).

## Usage

Utility functions can be imported individually and usage couldn't be more straightforward.

Example:

```js
import { whitespaceRemoveBreaks } from 'stringman-utils';
// or
const whitespaceRemoveBreaks = require('stringman-utils').whitespaceRemoveBreaks;

const removed = whitespaceRemoveBreaks('this line\n has a\r dumb amount of\n line breaks');
console.log(removed); // 'this line has a dumb amount of line breaks'
```

Another example:

```js
import { emailIsValid } from 'stringman-utils';
// or
const emailIsValid = require('stringman-utils').emailIsValid;

const valid = emailIsValid('test@test.me');
const invalid = emailIsValid('test-test.me');
console.log(valid); // true
console.log(invalid); // false
```

## Features

Nothing this library does is particularly difficult to do, but it is meant to save you time and headaches.

Need to pull an email address out of a string? Want to see what a value in feet would be in kilometers? Maybe you need to calculate a reading time for a blog? Stringman-Utils can do all of these things for you and much more.

Basically, this library has a lot of regular expressions for string manipulation, do some small css style calculations, and lots of unit conversions. I also exposed the functions inside of the `precisionMath` file I use for the unit conversion calculations which you can use for your own math functions. There are plans to grow the feature set in the future.

For a full list of functionality, check the [documentation](https://jgdigitaljedi.gitlab.io/stringman-utils/).

## Dependencies

- [Big.js](https://mikemcl.github.io/big.js/)

Big.js is the only external dependency right now. The reason for using it is to provide mathematical precision seeing that JavaScript tends to perform poorly with floating point values. Big.js helps to correct this issue providing accurate calculations.

## Background

This project started as a repo/npm library called "stringman" because it was centered around string manipulation.
I decided I didn't like how I had the tests and dev environment setup and wanted to add more to it. Because I'm
terrible at naming things, I simply added "Utils" to the end of it because it has grown to basically become a
collection of utilities.

## Design

- Every function is exported individually so they can be imported individually. The functions are kept small as well. This means you only import what you need and it will keep your bundle size small.
- Every file is focused on a certain type of functionality and all function within the files are camel case and prefixed with a common word in an attempt to create a better mental model of the library and make it easier to use code completion wihin your editor. For example, every function that has to do with manipulating whitespace is prefixed with `whitespace`. Maybe you have a string that might have extra spaces that need to be removed. You can call `whitespaceSingleSpace` to do this. If you want to remove line breaks, call `whitespaceRemoveBreaks`. You get the idea.
- TypeScript types are included.
- There are basic usage examples for each function in the documentation. There are also units tests for every function if you want to look at the source code.

## Links

- [changelog](https://gitlab.com/jgdigitaljedi/stringman-utils/-/blob/master/CHANGELOG.md)
- [documentation](https://jgdigitaljedi.gitlab.io/stringman-utils/)
- [npm](https://www.npmjs.com/package/stringman-utils)
- [source on GitLab](https://gitlab.com/jgdigitaljedi/stringman-utils)
- [known issues & todos](https://gitlab.com/jgdigitaljedi/stringman-utils/-/blob/master/todos.md)
- [ideas for future features](https://gitlab.com/jgdigitaljedi/stringman-utils/-/blob/master/ideas.md)

## More Examples

Stringman-utils has hundreds of helper functions that can be imported individually so you only have exactly what you need in your bundle. For a complete list, check out the documentation in the link above. Here are just a few more examples of the types of functions available (function names listed to demonstrate naming convention):

- convert miles to meters -> `distanceMilesToMeters`
- convert acres to square yds -> `areaSquareMilesToSquareYards`
- swap out strings inside square brackets with a different string -> `bracketsSwap`
- change any cased string to camel case -> `caseCamelCase`
- convert a hex color code to rgb -> `colorHexToRgb`
- convert MB to bytes -> `dataStorageMbToB`
- instantiate a class that auto formats currency to the locale passed when instantiated -> `CurrencyUtils`
- verify is a string is a valid email address -> `emailIsValid`
- retrieve an IP address from a string -> `ipRetrieve`
- convert a number to hexidecimal -> `numberConvertToHex`
- retrieve any strings inside of parenthesis -> `parensRetrieve`
- create a custom minimum password complexity check as a regular expression -> `passwordBuildRegex`
- perform precise multiplication without the normal JavaScript floating point inaccuracies -> `precisionMathMultiply`
- change double quotes to single quotes -> `quotesDoubleToSingle`
- check that string is a valid semantic version -> `semverIsValid`
- convert any speed to the percentage of the speed of light -> `speedPercentSpeedOfLight`
- get pixel value for REM -> `styleConvertRemToPixels`
- convert celcius to kelvin temperatures -> `tempCelciusToKelvin`
- get the domain from a URL -> `urlGetDomain`
- convert gallons to liters -> `volumeGallonsToLiters`
- convert metric tonnes to pounds -> `weightTonnesToLbs`
- fix a string with extra spaces to only be single spaced -> `whitespaceSingleSpace`
- get a count of how many times a particualr word is in a string -> `wordSpecificWordCount`
