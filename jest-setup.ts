import '@testing-library/jest-dom/extend-expect';

const JSDOM = require('jsdom').JSDOM;
const dom = new JSDOM();
global.document = dom.window.document;
global.window = dom.window;

Object.defineProperty(global.document, 'documentElement', {
  configurable: true,
  get() {
    return document.createElement('document');
  }
});
